function depth = CalcDepth( FullFocusMaps, imsize )
%CALCDEPTH Summary of this function goes here
%   Detailed explanation goes here
depth = 1;
oldval = 0;
for scale = 1:7
    depthmap = zeros(imsize);
    depthmap(:,:,1) = depthmap(:,:,1) + FullFocusMaps(:,:,scale,1);
    depthmap(:,:,2) = depthmap(:,:,2) + FullFocusMaps(:,:,scale,2);
    depthmap(:,:,3) = depthmap(:,:,3) + FullFocusMaps(:,:,scale,3);
    sumVal = sum(depthmap>0);
    if sumVal > oldval
        depth = scale;
        oldVal = sumVal;
    end
    
end

end

