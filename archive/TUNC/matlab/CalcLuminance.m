function Luminance = CalcLuminance( Image )
%CALCLUMINANCE Summary of this function goes here
%   Detailed explanation goes here

%http://www.cacs.louisiana.edu/~cice/lal/
%https://in.mathworks.com/matlabcentral/answers/127944-how-to-pick-the-j-th-percentile-of-a-vector#answer_135386


CorrectedImage = 0.27 * Image(:,:,1) + 0.67 * Image(:,:,2) +...
    0.06 * Image(:,:,3);
pctl = @(v,p) interp1(linspace(0.5/length(v), 1-0.5/length(v),...
    length(v))', sort(v), p*0.01, 'spline');
CorrectedVector = reshape(CorrectedImage,[prod(size(CorrectedImage)),1]);

cu = min(0.05,pctl(CorrectedVector,30)-pctl(CorrectedVector,5))/0.05;
co = min(0.05,pctl(CorrectedVector,95)-pctl(CorrectedVector,70))/0.05;

Luminance = cu * co * abs(pctl(CorrectedVector,95)-pctl(CorrectedVector,5));
end