function [FullFocusMaps, FullC] = ComputeFocusMaps( Image, nScales )
%COMPUTEFOCUSMAPS Summary of this function goes here
%   Detailed explanation goes here

FullFocusMaps = zeros([size(rgb2gray(Image)),7,3]);
FullC = zeros(size(Image));
for nChannel = 1:3
I = Image(:,:,nChannel); 
Pyramid = zeros([size(I),nScales]);
  
  for scale = 1:nScales
      Pyramid(:,:,scale) = Pyramid(:,:,scale) + medfilt2(I,[3*scale, 3*scale]);
  end
  
  DetailMap = zeros([size(I),nScales]);
  DetailMap(:,:,1) = DetailMap(:,:,1) + (I - Pyramid(:,:,1));
  for scale = 2:nScales
      DetailMap(:,:,scale) = DetailMap(:,:,scale) + ...
          (Pyramid(:,:,scale - 1) - Pyramid(:,:,scale));
  end
  
  C = sum(DetailMap,3);
  
   F = zeros([size(I),nScales - 1]);
   M = ones(size(I));
   for scale = 1: nScales - 1
       F(:,:,scale) = F(:,:,scale) + DetailMap(:,:,scale) .* ...
           double(M & (DetailMap(:,:,scale) > DetailMap(:,:,scale + 1)));
       M = M & (DetailMap(:,:,scale) == 0);
   end
   FullFocusMaps(:,:,:,nChannel) = FullFocusMaps(:,:,:,nChannel) + F;
   FullC (:,:,nChannel) = FullC (:,:,nChannel) + C;
end
nPlot = 1;
subplot(2,4,nPlot);subimage(Image);axis off;title('Original');
for scale = 1 : 3
    nPlot= nPlot + 1;
    focusmap = zeros(size(Image));
    focusmap(:,:,1) = focusmap(:,:,1) + FullFocusMaps(:,:,scale,1);
    focusmap(:,:,2) = focusmap(:,:,2) + FullFocusMaps(:,:,scale,2);
    focusmap(:,:,3) = focusmap(:,:,3) + FullFocusMaps(:,:,scale,3);  
    subplot(2,3,nPlot);subimage(focusmap);axis off;title(['focus map ' int2str(scale)]);
end

end

