clc;clear all;
image = '../../Data/TestImages/31_1030736.jpg';
nScales = 8;
Image = im2double(imread(image));
[FullFocusMaps,FullC] = ComputeFocusMaps(Image,nScales);

FullF1 = zeros(size(Image));
FullF1(:,:,1) = FullF1(:,:,1) + FullFocusMaps(:,:,1,1);
FullF1(:,:,2) = FullF1(:,:,2) + FullFocusMaps(:,:,1,2);
FullF1(:,:,3) = FullF1(:,:,3) + FullFocusMaps(:,:,1,3);
BinaryFullF1 = FullF1;
BinaryFullF1(FullF1>0) = 1;
BinaryFullF1(FullF1 == 0) = 0;
Aif = sum(sum(sum(BinaryFullF1)))/prod(size(Image));
InvBinaryFullF1 = FullF1;
InvBinaryFullF1(FullF1>0) = 0;
InvBinaryFullF1(FullF1 == 0) = 1;
Aoof = sum(sum(sum(InvBinaryFullF1)))/prod(size(Image));

Sharpness = CalcSharpness(FullF1);
Depth = CalcDepth(FullFocusMaps, size(Image));
Clarity = CalcClarity(FullC,Aoof,BinaryFullF1,InvBinaryFullF1);
Luminance = CalcLuminance(Image);
Colourfullness = CalcColourfullness(Image);

%subplot(2,2,1);subimage(Image);axis off;title('Original');
%subplot(2,2,2);subimage(FullF1);axis off;title('Depth Map 1');
%figure;

subplot(2,3,5);subimage(FullC);axis off;title('Contrast Image');
poster = ones(size(Image));
subplot(2,3,6);subimage(poster);axis off;title('Ratings');
text(20,20,['Sharpness  : ' num2str(Sharpness)]);
text(20,70,['Depth  : ' num2str(Depth)]);
text(20,110,['Clarity  : ' num2str(Clarity)]);
text(20,150,['Tone  : ' num2str(Luminance)]);
text(20,190,['Colourfullness  : ' num2str(Colourfullness)]);
