import matplotlib.pyplot as plt

circle1 = plt.Circle((0.75, 0.75), 0.05, color='r')



fig, ax = plt.subplots() # note we must use plt.subplots, not plt.subplot
# (or if you have an existing figure)
# fig = plt.gcf()
# ax = fig.gca()
plt.axis('off')
ax.add_artist(circle1)
#ax.add_artist(circle2)
#ax.add_artist(circle3)

#plt.show();
fig.savefig('Sample_3.jpg',dpi = 25)