from __future__ import print_function
import pdb
import numpy as np
import matplotlib
from scipy import stats
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from nltk.tokenize import RegexpTokenizer
#https://stackoverflow.com/questions/15547409/how-to-get-rid-of-punctuation-using-nltk-tokenizer

matplotlib.rcParams.update({'font.size': 18})
commentCorpus = [];
nComments = [];
nWords = [];
tokenizer = RegexpTokenizer(r'\w+')
styles = [];
with open('../Data/AVA/style_image_lists/styles.txt','rb') as f:
	for line in f:
		line = line.strip().strip('\n');
		styles.append(line.split()[1]);

with open ("../Data/AVA/AVA_COMMENTS_STYLE_DATA.txt",'rb') as f :
	for line in f :
		line = line.strip('\n');
		comments = line.split('#')[2:-1];
		commentCorpus.append(comments);
		nComments.append(len(comments));
		for comment in comments:
			#words = comment.strip('\n').split();
			words = tokenizer.tokenize(comment);
			if len(words) > 0: # since 0 length comment doesn't make sense
				nWords.append(len(words))
			#pdb.set_trace();
print("\n\nTotal number of comments are %d" %(np.sum(nComments)));
print ("Mean and SD of the number of comments per image : %f, %f" %(np.mean(nComments),np.std(nComments)));
print ("Mean and SD of the number of words per comment : %f, %f" %(np.mean(nWords),np.std(nWords)));
print("Maximum number of comments in an image : %d" %(np.amax(nComments)));
print("Maximum number of words in a comment : %d" %(np.amax(nWords)));
#pdb.set_trace();
#print (stats.mode(nWords[1:-1]))

n, bins, patches = plt.hist(nComments, 1000, normed=0, facecolor='green')
plt.xlabel('Number of Comments in photograph. \nMax number of comments: %d\nMean : %d(%f)'%(np.amax(nComments),np.mean(nComments),np.around(np.std(nComments),2)));
plt.ylabel('Number of Pictures')
plt.title('Distribution of Comments')
plt.axis([0, np.amax(nComments), 0, 1000])
plt.grid(True)


plt.figure();
n, bins, patches = plt.hist(nWords, bins =1000, normed=0, facecolor='green')
plt.xlabel('Number of Words in a comment\nMax number of words: %d\nMean : %d(%f)'%(np.amax(nWords),np.mean(nWords),np.around(np.std(nWords),2)))
plt.ylabel('Number of Comments')
plt.title('Distribution of Comment Length')
plt.axis([0, np.amax(nWords), 0, 10000])
plt.grid(True)

scoreData = np.loadtxt('../Data/AVA/AVA_STYLE_SCORES');

totalScores = np.sum(np.multiply(np.array(range(1,scoreData.shape[1]+1)),scoreData),axis = 1);
totalSamples = np.sum(scoreData,axis = 1);
#meanScorePerImage = np.mean(scores,axis = 1);
meanScorePerImage = np.divide(totalScores,totalSamples);
classLabels = np.loadtxt('../Data/AVA/style_image_lists/train.lab');

print ("\nMean and SD of scores : %f, %f\n" %(np.mean(meanScorePerImage),np.std(meanScorePerImage)));

matplotlib.rcParams.update({'font.size': 10})
for c,label in enumerate(np.unique(classLabels)):
	if c%7 == 0:
		plt.figure();
	#print (c);
	z = classLabels == label;
	meanScoreForLabel = meanScorePerImage[z];
	#pdb.set_trace();
	location  = int("17"+str(c%7+1));
	plt.subplot(location);	
	n, bins, patches = plt.hist(meanScoreForLabel, bins =1000, normed=0, facecolor='green');
	if c%7 == 0:	
		plt.xlabel('Scores')
		plt.ylabel('Number of Images')
	print ("Mean and SD of scores for %s: %f, %f" %(styles[c],np.mean(meanScoreForLabel),np.std(meanScoreForLabel)));
	title = styles[c]+ ' ('+ str(np.around(np.mean(meanScoreForLabel),2)) + ','+ str(np.around(np.std(meanScoreForLabel),2))+ ')';
	plt.title(title);
	plt.axis([0, 10, 0, 15])
	plt.grid(True)
#plt.tight_layout();

	
plt.show()
#pdb.set_trace();