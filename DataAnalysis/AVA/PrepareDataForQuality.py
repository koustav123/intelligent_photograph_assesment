from __future__ import print_function
import pdb
import numpy as np
from shutil import copyfile
#Finding scores for all images

Training_IDs = np.loadtxt('../Data/AVA/AVA_Full_Training.ids',dtype = int);
Trainig_scoreData = np.loadtxt('../Data/AVA/AVA_Full_Training.scores');

totalScores = np.sum(np.multiply(np.array(range(1,Trainig_scoreData.shape[1]+1)),Trainig_scoreData),axis = 1);
totalSamples = np.sum(Trainig_scoreData,axis = 1);
#meanScorePerImage = np.mean(scores,axis = 1);
meanScorePerImage = np.divide(totalScores,totalSamples);
_d_1 = 0;
_d_2 = 1;

_d1_scores = meanScorePerImage[(meanScorePerImage < (5-_d_1)) | (meanScorePerImage > (5+_d_1)) ];
_d1_Ids = Training_IDs[(meanScorePerImage < (5-_d_1)) | (meanScorePerImage > (5+_d_1)) ];
_d2_scores = meanScorePerImage[(meanScorePerImage < (5-_d_2)) | (meanScorePerImage > (5+_d_2)) ]
_d2_Ids = Training_IDs[(meanScorePerImage < (5-_d_2)) | (meanScorePerImage > (5+_d_2)) ];

_d1_Labels = np.ones(_d1_scores.shape)*(_d1_scores > 5);
_d2_Labels = np.ones(_d2_scores.shape)*(_d2_scores > 5);

np.savetxt('../Data/AVA/_d1_IDS.ids',_d1_Ids,fmt = "%d");
np.savetxt('../Data/AVA/_d1_Labels.labels',_d1_Labels, fmt = "%d");
np.savetxt('../Data/AVA/_d2_IDS.ids',_d2_Ids, fmt = "%d");
np.savetxt('../Data/AVA/_d2_Labels.labels',_d2_Labels, fmt = "%d");

#_d1_Labels = _d1_Labels[0:1000];
#_d2_Labels = _d2_Labels[0:1000];

#Partition training and validation sets for _d1
nTrain = 5*_d1_Labels.shape[0]/6;
nVal = _d1_Labels.shape[0]/6;
trainLabels = _d1_Labels[0:nTrain];
trainIds = _d1_Ids[0:nTrain];
valLabels = _d1_Labels[nTrain:-1];
valIds = _d1_Ids[nTrain:-1];

print ("Starting _d1")
print ("Starting _d1_training")
for eachId,eachLabel in zip(trainIds,trainLabels):
    src = '/media/koustav/Naihati/Dataset/AVADataSet/'+str(eachId)+'.jpg';
    dst = '/media/koustav/Naihati/Dataset/AVA_DATA_DIVIDED/D_1/train/'+str(int(eachLabel))+'/'+str(eachId)+'.jpg'
    #pdb.set_trace();
    try :   
        copyfile(src,dst);
    except :
        print ("Could not Process %s"%(src))

print ("Starting _d1_validation")
for eachId,eachLabel in zip(valIds,valLabels):
    src = '/media/koustav/Naihati/Dataset/AVADataSet/'+str(eachId)+'.jpg';
    dst = '/media/koustav/Naihati/Dataset/AVA_DATA_DIVIDED/D_1/val/'+str(int(eachLabel))+'/'+str(eachId)+'.jpg'
    try :	
        copyfile(src,dst);
    except:
        print ("Could not Process %s"%(src));
 

#Partition training and validation sets for _d2
nTrain = 5*_d2_Labels.shape[0]/6;
nVal = _d2_Labels.shape[0]/6;
trainLabels = _d2_Labels[0:nTrain];
trainIds = _d2_Ids[0:nTrain];
valLabels = _d2_Labels[nTrain:-1];
valIds = _d2_Ids[nTrain:-1];

print ("Starting _d2")
print ("Starting _d2_training")
for eachId,eachLabel in zip(trainIds,trainLabels):
    src = '/media/koustav/Naihati/Dataset/AVADataSet/'+str(eachId)+'.jpg';
    dst = '/media/koustav/Naihati/Dataset/AVA_DATA_DIVIDED/D_2/train/'+str(int(eachLabel))+'/'+str(eachId)+'.jpg'
    try :   
        copyfile(src,dst);
    except :
        print ("Could not Process %s"%(src))

print ("Starting _d2_validation")
for eachId,eachLabel in zip(valIds,valLabels):
    src = '/media/koustav/Naihati/Dataset/AVADataSet/'+str(eachId)+'.jpg';
    dst = '/media/koustav/Naihati/Dataset/AVA_DATA_DIVIDED/D_2/val/'+str(int(eachLabel))+'/'+str(eachId)+'.jpg'
    try :	
        copyfile(src,dst);
    except:
        print ("Could not Process %s"%(src));


#pdb.set_trace();
			