import os
from PIL import Image
import pdb
import numpy as np
import matplotlib.pyplot as plt

dataPath = '/media/koustav/Naihati/Dataset/AVA/AVA_STYLE_WITH_SALIENCY/train/'
dirs = os.listdir(dataPath);

def create_DT(size):
    #pdb.set_trace();
    # Creates the two distance channels (horizontal and vertical) normalized between 0 -1.
    size = (size[1],size[0]); # Image size and array size reverse upon conversion from PIL to array 
    x_array = np.tile(np.linspace(0,1,size[1]),(size[0],1));
    y_array = np.transpose(np.tile(np.linspace(0,1,size[0]),(size[1],1)));
    dt_array = np.dstack((x_array,y_array));
    return dt_array;

means = [];
sds = [];
for dirc in dirs:
    fileList = os.listdir(dataPath + '/' + dirc);
    for image in fileList:
        imPath = dataPath + '/' + dirc + '/' + image;
        img = Image.open(imPath);
        img_array = np.asarray(img);
        pdb.set_trace();
        sal_channel = img_array[:,:,3];
        means.append(np.mean(sal_channel/255.0))
        sds.append(np.std(sal_channel/255.0))
print ("Mean = %f, SD = %f"%(np.mean(means), np.mean(sds)));
plt.figure()
n, bins, patches = plt.hist(means, 50, normed=1, facecolor='green', alpha=0.75)
plt.title('Mean')
plt.figure();
n, bins, patches = plt.hist(sds, 50, normed=1, facecolor='green', alpha=0.75)
plt.title('SD');
plt.show();

#