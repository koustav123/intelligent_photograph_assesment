from __future__ import print_function
import os
import pdb
import numpy as np

rootDir = "/media/koustav/Naihati/Dataset/WikiPaintings_Divided/val/"
classes = os.listdir(rootDir);
'''print('\\begin{enumerate}')
for c in classes:
    print ('\\item %s'%(c))
    print ('\\begin{figure}')
    files = os.listdir(rootDir+c);
    indices = np.random.randint(0,len(files),5);
    print ('\\begin{tabular}{|c|c|c|c|c|}\n\\hline');
    for count, i in enumerate(indices):
        print ('\includegraphics[width=2cm]{/media/koustav/Naihati/Dataset/Flickr_Style_Divided/val/%s/%s}'%(c,files[i]), end = ' ');
        if count < 4:
            print ('&\n')
        else :
            print ('\n')
    print ('\\tabularnewline\n\\hline\n\\end{tabular}')
    print ('\\caption{%s}'%(c))
    print ('\\end{figure}')
    print ('\n')
print('\\end{enumerate}')'''

print('\\begin{figure}')
print ('\\begin{tabular}{|c|c|c|c|c|}\n\\hline');
for c in classes[21:28]:
    files = os.listdir(rootDir+c);
    indices = np.random.randint(0,len(files),5);
    for count, i in enumerate(indices):
        print ('\\includegraphics[width=2cm,height=2cm]{/media/koustav/Naihati/Dataset/WikiPaintings_Divided/val/%s/%s}'%(c,files[i]), end = ' ');
        if count < 4:
            print ('&')
        else :
            print ('\\\\')
            print('\\hline')
            print('\\multicolumn{5}{|c|}{%s}\\\\'%(c.replace('style_','').replace('_','-')))
            print('\\hline')
            
print ('\\end{tabular}')
    #print ('\\caption{%s}'%(c.replace('style_','').replace('_','-')))
    #print ('\n')
print('\\end{figure}')
#pdb.set_trace();