import numpy as np
import pdb

#fulltestids = 
file_full_class_ids = '/media/koustav/Naihati/Dataset/WikiPaintings_Divided/All_TestImages.id';
file_full_labels = '/media/koustav/Naihati/Dataset/WikiPaintings_Divided/All_TestLabels.Labels';
#classes_all = 
file_classes_25 = '/media/koustav/Naihati/Dataset/WikiPaintings_Divided/25_Classes.classlist';

full_class_ids = np.loadtxt(file_full_class_ids,dtype = str);
full_labels = np.loadtxt(file_full_labels,dtype = int);
classes_25 = np.loadtxt(file_classes_25,dtype = str).tolist();

labels_25 = [];
testIds_25 = [];
classes = [];
nSamples = np.zeros(len(classes_25));

for ids,cls in full_class_ids:
    if cls in classes_25:
        new_label = np.zeros(len(classes_25));
        new_label[classes_25.index(cls)] = 1;
        labels_25.append(new_label);
        testIds_25.append(ids);
        classes.append(cls);
        nSamples[classes_25.index(cls)] = nSamples[classes_25.index(cls)] + 1;
root = '/media/koustav/Naihati/Dataset/WikiPaintings_Divided/';     
np.savetxt(root+'25_TestIds.id',np.array(testIds_25), fmt = '%s');
np.savetxt(root+'25_TestLabels.Labels',np.vstack(labels_25), fmt = '%d');

np.savetxt(root+'NumberOfSamples.txt',zip(classes_25,nSamples),fmt = '%s');