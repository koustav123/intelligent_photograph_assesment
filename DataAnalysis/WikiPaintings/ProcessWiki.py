from __future__ import print_function
import os
import argparse
import pdb
import numpy as np
from itertools import compress
import multiprocessing
from shutil import copyfile


def SaveTrainImage((src, imId, label)):
    global args;
    try:
        copyfile(src,args.op+'/train/'+label+'/'+str(imId)+'.jpg');
    except:
        pass
    
def SaveValImage((src, imId, label)):
    global args;
    try:
        copyfile(src,args.op+'/val/'+label+'/'+str(imId)+'.jpg');
    except:
        pass
    
imageList = [];
labelList = [];
parser = argparse.ArgumentParser(description='Flickr-Style DATA Divider');
parser.add_argument('--id_file', type=str, default='../DataSource/wikipaintings_oct2013.csv',
                    help='location of the ids')
parser.add_argument('--ip', type=str, default='/media/koustav/Naihati/Dataset/WikiPaintings/',
                    help='folder to copy images from')
parser.add_argument('--op', type=str, default='/media/koustav/Naihati/Dataset/WikiPaintings_Divided/',
                    help='folder to save images')
                    
#numbers don't sum to 100
parser.add_argument('--percent_train', type=float, default=.6,
                    help='percentage of training data')
parser.add_argument('--percent_test', type=float, default=.2,
                    help='percentage of testing data')
parser.add_argument('--percent_val', type=float, default=.2,
                    help='percentage of validation data')
args = parser.parse_args();



with open (args.id_file) as f :
    for count, line in enumerate(f):
        line = line.strip('\n')
        contents = line.split(',')
        if 0 == count:
            continue;
            ''' classNames = contents[4:24];
            for name in classNames:
                if os.path.exists(args.op+'/train/'+name) or os.path.exists(args.op+'/val/'+name):
                    continue;
                os.system('mkdir '+args.op+'/train/'+name);
                os.system('mkdir '+args.op+'/val/'+name);'''
                
        else:
             imId = count;
             #assign source fro id
             src = args.ip+'/'+ str(imId) + '.jpg';
             
             #find class and split
             classLabel = contents[5].replace(' ','_').replace('(','_').replace(')','_');
             labelList.append(classLabel);
             imageList.append((src, imId, classLabel));
             
classNames = np.unique(labelList);
#pdb.set_trace();
if not os.path.exists(args.op+'train/'):
    os.system('mkdir ' + args.op+'train/');
    for c in classNames:
        os.system('mkdir ' + args.op+'train/' + c);
    
if not os.path.exists(args.op+'val/'):
    os.system('mkdir ' + args.op+'val/');
    for c in classNames:
        os.system('mkdir ' + args.op+'val/' + c);

total_data = len(imageList);
n_Train = int(args.percent_train*total_data);
n_Val = int(args.percent_val*total_data);
n_Test = int(args.percent_test*total_data);

random_test_indices = np.hstack((np.full(n_Train+n_Val, False),np.full(n_Test, True)));
np.random.shuffle(random_test_indices);

finalTestData = list(compress(imageList,random_test_indices));
finalTrainValData = list(compress(imageList, [not i for i in random_test_indices]));

random_train_indices = np.hstack((np.full(n_Val, False),np.full(n_Train, True)));
np.random.shuffle(random_train_indices);

finalTrainingData = list(compress(finalTrainValData,random_train_indices));
finalValData = list(compress(finalTrainValData, [not i for i in random_train_indices]));

with open(args.op+'TestImages.id', 'wb') as f:
    for d in finalTestData:
        print(d[1],d[2],file=f);

p1 = multiprocessing.Pool(32)
p1.map(SaveTrainImage, finalTrainingData); # range(0,1000) if you want to replicate your example
#p1.daemon = True;
p1.close();
p1.join();

p2 = multiprocessing.Pool(32)
p2.map(SaveValImage, finalValData); # range(0,1000) if you want to replicate your example
#p2.daemon = True;
p2.close();
p2.join();

#pdb.set_trace();
