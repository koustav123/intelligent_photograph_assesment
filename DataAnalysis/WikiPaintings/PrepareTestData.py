import numpy as np
import pdb

data = np.loadtxt('/media/koustav/Naihati/Dataset/WikiPaintings_Divided/TestImages.id', dtype = str);

classes = np.unique(data[:,1]).tolist();
labels = np.zeros([data.shape[0],len(classes)]);
#pdb.set_trace();
for c, d in enumerate(data):
    labels[c][classes.index(data[c][1])] = 1;

pdb.set_trace();
np.savetxt('/media/koustav/Naihati/Dataset/WikiPaintings_Divided/TestLabels.Labels', labels, fmt = '%d');    
np.savetxt('/media/koustav/Naihati/Dataset/WikiPaintings_Divided/TestIds.Id', data[:,0], fmt = '%s');    
#pdb.set_trace();