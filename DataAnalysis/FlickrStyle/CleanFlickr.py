from __future__ import print_function
import glob
import cv2
from skimage.measure import compare_ssim
import os

#imageList =  glob.glob('/media/koustav/Naihati/Dataset/WikiPaintings/*.jpg');
imageList =  glob.glob('/media/koustav/Naihati/Dataset/FlickrStyle/*.jpg');
blankImage = cv2.imread('/home/koustav/Desktop/IPA/intelligent_photograph_assesment/DataAnalysis/DataSource/4033.jpg');

grayBlankImage = cv2.cvtColor(blankImage, cv2.COLOR_BGR2GRAY);
for count, image in enumerate(imageList):
    if count%500 == 0:
        os.system('ls /media/koustav/Naihati/Dataset/FlickrStyle/ | wc -l');
    im = cv2.imread(image);
    gim = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY);
    #print count;
    try:
        (score, diff) = compare_ssim(grayBlankImage, gim, full=True)
    except:
        continue;
    if score == 1.0:
        os.system('rm '+image);
    #print (score,image)