from __future__ import print_function
import os
import argparse
import pdb
import numpy as np
from itertools import compress
import multiprocessing
from shutil import copyfile


def SaveTrainImage((src, imId, label)):
    global args;
    try:
        copyfile(src,args.op+'/train/'+label+'/'+str(imId)+'.jpg');
    except:
        pass
    
def SaveValImage((src, imId, label)):
    global args;
    try:
        copyfile(src,args.op+'/val/'+label+'/'+str(imId)+'.jpg');
    except:
        pass
    
trainList = [];
testList = [];

parser = argparse.ArgumentParser(description='Flickr-Style DATA Divider');
parser.add_argument('--id_file', type=str, default='../DataSource/flickr_df_mar2014.csv',
                    help='location of the ids')
parser.add_argument('--ip', type=str, default='/media/koustav/Naihati/Dataset/FlickrStyle/',
                    help='folder to copy images from')
parser.add_argument('--op', type=str, default='/media/koustav/Naihati/Dataset/Flickr_Style_Divided/',
                    help='folder to save images')
                    
#numbers don't sum to 100
parser.add_argument('--percent_train', type=float, default=.8,
                    help='percentage of training data')
parser.add_argument('--percent_test', type=float, default=.2,
                    help='percentage of testing data')
parser.add_argument('--percent_val', type=float, default=20,
                    help='percentage of validation data')
args = parser.parse_args();

if not os.path.exists(args.op+'train/'):
    os.system('mkdir ' + args.op+'train/');
    
if not os.path.exists(args.op+'val/'):
    os.system('mkdir ' + args.op+'val/');

with open (args.id_file) as f :
    for count, line in enumerate(f):
        line = line.strip('\n')
        contents = line.split(',')
        if 0 == count:
            classNames = contents[4:24];
            for name in classNames:
                if os.path.exists(args.op+'/train/'+name) or os.path.exists(args.op+'/val/'+name):
                    continue;
                os.system('mkdir '+args.op+'/train/'+name);
                os.system('mkdir '+args.op+'/val/'+name);
                
        else:
             imId = count;
             #assign source fro id
             src = args.ip+'/'+ str(imId) + '.jpg';
             
             #find class and split
             classLabel = classNames[contents[4:24].index('True')];
             split = contents[-1];
             if split == 'train':
                     trainList.append((src, imId, classLabel));
             else :
                     testList.append((src, imId, classLabel));
             

total_train_data = len(trainList);
n_Train = int(args.percent_train*total_train_data);
n_Val = total_train_data - n_Train;
random_data_indices = np.hstack((np.full(n_Train, True),np.full(n_Val, False)));
np.random.shuffle(random_data_indices);

finalTrainingData = list(compress(trainList,random_data_indices));
finalValData = list(compress(trainList, [not i for i in random_data_indices]));


with open(args.op+'TestImages.id', 'wb') as f:
    for d in testList:
        print (d[1],d[2],file=f);
p1 = multiprocessing.Pool(16)
p1.map(SaveTrainImage, finalTrainingData); # range(0,1000) if you want to replicate your example
#p1.daemon = True;
p1.close();
p1.join();

p2 = multiprocessing.Pool(16)
p2.map(SaveValImage, finalValData); # range(0,1000) if you want to replicate your example
#p2.daemon = True;
p2.close();
p2.join();

#pdb.set_trace();
