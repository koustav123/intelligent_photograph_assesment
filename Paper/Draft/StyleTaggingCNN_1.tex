\documentclass[10pt,twocolumn,letterpaper]{article}
\usepackage{iccv}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\graphicspath{ {figure/pdf/} }
% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

% \iccvfinalcopy % *** Uncomment this line for the final submission

\def\iccvPaperID{****} % *** Enter the ICCV Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ificcvfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Understanding Photographic Styles Using Dense Convolutional Networks}

\author{First Author\\
Institution1\\
Institution1 address\\
{\tt\small firstauthor@i1.org}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Second Author\\
Institution2\\
First line of institution2 address\\
{\tt\small secondauthor@i2.org}
}

\maketitle
%\thispagestyle{empty}


%%%%%%%%% ABSTRACT
\begin{abstract}
   The ABSTRACT is to be in fully-justified italicized text, at the top
   of the left-hand column, below the author and affiliation
   information. Use the word ``Abstract'' as the title, in 12-point
   Times, boldface type, centered relative to the column, initially
   capitalized. The abstract is to be in 10-point, single-spaced type.
   Leave two blank lines after the Abstract, then begin the main text.
   Look at previous ICCV abstracts to get a feel for style and length.
	 Please note that the title can be up to 512 characters in length. 
	 The maximum size of the abstract is 40pt00 characters.Look at previous ICCV abstracts to get a feel for style and length.
	 Please note that the title can be up to 512 characters in length. 
	 The maximum size of the abstract is 40pt00 characters.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
\label{sec:intro}
\noindent Amid millions of images and video clips uploaded over the internet, there are a few which stand out and leave their mark. For example, the recent photograph of the assassination of a Russian ambassador by \textit{Burhan Ozbilici} made headlines because of the sheer power, anxiety and irony it stands for. On the other hand, one cannot help but appreciate the portrayal of the co-existence of the ordinary and the sublime, in renowned Iranian film director Majid Majidi's movies. Browsing through the works of the great photographers or cinematographers, one gets a taste of their originality or style, which make them stand out, consistently, over the years. Thus, analysing styles, is a core component for understanding visual arts.

\noindent In this work, we propose a system, which tries to model the aesthetic attributes of a photograph and predict it's style. Please note, that by aesthetic attributes we mean that we try to model some of the content-oriented objective properties of a photograph based on appearance and geometric features~\cite{beardsley1981aesthetics}. Analysing it's affect-oriented or normative properties is beyond the scope of this work. Motivated by the recent developments in the field of Deep Convolutional  Neural Networks~(CNN), our system takes a photograph as an input and predict it's styles from a list of 14 categories, as listed in Figure~\ref{Fig:ExampleImages}. Additionally, it assesses the overall quality of the picture and rates it on a scale of $10$. There are several applications of automatic photographic style classification. For example, post-processing images and videos, tagging, image-retrieval, scene-analysis, building assistive-technolgies \etc.

\noindent Using transfer learning\cite{yosinski2014transferable}, we adopt the DenseNet~\cite{huang2016densely} architecture, trained on ImageNet~\cite{deng2009imagenet} dataset and fine-tune the model on the 14 style classes in the AVA dataset\cite{murray2012ava}. AVA is an image database containing 250,000 images with a quality rating on a scale of 10. A subset of approximately 14000 images contain style annotations corresponding to 14 different photographic attributes as illustrated in Figure~\ref{Fig:ExampleImages}. We describe AVA in more detail in Section~\ref{sec:dataset}. In our experiments, we observe that the DenseNet161 architecture perform better than the state of the art in style-classification~\cite{lu2015deep,lu2014rapid} and overall quality prediction[\#VERIFY].\\
Our second contribution in this work is an analysis of the effect of different data-augmentation strategies on style prediction. Traditionally, CNNs take input of a fixed size (usually a $224*224*$ patch). The source image is either cropped or re-scaled~(warped) to the desired size and passed through the network. Although information is lost, it works well in scenarios where the object of interest is localized at a particular position (say at the centre). In fact, as pointed out by~\cite{lu2015deep} it improves performance by reducing overfitting. On the other hand, warping captures global information better but it loses much of geometric properties due to distortion and comes with the curse of overfitting. \\
Intuitively, it appears that the content-oriented properties of a photograph (as in Figure~\ref{Fig:ExampleImages}) should depend on both local and global factors. For example, to find whether a photograph conforms to the Rule of Thirds principle, the relative positions of the main subjects are required. This information is completely lost when a fixed-size crop is extracted. But, the appearance based properties like Image Grain and HDR are preserved. However, Contrary to this intuition, it is shown in~\cite{lu2014rapid} that cropping performs much better than warping in style classification. We experiment, with several data-augmentation strategies and try to understand the rationale behind such counter-intuitive results. In Section~\ref{sec:ResStyleClass}, we heuristically conclude that although cropping improves the overall accuracy, it performs worse in case of particular classes like Rule of Thirds or Vanishing Lines. We also observe that a hybrid data augmentation strategy, which is a combination of random sized cropping and warping,  improves the overall accuracy better than either warping or cropping.

\noindent The rest of the paper is organised as follows. In Section~\ref{sec:reltd}, we summarize the relevant literature in image quality prediction. In Section~\ref{sec:arch}, we describe the CNN model we adopted and different augmentation strategies we used. In Section~\ref{sec:dataset}, we provide a detailed description and analysis of the AVA dataset. In Section~\ref{sec:results}, we provide  details of the experiments conducted and analyse the results. 
\begin{figure*}
\label{Fig:ExampleImages}
\centering
\resizebox{\textwidth}{!}{
\fbox{
\begin{tabular}{ccccccc}
\includegraphics[height=42pt, width = 56pt]{AT_1}&
\includegraphics[height=42pt, width = 56pt]{AT_2}&
\includegraphics[height=42pt, width = 56pt]{AT_3}&
\includegraphics[height=42pt, width = 56pt]{AT_4}&
\includegraphics[height=42pt, width = 56pt]{AT_5}&
\includegraphics[height=42pt, width = 56pt]{AT_6}&
\includegraphics[height=42pt, width = 56pt]{AT_7}\\
\includegraphics[height=42pt, width = 56pt]{AT_8}&
\includegraphics[height=42pt, width = 56pt]{AT_9}&
\includegraphics[height=42pt, width = 56pt]{AT_10}&
\includegraphics[height=42pt, width = 56pt]{AT_11}&
\includegraphics[height=42pt, width = 56pt]{AT_12}&
\includegraphics[height=42pt, width = 56pt]{AT_13}&
\includegraphics[height=42pt, width = 56pt]{AT_14}
\end{tabular}}
}
\caption{Example images from the AVA dataset corresponding to 14 different styles. (L-R)
\textbf{Row 1 : }  Complementary Colors, Duotones, HDR, Image Grain, Light On White, Long Exposure, Macro.
\textbf{ Row 2 : } Motion Blur, Negative Image, Rule of Thirds, Shallow DOF, Silhouettes, Soft Focus, Vanishing Point}
\end{figure*}
\section{Related Work}
\label{sec:reltd}
\noindent Although image and video classification has always been a fundamental problem in Computer Vision, a considerable part of existing research is focussed on identifying and classifying objects or the scene. In other words, the intriguing part for the community has been understanding the quantifiable visual semantics like the class~\cite{lu2007survey,schmidhuber2015deep}, position~\cite{yilmaz2006object} and number of objects in an image. However, understanding the qualitative aspects from a creative perspective, has received limited attention.\\
The early works in photograph quality prediction relied on modelling popular attributes like the Rule of The Thirds, colour harmony, exposure etc~\cite{datta2006studying,ke2006design,luo2008photo}. Some recent works address the problem in a similar way but with improved features\cite{ obrador2012towards,dhar2011high,joshi2011aesthetics,san2012leveraging}. In \cite{aydin2015automated}, Aydin~\etal point out the \textit{generalist} and \textit{particularist} approach in evaluating a photograph and propose a callibration technique for aggregating the scores for different attributes. In~\cite{murray2012ava}, Marchesotti~\etal published the AVA dataset which is a collection of about 255,000 images, ratings and style annotations from DPChallenge website. It was the release of AVA and parallel advances in Deep Learning that resulted in a surge of research in this area in the last few years.\\
In~\cite{lu2014rapid}, Lu~\etal propose two Convolutional Neural Network architectures which capture the local and global proerties of a photograph. The same authors use a multiple instance learning-based strategy in~\cite{lu2015deep} and achieve a much better performance for style-classification. In~\cite{kong2016photo}, Kong~\etal learn styles and ratings jointly on a new dataset. Mai~\etal~\cite{mai2016composition} propose a network that uses a composition-preserving input mechanism.\\
In recent years, Deep Learning has performed remarkably well in many Computer Vision tasks like classification~\cite{krizhevsky2012imagenet,simonyan2014very}, detection~\cite{girshick2015fast,ren2015faster}, segmentation~\cite{noh2015learning} and scene understanding~\cite{karpathy2015deep,xu2015show,antol2015vqa}.  Recent works like~\cite{huang2016densely,he2016deep} have performed well in multi-tasking frameworks for detection and classification. In~\cite{he2016deep}, the authors use a residual framework for tackling training error upon addition of new layers. In ~\cite{huang2016densely} the authors use dense connections by connecting outputs from all the previous layers as input to the next layer. \\
\noindent In principle, our pipeline is similar to \cite{lu2015deep,lu2014rapid} in a sense that we also perform a neural style prediction on the AVA dataset. However, our work differs in two important aspects. First, we use an architecture that performs better than their double column~\cite{lu2014rapid} or multi-patch aggregation~\cite{lu2015deep} strategies, using a simpler and straightforward training procedure. Second, we  attempt to understand the effects of different data-augmentation strategies on the photographic attributes. In~\cite{lu2014rapid,lu2015deep} the authors suggest that a random crop from the input handles overfitting better than warping and hence perform better. We argue that although a random crop preserves the appearance-based attributes, it fails to preserve the geometric attributes of the photograph like the rule-of-thirds. By examining the effects of different data-augmentation strategies on the different geometric and appearance-based styles, we argue that a hybrid strategy acts as a trade-off between cropping and warping and performs better. 
\section{Architecture}
\newpage
\label{sec:arch}
\subsection{CNN}
\begin{figure*}
\begin{center}
\fbox{\rule{0pt}{4in} \rule{\linewidth}{0pt}}
\end{center}
   \caption{CNN Architecture}
\label{fig:short}
\end{figure*}
\subsection{Data Augmentation}
\label{sec:archDataAug}
\noindent Since the input dimensions of our network are fixed and the photographic images have non-uniform dimensions, we try five different data-augmentation techniques.
\begin{itemize}
\item \textbf{Centre Crop ($T_{cc}$)} : Here, a patch of size ($s*s*3$) is extracted from the center of the image. This popular strategy is common in scenarios where the object of interest is centrally-localized.
\item \textbf{Random crop of fixed size ($T_{rc}$)} : Patches of size ($s*s*3$) are extracted from the image at random locations. Although it suffers from data loss, it is effective in scenarios where the object of interest is not centrally localized.
\item \textbf{Warping ($T_w$)} : The input is anisotropically resized to a fixed dimension of ($s*s*3$). This transformation fails to preserve the geometric properties of the input. However unlike a crop, it preserves global information. 
\item \textbf{Isotropic centre crop ($T_{icc}$)} : In this case, the input is isotropically resized by warping it's lower dimension to $s$. Then, a centre crop of ($s*s*3$) is applied. This preserves the geometric properties of the image at the cost of some global information. 
\item \textbf{Random crops of random sizes ($T_{rnc}$)} : A crop of random size and random aspect ratio is made from the input. Then, the it is resized to ($s*s*3$). We demonstrate in Section~\ref{sec:results}, that this strategy addresses the limitations of the previous strategies and performs best. 
\end{itemize}
We train and test our networks with all these strategies. In~\cite{lu2014rapid}, the authors use $T_{rc}$ for a single-column network and both $T_{rc}$ and $T_w$ for a double column network. In~\cite{lu2015deep}, the authors use $T_{rc}$.
\section{Dataset}
\label{sec:dataset}
\section{Experiments}
\label{sec:results}
\subsection{Style Classification}
\noindent We train style classifiers on 14 AVA style attributes with different data-augmentation strategies described in Section~\ref{sec:archDataAug}. We train for 25 epochs and during each epoch, we choose one of the transformations. We use 11270 images for training and validation and the 2573 images for testing. Please note, that for all epochs, the transformation type remains constant for the model. The experiments are carried out in a GPU with $2560$ cuda cores and $8$ Gigabytes of memory. Training a model takes about 120 minutes with a batch-size of 5. During test phase, we follow the approach proposed by ~\cite{lu2014rapid,lu2015deep}. 50 patches are extracted from the test-image and and each patch is passed through the network. The results are averaged to achieve the final scores. The scores are reported in terms of Average Precision (AP) and Mean Average Precision (MAP).
\label{sec:ResStyleClass}
\subsubsection{Best Data-Augmentation}
\noindent Using the DenseNet architecture, we perform experiments with $25$ possible combinations of the $5$ different data-augmentation strategies. From the results in Table~\ref{tab:allAug}, we observe that $T_{rnc}$ performs best when used both during train and test phases. Table~\ref{tab:allNetStyle} lists different networks trained and tested  with $T_{rnc}$ except the last two rows which are reported from the papers. From Table~\ref{tab:allNetStyle}, it is observed that all networks trained with  $T_{rnc}$ outperforms ~\cite{lu2014rapid,lu2015deep} by a large margin. One might argue that this improvement can be attributed to the use of a better architecture. We accept this argument. But we observe from Table~\ref{tab:allAug} that for the same architecture, $T_{rnc}$ performs much better than all other strategies. So, it can be safely concluded that for improving the overall accuracy of style classification, $T_{rnc}$ is the best strategy. But as we had pointed out in Section~\ref{sec:intro} we are also interested in finding out how these strategies perform for different attributes, individually. In the next section, we analyse each style attribute. 
\begin{table}
\centering
\caption{\textbf{Style Classification : Comparison of DenseNet161 having different augmentation strategies with the state of the art}}
\begin{tabular}{|c|c|c|c|c|}
\hline
\# & Aug-Training & Aug-Testing & AP & MAP\\
\hline\hline
1&$T_{cc}$&$T_{cc}$& 69.65 &63.85\\
2&$T_{cc}$&$T_w$& 62.78 & 56.54\\
3&$T_{cc}$&$T_{rc}$&72.83&65.15\\
4&$T_{cc}$&$T_{icc}$&64.68&58.08\\
5&$T_{cc}$&$T_{rnc}$&70.15&64.37\\
\hline\hline
6&$T_{rc}$&$T_{cc}$&70.59&62.24\\
7&$T_{rc}$&$T_{rc}$&\textbf{75.88}&\textbf{67.90}\\
8&$T_{rc}$&$T_w$&67.93&61.87\\
9&$T_{rc}$&$T_{icc}$&69.53&63.16\\
10&$T_{rc}$&$T_{rnc}$&73.74&67.20\\
\hline\hline
11&$T_w$&$T_{cc}$&60.08&52.81\\
12&$T_w$&$T_{rc}$&66.81&59.62\\
13&$T_w$&$T_{w}$ & 69.68 & 63.75\\
14&$T_w$&$T_{icc}$&69.27&63.35\\
15&$T_w$&$T_{rnc}$&71.35&65.96\\
\hline\hline
16&$T_{icc}$&$T_{cc}$&62.07&53.76\\
17&$T_{icc}$&$T_w$&67.65&61.03\\
18&$T_{icc}$&$T_{rc}$&70.01&64.52\\
19&$T_{icc}$&$T_{icc}$& 70.42 &64.31\\
20&$T_{icc}$&$T_{rnc}$&72.42&66.35\\
\hline\hline
21&$T_{rnc}$&$T_{cc}$&69.01&60.68\\
22&$T_{rnc}$&$T_{rc}$&74.17&66.40\\
23&$T_{rnc}$&$T_w$&71.47&65.42\\
24&$T_{rnc}$&$T_{icc}$&72.52&66.13\\
25&$T_{rnc}$&$T_{rnc}$& \textbf{75.34} & \textbf{68.60} \\
\hline
\end{tabular}
\label{tab:allAug}
\end{table}

\begin{table}
\centering
\caption{\textbf{Style Classification : Different Networks with the best augmentation}}
\begin{tabular}{|l|c|c|}
\hline
Network & AP &  MAP\\
\hline\hline
DenseNet161 & 75.26 & \textbf{68.60} \\
ResNet152& 74.08 & 67.25\\
VGG-19 & \textbf{75.34} & 67.81 \\
VGG-16&75.29&67.39\\
RAPID & 56.93 & 56.81\\
Multi-Patch&69.78&64.07\\
\hline
\end{tabular}
\label{tab:allNetStyle}
\end{table}
%\begin{figure*}
%\begin{center}
%\fbox{\rule{0pt}{5in} \rule{\linewidth}{0pt}}
%\end{center}
%   \caption{Scores for 10 crops for $T_{rc}$ and $T_{rnc}$ }
%\label{fig:short}
%\end{figure*}
\subsubsection{Class-wise Precision Scores }
In Table~\ref{ref:PerClassPrecision} we report per-class precision scores for all the $25$ combinations. From the table we observe that the precision scores for HDR, Motion Blur, Rule of Thirds, Soft Focus are lower in comparison to other attributes. However, the poor performance of  HDR, Motion Blur and Soft Focus could be a result of less number of samples available for training. But in spite of having sufficient samples, the Rule of Thirds perform poorly. We argue that this was because of the loss of information during data augmentation. 
In Figure~\ref{fig:classWisePrecision}, we plot the precision for Rule of Thirds for all augmentations. We observe that the per class precision goes highest in $T_w-T_{w}$ augmentation strategy \textit{i.e} warping, which supports the intuitive claim that warping preserves global information better than cropping. 
%For the three other attributes in Figure~\ref{fig:classWisePrecision}, which are primarily appearance-based, cropping strategies work better. We assume, that adding more data to these classes will improve performance. 
\begin{table*}
\caption{\textbf{Per Class Precision Score for 25 augmentation strategies :} The first $2$ rows correspond to the style attributes and the numebr of samples available for training the CNN. The following $25$ rows are divided into $5$ blocks corresponding to the five augmentation strategies in the same order as in Table~\ref{tab:allAug}. We observe that the precision scores for HDR, Motion Blur, Rule of Thirds, Soft Focus are lower in comparison to other attributes. We argue that the poor performance of  HDR, Motion Blur and Soft Focus could be a result of less number of samples available for training.}. 
\centering
\resizebox{\textwidth}{!}{
\begin{tabular}{|p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}p{1cm}|}
\hline
\multicolumn{14}{|c|}{\textbf{ClassLabels}}\\
 %1& 2 & 3 & 4& 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 & 13 & 14\\
Comp Colors & Duo-tones &HDR&Image Grain&Light On White&Long Exposure&Macro&Motion Blur&Negative Image&Rule of Thirds&Shallow DOF& Silhoue-ttes &Soft Focus & Vanish-ing Point\\
%\hline
\multicolumn{14}{|c|}{\textbf{Number of samples available per class for training}}\\
760&1041&317&672&960&676&1359&488&768&1112&1184&825&568&540\\
\hline
\multicolumn{14}{c}{Precision Scores}\\
\hline
55.58&65.66&51.87&75.25&71.34&55.34&56.45&46.01&72.85&29.85&66.95&85.74&39.91&56.03\\
55.46&69.67&56.65&37.10&73.37&51.54&51.05&44.96&74.72&24.74&67.23&87.54&31.05&66.50\\
59.12&75.32&62.41&82.16&78.72&58.28&61.66&58.32&81.24&23.32&70.29&91.68&45.57&63.96\\
55.34&69.57&56.61&44.95&73.29&51.69&52.36&48.02&75.71&26.24&68.82&89.17&35.76&65.70\\
59.45&74.67&67.83&67.10&76.43&59.46&59.23&54.29&81.53&26.64&71.44&91.65&43.76&67.78\\
\hline\hline
46.90&65.19&54.93&44.36&68.64&46.90&46.65&44.25&73.77&24.75&63.09&80.13&26.68&53.20\\
56.10&72.86&69.93&65.88&75.70&57.84&56.48&55.56&83.18&32.60&69.43&88.61&36.27&72.14\\
53.91&74.82&60.37&50.97&80.65&54.31&53.07&52.53&81.94&23.70&69.72&88.99&30.00&59.83\\
57.18&73.52&68.54&66.99&74.64&56.43&54.49&53.81&82.44&30.21&68.51&89.18&40.02&70.96\\
58.95&77.64&72.51&76.20&79.87&58.32&58.15&56.72&85.62&27.76&71.60&90.83&39.18&70.19\\
\hline\hline
55.22&74.94&57.43&80.99&72.45&55.17&58.81&52.94&76.66&25.15&75.03&87.51&41.50&57.56\\
54.73&80.46&69.21&47.32&79.23&52.03&55.81&48.48&83.80&27.28&77.48&89.47&34.96&65.93\\
59.71&81.12&69.81&84.91&80.40&58.01&63.75&61.24&83.80&27.92&78.87&91.31&46.42&63.39\\
56.13&79.33&72.33&53.96&78.69&51.49&57.41&51.08&82.46&26.38&78.14&90.23&39.42&67.23\\
59.34&81.78&74.16&71.57&80.55&56.94&61.64&57.04&83.82&27.46&79.72&92.05&45.00&69.77\\
\hline\hline
53.15&67.70&46.46&46.82&69.59&51.44&52.66&45.26&67.79&25.23&63.21&80.99&32.04&50.34\\
59.26&76.53&64.06&68.22&80.95&56.35&62.04&54.12&77.21&30.51&75.80&89.48&39.98&68.89\\
60.06&77.39&53.97&50.83&81.32&55.78&58.73&56.06&79.52&23.30&68.61&89.09&37.89&61.94\\
60.05&74.83&63.55&70.82&78.60&57.16&62.16&52.92&76.70&31.22&75.60&89.92&40.29&66.63\\
61.93&79.75&62.91&79.60&80.52&60.06&63.50&56.93&79.96&27.78&75.32&91.38&42.02&67.28\\
\hline\hline
55.72&69.79&53.06&70.74&73.22&54.82&59.98&50.52&75.88&27.06&73.79&86.87&41.67&56.39\\
59.96&75.70&66.99&67.01&82.55&57.83&60.88&49.95&83.50&30.74&79.80&91.16&41.89&68.04\\
60.89&76.69&61.84&77.13&82.03&58.24&64.14&56.91&83.32&28.00&79.08&91.78&44.83&64.76\\
61.29&75.13&68.79&72.54&81.17&56.42&61.82&52.09&82.59&29.67&80.36&92.17&42.30&69.61\\
62.36&79.62&69.50&81.96&81.74&59.32&65.16&55.71&84.37&30.50&81.20&93.42&46.36&69.29\\
\hline
\end{tabular}
}
\label{ref:PerClassPrecision}
\end{table*}

\begin{figure}
\begin{center}
%\resizebox{\textwidth}{!}{
\fbox{
\begin{tabular}{c}
\includegraphics[width = .4\textwidth]{Rule_of_Thirds}%&
%\includegraphics[width = .5\textwidth]{HDR}\\
%\includegraphics[width = .5\textwidth]{Motion_Blur}&
%\includegraphics[width = .5\textwidth]{Soft_Focus}
\end{tabular}
}
%}
\end{center}
   \caption{\textbf{Precision scores for Rule of Thirds for 25 augmentation combinations :} There are 25 bins along $x$-axis, each one corresponding to an attribute in Table~\ref{tab:allAug}, in order. The $y$ axis corresponds to the precision scores. The blue coloured bin is the bin with max score. In this figure, we observe that the $T_w-T_w$ combination works best for Rule of Thirds, which supports the intuition that global properties are preserved in warping. }
\label{fig:classWisePrecision}
\end{figure}

\subsubsection{Overfitting}
\begin{figure*}
\resizebox{.8\textwidth}{!}{
\includegraphics[width=\textwidth]{OverfittingProblem}
}
\caption{\textbf{Class-Wise Precision scores for $T_w$, $T_{rc}$ and $T_{rnc}$ :} The $x$-axis correspond to 14 attributes.Each attribute has three bins corresponding to the precision scores for three augmentations. The comparison shows that $T_w$ performs worse and $T_{rnc}$ performs better than $T_{rc}$, in most cases. Only in the two global attributes, Rule of Thirds and Vanishing Lines, $T_w$ performs better, which is consistent with our intuitive assumption. }
\label{fig:overfitting}
\end{figure*}
We observe that although warping performs better in classifying Rule of Thirds, it's over all performance across $14$ classes , is poor. It was also observed training with warping, the network overfits on the training data. Thus we can argue that during training, warping results in poor generalization performance and hence the drop in test precision. We plot the precision scores for all the classes with $T_w$, $T_{rc}$ and $T_{rnc}$ in Figure~\ref{fig:overfitting}.

\subsection{Quality Prediction}
We perform quality estimation of the images from AVA dataset. \textbf{Yet To complete}
\label{sec:ResQualPred}
\begin{table}
\label{tab:allNetQuality}
\centering
\caption{\textbf{Quality Prediction : Different Networks with the best augmentation}}
\begin{tabular}{|l|c|c|}
\hline
Network & AP &  MAP\\
\hline\hline
DenseNet161&---&---\\
ResNet152&---&---\\
VGG-19&---&---\\
VGG-16&---&---\\
RAPID&---&---\\
Multi-Patch&---&---\\
\hline
\end{tabular}
\end{table}
\section{Applications}
\noindent There is a huge scope for an automatic style and quality estimator in the domain of digital photography. Figure~\ref{fig:app}(a)and(b) show our system's correct responses on two famous pictures by Steve McCurry and Burhan Ozbilici, respectively. \\
Interactive and more intelligent cameras, automated photo correction, intelligent photo-organizers are some of the many prospective applications. Such systems could also be useful for developing assistive-technologies like audio-guides . \\
Our system can be directly extended to video-processing for predicting shot-styles. For example, in Figure~\ref{fig:app}(c), we successfully test our system on shot from Majid Majidi's famous movie \textit{The Colors of Paradise}. \\
However, our attribute space is limited to only 14 styles, which is one major drawback. The set of attributes that make a good photograph is non-exhaustive. For example, in Figure~~\ref{fig:app}(d) the photograph is pleasing because of nice repetitive patterns and soft tones. But the predictions of our system are far from the original attributes.
\begin{figure*}
\begin{center}
\fbox{
\resizebox{.65\textwidth}{!}{
\begin{tabular}{c}
\includegraphics[width = .8\textwidth]{Mccurry}\\
(a)\\
\includegraphics[width = .8\textwidth]{Ozbilici}\\
(b)\\
\includegraphics[width = .8\textwidth]{Majidi}\\
(c)\\
\includegraphics[width = .8\textwidth]{RepPatt}\\
(d)
\end{tabular}
}
}
\end{center}
   \caption{Applications }
\label{fig:app}
\end{figure*}
\section{Conclusion}
In this work, we utilize the power of very deep neural networks and advance the state of the art in photographic style-classification. There are several possible directions from here. Building a more generalized model, not limited to a small set of attributes could be one of them. Extending the system to the domain of videos and 360 images could be another. We can hope that this area will become more active in near future with it's challenging and interesting set of problems. 

{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}
 
\end{document}
