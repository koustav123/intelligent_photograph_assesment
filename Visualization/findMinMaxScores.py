import torch
import numpy as np
import sys
import os
from shutil import copyfile
import pdb
#adds the utl folder to the path
sys.path.append('../utils');
import Augmentation
import TestModule

testIds = '/media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl'
testDataPath = '/media/koustav/Naihati/Dataset/AVADataSet/'
testLabels = '/media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab'
styleFile = '/media/koustav/Naihati/Dataset/AVA/style_image_lists/styles.txt';
outputDir = 'Output/MinMax/';
#model = '/media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_VGG19_aug_S_lr_0.001.model'
model = '/media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_ResNet18_aug_RSC_lr_0.001.model';
nMax = 10;
nMin = 10;
aug = 'S';

classes = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/style_image_lists/styles.txt', dtype = str);
classes = [c[1] for c in classes]

if os.path.exists(outputDir):
    os.system('rm -rf '+ outputDir);
os.system('mkdir '+ outputDir);
for c in classes:
    os.system('mkdir '+outputDir+'/'+c);
  
model_ft = torch.load(model);
#pdb.set_trace();
aug = Augmentation.Augmentation(aug);
data_transforms = aug.applyTransforms(crop_size = 224);
t =   TestModule.TestModule(model_ft,data_transforms['val'], testDataPath, testLabels, testIds, False);
goodImages, badImages = t.findMinMaxScores(nMin, nMax);

for c1,(i,j) in enumerate(zip(goodImages, badImages)):
    #pdb.set_trace();
    for c2,(g,b) in enumerate(zip(i,j)):
        copyfile(testDataPath+str(g)+'.jpg', outputDir + classes[c1]+'/GOOD_'+str(g)+'.jpg');
        copyfile(testDataPath+str(b)+'.jpg', outputDir + classes[c1]+'/BAD_'+str(b)+'.jpg');