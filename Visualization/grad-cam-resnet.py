import torch
import argparse
import pdb
import sys
from PIL import Image
from torch.autograd import Variable
import numpy as np
import cv2
from torchvision import models

sys.path.append('../utils');
from GradCam import GradCamResNet
import Augmentation

def show_cam_on_image(img, mask):
	heatmap = cv2.applyColorMap(np.uint8(255*mask), cv2.COLORMAP_JET)
	heatmap = np.float32(heatmap) / 255
	cam = heatmap + np.float32(img)
	cam = cam / np.max(cam)
	cv2.imwrite("cam.jpg", np.uint8(255 * cam))

def preprocess_image(img):
	means=[0.485, 0.456, 0.406]
	stds=[0.229, 0.224, 0.225]

	preprocessed_img = img.copy()[: , :, ::-1]
	for i in range(3):
		preprocessed_img[:, :, i] = preprocessed_img[:, :, i] - means[i]
		preprocessed_img[:, :, i] = preprocessed_img[:, :, i] / stds[i]
	preprocessed_img = np.ascontiguousarray(np.transpose(preprocessed_img, (2, 0, 1)))
	preprocessed_img = torch.from_numpy(preprocessed_img)
	preprocessed_img.unsqueeze_(0)
	input = Variable(preprocessed_img, requires_grad = True)
	return input

def get_args():
    	parser = argparse.ArgumentParser()
    	parser.add_argument('--use-cuda', action='store_true', default=False,
    	                    help='Use NVIDIA GPU acceleration')
    	parser.add_argument('--image-path', type=str, default='/home/koustav/Desktop/IPA/intelligent_photograph_assesment/Visualization/ChessBoard.png',
    	                    help='Input image path')
    	parser.add_argument('--model', type=str, default='/media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_ResNet18_aug_RSC_lr_0.001.model',
    	                    help='model')
    	args = parser.parse_args()
    	args.use_cuda = args.use_cuda and torch.cuda.is_available()
    	if args.use_cuda:
    	    print("Using GPU for acceleration")
    	else:
    	    print("Using CPU for computation")
    
    	return args

if __name__ == '__main__':
    args = get_args()
    model_ft = torch.load(args.model);
    model_ft.train(False);
    #pdb.set_trace();
    aug = Augmentation.Augmentation('S');
    data_transforms = aug.applyTransforms(crop_size = 224);
    grad_cam = GradCamResNet(model = model_ft, \
					target_layer_names = ['layer4:1:residue'], use_cuda=args.use_cuda);
    #grad_cam = GradCamResNet(model = models.resnet18(pretrained = True), \
	#				target_layer_names = ['layer4:1:residue'], use_cuda=args.use_cuda);    
    img = Image.open(args.image_path);
    img_tensor = data_transforms['val'](img);
    img_variable = Variable(img_tensor.cuda()).unsqueeze(0);
    #img_variable = Variable(img_tensor).unsqueeze(0);    
    img = cv2.imread(args.image_path, 1)
    img = np.float32(cv2.resize(img, (224, 224))) / 255
    #img_variable = preprocess_image(img)'''
    #
    target_index = 9
    mask = grad_cam(img_variable, target_index)
    #pdb.set_trace();
    show_cam_on_image(img, mask)