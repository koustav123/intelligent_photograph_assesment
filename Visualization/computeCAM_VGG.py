from __future__ import print_function
import torch
from torch.autograd import Variable
from torch.autograd import Function
from torchvision import models
from torchvision import utils
import cv2
import sys
import numpy as np
import argparse
import pdb
import numpy as np
import os
from PIL import Image

from shutil import copyfile

sys.path.append('../utils');
import Augmentation

rootdir = "/home/koustav/Desktop/IPA/intelligent_photograph_assesment/Visualization/Output/MinMax/"
dataset = 'VGG'
styleFile = '/media/koustav/Naihati/Dataset/AVA/style_image_lists/styles.txt';
model = '/media/koustav/Naihati/CNN_Models/Style/net_VGG19_aug_RSC_lr_0.001.model';

model_ft = torch.load(model);



def brute_predict(img_tensor):
        img_variable = Variable(img_tensor.cuda());
        outputs = model_ft(img_variable.unsqueeze(0));
        return outputs.data.cpu().numpy()[0];





class FeatureExtractor():
    """ Class for extracting activations and 
    registering gradients from targetted intermediate layers """
    def __init__(self, model, target_layers):
        self.model = model
        self.target_layers = target_layers
        self.gradients = []

    def save_gradient(self, grad):
    	self.gradients.append(grad)

    def __call__(self, x):
        outputs = []
        self.gradients = []
        for name, module in self.model._modules.items():
            x = module(x)
            if name in self.target_layers:
            	x.register_hook(self.save_gradient)
                outputs += [x]
        return outputs, x

class ModelOutputs():
	""" Class for making a forward pass, and getting:
	1. The network output.
	2. Activations from intermeddiate targetted layers.
	3. Gradients from intermeddiate targetted layers. """
	def __init__(self, model, target_layers):
		self.model = model
		self.feature_extractor = FeatureExtractor(self.model.features, target_layers)

	def get_gradients(self):
		return self.feature_extractor.gradients

	def __call__(self, x):
		target_activations, output  = self.feature_extractor(x)
		output = output.view(output.size(0), -1)
		output = self.model.classifier(output)
		return target_activations, output

def preprocess_image(img):
	means=[0.485, 0.456, 0.406]
	stds=[0.229, 0.224, 0.225]

	preprocessed_img = img.copy()[: , :, ::-1]
	for i in range(3):
		preprocessed_img[:, :, i] = preprocessed_img[:, :, i] - means[i]
		preprocessed_img[:, :, i] = preprocessed_img[:, :, i] / stds[i]
	preprocessed_img = \
		np.ascontiguousarray(np.transpose(preprocessed_img, (2, 0, 1)))
	preprocessed_img = torch.from_numpy(preprocessed_img)
	preprocessed_img.unsqueeze_(0)
	input = Variable(preprocessed_img, requires_grad = True)
	return input

def show_cam_on_image(img, mask, path):
	heatmap = cv2.applyColorMap(np.uint8(255*mask), cv2.COLORMAP_JET)
	heatmap = np.float32(heatmap) / 255
	cam = heatmap + np.float32(img)
	cam = cam / np.max(cam)
	cv2.imwrite(path, np.uint8(255 * cam))

class GradCam:
	def __init__(self, model, target_layer_names, use_cuda):
		self.model = model
        	self.model.eval()
	        self.cuda = use_cuda
	        if self.cuda:
	            self.model = model.cuda()

		self.extractor = ModelOutputs(self.model, target_layer_names)

	def forward(self, input):
		return self.model(input)
  
	def predict(self, input):
         if self.cuda:
			features, output = self.extractor(input.cuda())
         else:
			features, output = self.extractor(input)
#         index = np.argmax(output.cpu().data.numpy())   
         
         classes = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/style_image_lists/styles.txt', dtype = str);
         classes = [c[1] for c in classes]
         return(output.cpu().data.numpy());
  
  
	def __call__(self, input, index = None):
		if self.cuda:
			features, output = self.extractor(input.cuda())
		else:
			features, output = self.extractor(input)

		if index == None:
			index = np.argmax(output.cpu().data.numpy())
		classes = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/style_image_lists/styles.txt', dtype = str);
		classes = [c[1] for c in classes]
		#print ("Class : %s"%(classes[index]));

		one_hot = np.zeros((1, output.size()[-1]), dtype = np.float32)
		one_hot[0][index] = 1
		one_hot = Variable(torch.from_numpy(one_hot), requires_grad = True)
		if self.cuda:
			one_hot = torch.sum(one_hot.cuda() * output)
		else:
			one_hot = torch.sum(one_hot * output)

		self.model.features.zero_grad()
		self.model.classifier.zero_grad()
		one_hot.backward(retain_variables=True)

		grads_val = self.extractor.get_gradients()[-1].cpu().data.numpy()

		target = features[-1]
		target = target.cpu().data.numpy()[0, :]

		weights = np.mean(grads_val, axis = (2, 3))[0, :]
		cam = np.ones(target.shape[1 : ], dtype = np.float32)

		for i, w in enumerate(weights):
			cam += w * target[i, :, :]

		cam = np.maximum(cam, 0)
		cam = cv2.resize(cam, (224, 224))
		cam = cam - np.min(cam)
		cam = cam / np.max(cam)
		return cam

class GuidedBackpropReLU(Function):

    def forward(self, input):
        positive_mask = (input > 0).type_as(input)
        output = torch.addcmul(torch.zeros(input.size()).type_as(input), input, positive_mask)
        self.save_for_backward(input, output)
        return output

    def backward(self, grad_output):
        input, output = self.saved_tensors
        grad_input = None

        positive_mask_1 = (input > 0).type_as(grad_output)
        positive_mask_2 = (grad_output > 0).type_as(grad_output)
        grad_input = torch.addcmul(torch.zeros(input.size()).type_as(input), torch.addcmul(torch.zeros(input.size()).type_as(input), grad_output, positive_mask_1), positive_mask_2)

        return grad_input

class GuidedBackpropReLUModel:
	def __init__(self, model, use_cuda):
		self.model = model
		self.model.eval()
		self.cuda = use_cuda
		if self.cuda:
			self.model = model.cuda()

		# replace ReLU with GuidedBackpropReLU
		for idx, module in self.model.features._modules.items():
			if module.__class__.__name__ == 'ReLU':
				self.model.features._modules[idx] = GuidedBackpropReLU()

	def forward(self, input):
		return self.model(input)

	def __call__(self, input, index = None):
		if self.cuda:
			output = self.forward(input.cuda())
		else:
			output = self.forward(input)

		if index == None:
			index = np.argmax(output.cpu().data.numpy())

		one_hot = np.zeros((1, output.size()[-1]), dtype = np.float32)
		one_hot[0][index] = 1
		one_hot = Variable(torch.from_numpy(one_hot), requires_grad = True)
		if self.cuda:
			one_hot = torch.sum(one_hot.cuda() * output)
		else:
			one_hot = torch.sum(one_hot * output)

		# self.model.features.zero_grad()
		# self.model.classifier.zero_grad()
		one_hot.backward(retain_variables=True)

		output = input.grad.cpu().data.numpy()
		output = output[0,:,:,:]

		return output

def get_args():
    	parser = argparse.ArgumentParser()
    	parser.add_argument('--use-cuda', action='store_true', default=False,
    	                    help='Use NVIDIA GPU acceleration')
    	args = parser.parse_args()
    	args.use_cuda = args.use_cuda and torch.cuda.is_available()
    	'''if args.use_cuda:
    	    print("Using GPU for acceleration")
    	else:
    	    print("Using CPU for computation")'''
    
    	return args

if __name__ == '__main__':
    if os.path.exists('Output/AVA/'):
        os.system('rm -rf '+ 'Output/AVA/');
        os.system('rm -rf '+ 'Output/Vis.html');
    with open('Output/Vis_VGG.html', 'wb') as f1:
        with open('Vis_1.html','rb') as f2:
            print (f2.read(), file = f1);
        #print (os.system('cat Visual.html'),file = f);
    args = get_args()
    model_ft = torch.load(model);
    layers = [0,1, 3, 6, 8, 11, 13, 15, 17, 20, 22, 24, 26, 29, 31, 33, 35];
    #layers = ['Input', 1, 20, 35];
    classes = np.loadtxt(styleFile, dtype = str);
    classes = [c[1] for c in classes]
    aug = Augmentation.Augmentation('S');
    data_transforms = aug.applyTransforms(crop_size = 224)['val'];
    os.mkdir('Output/'+dataset);
    for index, c in enumerate(classes):
        imageList = [];
        scoreList = [];
        print (c);
        os.mkdir('Output/'+dataset+'/'+c);
        fileList = os.listdir(rootdir+'/'+c);
        fileList.sort();
        #pdb.set_trace();
        #randsIndexes = np.random.randint(0,len(fileList), 6);
        randsIndexes = range(len(fileList));
        for idx in randsIndexes:
            layerList = [];
            src = rootdir+'/'+c+'/'+fileList[idx];
            dst = 'Output/'+dataset+'/'+c+'/'+fileList[idx].replace('.jpg','')+'_0.jpg';
	    img = Image.open(src);            
	    im2Write = cv2.imread(src, 1)
            im2Write = np.float32(cv2.resize(im2Write, (224, 224)));
            #grad_cam_1 = GradCam(model = model_ft, target_layer_names = ['35'], use_cuda=args.use_cuda);
	    #pdb.set_trace();
            input_1 = data_transforms(img);
            scores = brute_predict(input_1);
            #pdb.set_trace();
            scoreList.append(scores[index]);
            cv2.imwrite(dst, np.uint8(im2Write))
            #copyfile(src,dst);
            layerList.append(dst);
            #copyfile(src,dst);
            for lr in layers[1:]:
                grad_cam = GradCam(model = model_ft, target_layer_names = [str(lr)], use_cuda=args.use_cuda);
                img = cv2.imread(src, 1)
                img = np.float32(cv2.resize(img, (224, 224))) / 255
                input = preprocess_image(img)
                target_index = index;
                mask = grad_cam(input, target_index)
                show_cam_on_image(img, mask, dst.replace('_0.jpg','')+'_'+str(lr)+'.jpg' );
                #gb_model = GuidedBackpropReLUModel(model = models.vgg19(pretrained=True), use_cuda=args.use_cuda)
                #gb_model = GuidedBackpropReLUModel(model = model_ft, use_cuda=args.use_cuda)
                #pdb.set_trace();
                #gb = gb_model(input, index=target_index)
                layerList.append(dst.replace('_0.jpg','')+'_'+str(lr)+'.jpg');
                #utils.save_image(torch.from_numpy(gb), dst.replace('_0.jpg','')+'_'+str(lr)+'.jpg')
	    #pdb.set_trace()
            imageList.append(layerList);
        #pdb.set_trace();
        with open('Output/Vis_VGG.html', 'a') as f1:
            print('<div data-role="collapsible">',file = f1);
            print ('<h1>'+c+'</h1>',file = f1);
            print("<table style = \"font-size:20;width:90%\">", file = f1);
            print('<tr><th>Layers</th><th colspan="6">Samples</th></tr>',file = f1);
            print('<tr><td></td><td>%0.2f</td><td>%0.2f</td><td>%0.2f</td><td>%0.2f</td><td>%0.2f</td><td>%0.2f</td></tr>'%(scoreList[0],scoreList[1],scoreList[2],scoreList[3],scoreList[4],scoreList[5]),file = f1);
            for i in range (17):
                #pdb.set_trace();
                print ('<tr><td>%s</td><td><img src = %s></td><td><img src = %s></td><td><img \
                src = %s></td><td><img src = %s></td><td><img src = %s></td><td><img src = %s></td></tr>'%(layers[i], 
                imageList[0][i],imageList[1][i],imageList[2][i],imageList[3][i],imageList[4][i], imageList[5][i]), file = f1 )
	    #pdb.set_trace();
            print('</table></div>',file = f1)
    with open('Output/Vis_VGG.html', 'a') as f1:
        with open('Vis_2.html','rb') as f3:
            print (f3.read(), file = f1);    
        #pdb.set_trace();
        #print fileList
			
			
			
