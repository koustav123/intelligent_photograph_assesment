import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
import pdb
import torch
import math
from collections import OrderedDict

'''class Net_MLP(nn.Module):

    def __init__(self, nClasses = 3):
        super(Net_MLP, self).__init__()
        
        self.hidden_1 = nn.Linear(64*64*3, 64*64*3 );
        self.hidden_2 = nn.Linear(64*64*3, 64*64*3);
        self.hidden_3 = nn.Linear(64*64*3, 64*64*3);
        self.classifier = nn.Linear(64*64*3, 1000);

    def forward(self, x):
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.hidden_1(x))
        x = F.relu(self.hidden_2(x))
        x = F.relu(self.hidden_3(x))
        x = self.classifier(x);
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

def conv3x3(in_planes, out_planes, stride=1):
    "3x3 convolution with padding"
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)

        
class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out'''
        
def weights_freeze(model):
    for param in model.parameters():
        param.requires_grad = False        

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)
        
class ResNet_Conv(nn.Module):

    def __init__(self, dr = 0.0, pretrained = False, freeze = False, nLayers = 18, pooling = False):
        super(ResNet_Conv, self).__init__()
        if pretrained:
            self.resnet=self.init_model(nLayers, True);
            if freeze:
                self.resnet.apply(weights_freeze)
                '''self.resnet.conv1.requires_grad=False;
                self.resnet.layer1.requires_grad=False;
                self.resnet.layer2.requires_grad=False
                self.resnet.layer3.requires_grad=False
                self.resnet.layer4.requires_grad=False'''
        else:
            self.resnet=self.init_model(nLayers, False);
        self.dr = dr;
        self.pooling = pooling
        self.downsampler = nn.Conv2d(64, 64, kernel_size=7, stride=2, padding=3)
        self.downsampler.apply(weights_init)
    
    
    def init_model(self, nLayers, pretrained):
        if nLayers == 18:
            return models.resnet18(pretrained);
        if nLayers == 34:
            return models.resnet34(pretrained);
        if nLayers == 50:
            return models.resnet50(pretrained);
        if nLayers == 101:
            return models.resnet101(pretrained);
        elif nLayers == 152:
            return models.resnet152(pretrained);
        else:
            print ("Incorrect Layers in ResNet : %d. Please specify 18,34,50,101,152"%(nLayers));
            
    def forward(self, x):
        #pdb.set_trace();
        dropout_layer = nn.Dropout(p = self.dr);
        dropout_layer_0 = nn.Dropout(p = 0.2 if self.dr >= 0.2 else 0.0);
        
        x = dropout_layer_0(x);
        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        #pdb.set_trace()
        if self.pooling:
            x = self.resnet.maxpool(x)
        else:
            x = self.downsampler(x)
        
        x = dropout_layer(x);
        
        x = self.resnet.layer1(x)
        x = dropout_layer(x);
        
        x = self.resnet.layer2(x)
        x = dropout_layer(x);
        
        x = self.resnet.layer3(x)
        x = dropout_layer(x);
        
        x = self.resnet.layer4(x)
        x = dropout_layer(x);


        return x

class DenseNet_Conv(nn.Module):

    def __init__(self, dr = 0.0, pretrained = False, freeze = False, nLayers = 161):
        super(DenseNet_Conv, self).__init__()
        if pretrained:
            self.densenet = self.init_model(nLayers, True);
            if freeze:
                #self.densenet.features.requires_grad=False;
                self.densenet.features.apply(weights_freeze)
        else:
            self.densenet=self.init_model(nLayers, False);
        self.dr = dr;
        
    
    
    def init_model(self, nLayers, pretrained):
        if nLayers == 161:
            return models.densenet161(pretrained);
        else:
            print ("Incorrect Layers in DenseNet : %d. Please specify 161"%(nLayers));
            
    def forward(self, x):
        features = self.densenet.features(x)
        return features;


class VGG_Conv(nn.Module):

    def __init__(self, dr = 0.0, pretrained = False, freeze = False, nLayers = 19):
        super(VGG_Conv, self).__init__()
        if pretrained:
            self.vgg = self.init_model(nLayers, True);
            if freeze:
                self.vgg.features.requires_grad=False;
        else:
            self.vgg = self.init_model(nLayers, False);
        self.dr = dr;
        
    
    
    def init_model(self, nLayers, pretrained):
        if nLayers == 19:
            return models.vgg19(pretrained);
        else:
            print ("Incorrect Layers in vgg : %d. Please specify 19"%(nLayers));
            
    def forward(self, x):
        features = self.vgg.features(x)
        return features;


       
class ResNet18_With_Dropouts(nn.Module):

    def __init__(self, dr = .5, pretrained = False, freeze = False):
        super(ResNet18_With_Dropouts, self).__init__()
        if pretrained:
            self.resnet=models.resnet18(True);
            if freeze:
                self.resnet.conv1.requires_grad=False;
                self.resnet.layer1.requires_grad=False;
                self.resnet.layer2.requires_grad=False;
                self.resnet.layer3.requires_grad=False;
                self.resnet.layer4.requires_grad=False;
        else:
            self.resnet=models.resnet18(False)
        self.dr = dr;
        

    def forward(self, x):
        #pdb.set_trace();
        dropout_layer = nn.Dropout(p = self.dr);
        dropout_layer_0 = nn.Dropout(p = 0.2 if self.dr >= 0.2 else 0.0);
        
        x = dropout_layer_0(x);

        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        x = self.resnet.maxpool(x)
        x = dropout_layer(x);
        
        x = self.resnet.layer1(x)
        x = dropout_layer(x);

        x = self.resnet.layer2(x)
        x = dropout_layer(x);

        x = self.resnet.layer3(x)
        x = dropout_layer(x);

        x = self.resnet.layer4(x)
        x = dropout_layer(x);
        #pdb.set_trace();
        x = self.resnet.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.resnet.fc(x)

        return x


'''class TCD_Sal_Net(nn.Module):
    def __init__(self, block, layers, num_classes=1000):
        self.inplanes = 32
        super(TCD_Sal_Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(32)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 32, layers[0])
        self.layer2 = self._make_layer(block, 32, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 32, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 64, layers[3], stride=2)
        self.avgpool = nn.AvgPool2d(7)
        self.fc = nn.Linear(64 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
        
    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)
        
        
    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        #x = self.avgpool(x)
        #x = x.view(x.size(0), -1)
        #x = self.fc(x)

        return x
        
        
class TCD_Sal_Net_2():

    def __init__(self, dr = .5, pretrained = False):
        super(TCD_Sal_Net_2, self).__init__()
        if pretrained:
            self.resnet=models.resnet18(True);
            self.resnet.conv1.requires_grad=False;
            self.resnet.bn1.requires_grad=False;
            self.resnet.relu.requires_grad=False;
            self.resnet.maxpool.requires_grad=False;
            self.resnet.layer1.requires_grad=False;
            self.resnet.layer2.requires_grad=False
            self.resnet.layer3.requires_grad=False
            self.resnet.layer4.requires_grad=False
        else:
            self.resnet=models.resnet18(False)
        self.dr = dr;
        

    def forward(self, x):
        #pdb.set_trace();
        dropout_layer = nn.Dropout(p = self.dr);
        #dropout_layer_0 = nn.Dropout(p = 0.2);
        #x = dropout_layer_0(x);
        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        x = self.resnet.maxpool(x)
        x = dropout_layer(x);
        
        x = self.resnet.layer1(x)
        x = dropout_layer(x);
        x = self.resnet.layer2(x)
        x = dropout_layer(x);
        x = self.resnet.layer3(x)
        x = dropout_layer(x);
        x = self.resnet.layer4(x)
        x = dropout_layer(x);
        #pdb.set_trace();
        #x = self.resnet.avgpool(x)
        #x = x.view(x.size(0), -1)
        #x = self.resnet.fc(x)

        return x
        
class TCD_ResNet_Column_1(nn.module):

    def __init__(self, dr = 0.5, pretrained = False):
        super(TCD_ResNet_Column_1,self).__init__()
        if pretrained:
            self.resnet=models.resnet18(True);
        else:
            self.resnet=models.resnet18(False)
            
        self.resnet.avgpool = None;
        self.resnet.fc = None;
        self.resnet.conv1.requires_grad=False;
        self.resnet.bn1.requires_grad=False;
        self.resnet.relu.requires_grad=False;
        self.resnet.maxpool.requires_grad=False;
        self.resnet.layer1.requires_grad=False;
        self.resnet.layer2.requires_grad=False
        self.resnet.layer3.requires_grad=False
        self.resnet.layer4.requires_grad=False
            
    def forward(self, x):

        dropout_layer = nn.Dropout(p = self.dr);
        dropout_layer_0 = nn.Dropout(p = 0.2);
        x = dropout_layer_0(x);
        x = self.resnet.conv1(x)
        x = self.resnet.bn1(x)
        x = self.resnet.relu(x)
        x = self.resnet.maxpool(x)
        x = dropout_layer(x);
        
        x = self.resnet.layer1(x)
        x = dropout_layer(x);
        x = self.resnet.layer2(x)
        x = dropout_layer(x);
        x = self.resnet.layer3(x)
        x = dropout_layer(x);
        x = self.resnet.layer4(x)
        x = dropout_layer(x);
        
        
        return x;'''
        
class TCD_ResNet_With_Saliecy(nn.Module):

    def __init__(self, dr = 0.0, col_1_pretrained = True, freeze_1 = False, nLayers_1 = 18,col_2_pretrained = True, 
                 freeze_2 = False, nLayers_2 = 18, pooling = False):
        super(TCD_ResNet_With_Saliecy, self).__init__()
        #pdb.set_trace();
        self.col_1 = ResNet_Conv(dr, col_1_pretrained, freeze_1, nLayers_1, pooling );
        self.col_2 = ResNet_Conv(dr, col_2_pretrained , freeze_2, nLayers_2, pooling );

        '''self.fc_1 = nn.Linear(1024, 512);
        self.bn = nn.BatchNorm1d(512);
        self.relu = nn.ReLU(inplace=True);
        self.fc_2 = nn.Linear(512, 14);'''
        self.fc = nn.Linear(self.col_1.resnet.fc.in_features + self.col_2.resnet.fc.in_features, 14);
        self.avgpool = nn.AvgPool2d(7);     

        

    def forward(self, features):
        #pdb.set_trace();
        chunks = features.chunk(2,1);
        x = chunks[0];
        z = chunks[1];
        

        x = self.col_1(x);
        z = self.col_2(z);
        #pdb.set_trace();
        y = torch.cat((x,z),1);
        y = self.avgpool(y);
        y = y.view(y.size(0), -1);
        #y = self.fc_1(y);
        #y = self.bn(y);
        #y = self.relu(y);
        #y = self.fc_2(y);
        y = self.fc(y);
        return y

class TCD_DenseNet_With_Saliecy(nn.Module):

    def __init__(self, dr = 0.0, col_1_pretrained = True, freeze_1 = False, nLayers_1 = 161,col_2_pretrained = True, 
                 freeze_2 = False, nLayers_2 = 161):
        super(TCD_DenseNet_With_Saliecy, self).__init__()
        #pdb.set_trace();
        self.col_1 = DenseNet_Conv(dr, col_1_pretrained, freeze_1, nLayers_1 );
        self.col_2 = DenseNet_Conv(dr, col_2_pretrained , freeze_2, nLayers_2);

        '''self.fc_1 = nn.Linear(1024, 512);
        self.bn = nn.BatchNorm1d(512);
        self.relu = nn.ReLU(inplace=True);
        self.fc_2 = nn.Linear(512, 14);'''
        self.classifier = nn.Linear(self.col_1.densenet.classifier.in_features + self.col_2.densenet.classifier.in_features, 14);

        

    def forward(self, features):
        #pdb.set_trace();
        chunks = features.chunk(2,1);
        x = chunks[0];
        z = chunks[1];
        

        x = self.col_1(x);
        z = self.col_2(z);
        y = torch.cat((x,z),1);
        y = F.relu(y, inplace=True)
        y = F.avg_pool2d(y, kernel_size=7, stride=1).view(y.size(0), -1)
        y = self.classifier(y);
        return y

class TCD_VGG_With_Saliecy(nn.Module):

    def __init__(self, dr = 0.0, col_1_pretrained = True, freeze_1 = False, nLayers_1 = 19,col_2_pretrained = True, 
                 freeze_2 = False, nLayers_2 = 19):
        super(TCD_VGG_With_Saliecy, self).__init__()
        #pdb.set_trace();
        self.col_1 = VGG_Conv(dr, col_1_pretrained, freeze_1, nLayers_1 );
        self.col_2 = VGG_Conv(dr, col_2_pretrained , freeze_2, nLayers_2);

        '''self.fc_1 = nn.Linear(1024, 512);
        self.bn = nn.BatchNorm1d(512);
        self.relu = nn.ReLU(inplace=True);
        self.fc_2 = nn.Linear(512, 14);'''
        #self.classifier = nn.Linear(self.col_1.vgg.classifier._modules['6'].in_features + self.col_2.vgg.classifier._modules['6'].in_features, 14);
        
        self.classifier = nn.Sequential(
            nn.Linear(self.col_1.vgg.classifier._modules['0'].in_features + self.col_2.vgg.classifier._modules['0'].in_features, 4096),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(4096, 14),
        )        

        

    def forward(self, features):
        #pdb.set_trace();
        chunks = features.chunk(2,1);
        x = chunks[0];
        z = chunks[1];
        

        x = self.col_1(x);
        z = self.col_2(z);
        y = torch.cat((x,z),1);
        y = y.view(y.size(0), -1);
        y = self.classifier(y);
        return y

class TCD_Residual_ResNet(nn.Module):
    def __init__(self, dr = 0.0, pretrained = True, freeze = False, pooling  = True):
        super(TCD_Residual_ResNet, self).__init__()
        self.net = models.resnet152(pretrained)
        self.gssm_conv_1 = nn.Sequential(OrderedDict([('conv', nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3)),
                                                    ('bn', nn.BatchNorm2d(64)),
                                                    ('relu',nn.ReLU(inplace=True))]))
        
        self.gssm_conv_2 = nn.Sequential(OrderedDict([('conv', nn.Conv2d(64, 256, kernel_size=7, stride=1, padding=3)),
                                                    ('bn', nn.BatchNorm2d(256)),
                                                    ('relu',nn.ReLU(inplace=True))]))        

        self.gssm_conv_3 = nn.Sequential(OrderedDict([('conv', nn.Conv2d(256, 512, kernel_size=7, stride=2, padding=3)),
                                                    ('bn', nn.BatchNorm2d(512)),
                                                    ('relu',nn.ReLU(inplace=True))]))

        self.gssm_conv_4 = nn.Sequential(OrderedDict([('conv', nn.Conv2d(512, 1024, kernel_size=7, stride=2, padding=3)),
                                                    ('bn', nn.BatchNorm2d(1024)),
                                                    ('relu',nn.ReLU(inplace=True))]))
        
        self.gssm_conv_5 = nn.Sequential(OrderedDict([('conv', nn.Conv2d(1024, 2048, kernel_size=7, stride=2, padding=3)),
                                                    ('bn', nn.BatchNorm2d(2048)),
                                                    ('relu',nn.ReLU(inplace=True))]))

            
        self.pooling = pooling
        self.downsampler = nn.Conv2d(64, 64, kernel_size=7, stride=2, padding=3)
        
        for model in [self.gssm_conv_1, self.gssm_conv_2, self.gssm_conv_3, self.gssm_conv_4, self.gssm_conv_5, self.downsampler]:
            model.apply(weights_init)  
        
        
        
    def forward(self, features):
        #pdb.set_trace()
        chunks = features.chunk(2,1);
        #chunk out GSSM
        x = chunks[0];
        z = chunks[1];
        
        #convolve RGB        
        x = self.net.conv1(x)
        x = self.net.bn1(x)
        x = self.net.relu(x)
        
        #convolve GSSM
        z = self.gssm_conv_1(z)
        z = F.max_pool2d(z, kernel_size=3, stride=2, padding=1)
        
        
        if self.pooling:
            x = self.net.maxpool(x)
        else:
            x = self.downsampler(x)
        
        #add residue
        x = x+z
        
        x = self.net.layer1(x)
        z = self.gssm_conv_2(z)
        x = x+z
        
        x = self.net.layer2(x)
        z = self.gssm_conv_3(z)
        x = x+z
        
        x = self.net.layer3(x)
        z = self.gssm_conv_4(z)
        x = x+z

        x = self.net.layer4(x)
        z = self.gssm_conv_5(z)
        x = x+z

        x = self.net.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.net.fc(x)

        return x        
        
class ResNet_Direct_Saliency(nn.Module):
    def __init__(self, dr = 0.0, pretrained = True, freeze = False, pooling  = True):
        super(ResNet_Direct_Saliency, self).__init__()
        self.net = models.resnet152(pretrained)
        self.pooling = pooling
        self.downsampler = nn.Conv2d(64, 64, kernel_size=7, stride=2, padding=3)
        self.downsampler.apply(weights_init)
        #self.sal_fc_1 = nn.Linear(3136, self.net.fc.in_features)
        #self.sal_batch_norm = nn.BatchNorm2d(self.net.fc.in_features)
        self.fc = nn.Linear(self.net.fc.in_features + 3136 ,14)
        self.net.fc = None
    def forward(self, features):
        #pdb.set_trace()    
        chunks = features.chunk(2,1);
        #chunk out GSSM
        x = chunks[0];
        z = chunks[1];
        
        #convolve RGB        
        x = self.net.conv1(x)
        x = self.net.bn1(x)
        x = self.net.relu(x)

        if self.pooling:
            x = self.net.maxpool(x)
        else:
            x = self.downsampler(x)
            
        x = self.net.layer1(x)
        x = self.net.layer2(x)
        x = self.net.layer3(x)
        x = self.net.layer4(x)
        x = self.net.avgpool(x)
        x = x.view(x.size(0), -1)
        
        #work on saliency
        z = z[:,0,:,:]
        #featuresize 224*224
        z = F.max_pool2d(z, kernel_size=3, stride=2, padding=1)
        z = F.max_pool2d(z, kernel_size=3, stride=2, padding=1)
        #featuresize 56*56
        
        z = z.view(z.size(0), -1)
        #z = self.sal_fc_1(z)
        #z = F.relu(z)
        #z = self.sal_batch_norm(z)
        
        x = torch.cat((x,z),1)
        y = self.fc(x)
        
        return y

class ResNet_Direct_GSSM(nn.Module):
    def __init__(self, dr = 0.0, pretrained = True, freeze = False, pooling  = True):
        super(ResNet_Direct_GSSM, self).__init__()
        self.net = models.resnet152(pretrained)
        self.pooling = pooling
        self.downsampler = nn.Conv2d(64, 64, kernel_size=7, stride=2, padding=3)
        self.downsampler.apply(weights_init)
        self.sal_fc_1 = nn.Linear(9408, self.net.fc.in_features)
        self.sal_batch_norm = nn.BatchNorm2d(self.net.fc.in_features)
        self.fc = nn.Linear(2 * self.net.fc.in_features ,14)
        self.net.fc = None
    def forward(self, features):
        #pdb.set_trace()    
        chunks = features.chunk(2,1);
        #chunk out GSSM
        x = chunks[0];
        z = chunks[1];
        
        #convolve RGB        
        x = self.net.conv1(x)
        x = self.net.bn1(x)
        x = self.net.relu(x)

        if self.pooling:
            x = self.net.maxpool(x)
        else:
            x = self.downsampler(x)
            
        x = self.net.layer1(x)
        x = self.net.layer2(x)
        x = self.net.layer3(x)
        x = self.net.layer4(x)
        x = self.net.avgpool(x)
        x = x.view(x.size(0), -1)
        
        #featuresize 224*224
        z = F.max_pool2d(z, kernel_size=3, stride=2, padding=1)
        z = F.max_pool2d(z, kernel_size=3, stride=2, padding=1)
        #featuresize 56*56
        
        z = z.view(z.size(0), -1)
        z = self.sal_fc_1(z)
        z = self.sal_batch_norm(z)        
        z = F.relu(z)
        
        
        x = torch.cat((x,z),1)
        y = self.fc(x)
        
        return y

def TCD_resnet18(dr,pretrained):
    """Constructs a ResNet-18 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = TCD_ResNet(dr,pretrained)
    return model

def TCD_resnet_with_saliency(dr,pretrained, freeze, pooling, nLayers_1 = 18, nLayers_2 = 18):
    """Constructs a ResNet-18 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = TCD_ResNet_With_Saliecy(dr,pretrained,freeze,nLayers_1,pretrained,freeze,nLayers_2, pooling);
    #model = TCD_ResNet_With_Saliecy(dr,pretrained,freeze,nLayers_1, False, False, nLayers_2);
    #model = nn.DataParallel(model);
    #pdb.set_trace()
    return model

def TCD_resnet18_with_dropout(dr,pretrained, freeze):
    """Constructs a ResNet-18 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = ResNet18_With_Dropouts(dr,pretrained,freeze)
    return model

def TCD_densenet_with_saliency(dr,pretrained, freeze, nLayers_1 = 161, nLayers_2 = 161):
    """Constructs a ResNet-18 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    #model = TCD_ResNet_With_Saliecy(dr,pretrained,freeze,nLayers_1,pretrained,freeze,nLayers_2);
    model = TCD_DenseNet_With_Saliecy(dr,pretrained,freeze,nLayers_1, pretrained, freeze, nLayers_2);
    #model = nn.DataParallel(model);
    return model

def TCD_VGG_with_saliency(dr,pretrained, freeze, nLayers_1 = 19, nLayers_2 = 19):
    """Constructs a ResNet-18 model.
    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    #model = TCD_ResNet_With_Saliecy(dr,pretrained,freeze,nLayers_1,pretrained,freeze,nLayers_2);
    model = TCD_VGG_With_Saliecy(dr,pretrained,freeze,nLayers_1, pretrained, freeze, nLayers_2);
    model = nn.DataParallel(model);
    return model


def residusal_resnet(dr, pretrained, freeze):
    model = TCD_Residual_ResNet(dr, pretrained, freeze)
    #model = nn.DataParallel(model);
    return model
    
def resnet_saliency_direct(dr, pretrained, freeze, pooling):
    model = ResNet_Direct_Saliency(dr, pretrained, freeze, pooling)
    return model    
    
def resnet_gssm_direct(dr, pretrained, freeze, pooling):
    model = ResNet_Direct_GSSM(dr, pretrained, freeze, pooling)
    return model   