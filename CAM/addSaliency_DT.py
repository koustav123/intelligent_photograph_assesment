from PIL import Image
import pdb
import sys
sys.path.append('../utils');
from torchvision import models
import os
import numpy as np
import pickle


def find_DT(w,h, dr = [0,255]):
    x_range = np.linspace(dr[0],dr[1],w);
    y_range = np.linspace(dr[0],dr[1],h);
    x_array, y_array = np.meshgrid(x_range,y_range);
    dt = np.sqrt((x_array**2 + y_array**2)/2);
    return dt
    
    
    
if __name__ == '__main__':
    find_DT(1600,900);