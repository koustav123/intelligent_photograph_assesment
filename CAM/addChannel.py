''' This code adds a CAM cannel to every image of a folder and saves them in another folder having the same name with different extension
'''

from PIL import Image
import pdb
import sys
sys.path.append('../utils');
from GradCam import GradCamResNet
from torchvision import models
import os
import numpy as np
import pickle

def Gaussian2D(size):
    x, y = np.meshgrid(np.linspace(-1,1,size), np.linspace(-1,1,size))
    d = np.sqrt(x*x+y*y)
    sigma, mu = .75, 0.0
    g = np.exp(-( (d-mu)**2 / ( 2.0 * sigma**2 ) ) )
    #print("2D Gaussian-like array:")
    #print(g)
    return g
    
def Gaussian(size):
    '''gauss = np.zeros((size/4,size/4));
    for i in range(gauss.shape[0]):
        for j in range(gauss.shape[1]):
            gauss[i][j] = int(np.sqrt(i**2+j**2));'''
    gauss = Gaussian2D(size/2);       
    full_gauss = np.vstack((
    np.hstack((gauss,gauss)),np.hstack((gauss,gauss))
    ));
    #full_gauss = full_gauss/np.max(full_gauss);
    #full_gauss = np.abs(full_gauss-.9);
    return full_gauss 
    
    



'''input_image = '/home/koustav/Desktop/IPA/intelligent_photograph_assesment/CAM/Data/GOOD_616603.jpg';

img = Image.open(input_image);
img2 = img.resize([224,224]);
img3 = img2.resize(img.size, resample = 2);
img2.save('img2.png')
img3.save('img3.png')'''
#pdb.set_trace();
mean_sal = [];
root_input = '/media/koustav/Naihati/Dataset/AVA/test_1/'
root_output = '/media/koustav/Naihati/Dataset/AVA/test_2/'
dirs = os.listdir(root_input);
saliency_model = models.resnet18(True);
saliency_model.eval();
saliency_model.train(False);

for dirc in dirs:
    #os.system('rm -rf '+ root_output + dirc );
    os.system('mkdir '+ root_output + dirc );
    inputDir = root_input + dirc;
    outputDir = root_output + dirc;

    fileList = os.listdir(inputDir);
    for f in fileList:
        grad_cam = GradCamResNet(model = saliency_model , target_layer_names = ['layer4:1:residue'], use_cuda=False);
        img = Image.open(inputDir+'/'+f);
        img = img.convert('RGB');
        temp_img = img.resize([224,224]);
        #pdb.set_trace()
        #temp_img.show();
        temp_img = np.asarray(temp_img);
        temp_img = np.float32(temp_img)/255;
        #print(temp_img.shape);
        try:
            input = grad_cam.preprocess_image(temp_img);
            mask = grad_cam(input, None);
        except:
            print('Could not process : %s'%(inputDir+'/'+f))
            continue
          
        #Image.fromarray((mask*255).astype(np.uint8)).resize(img.size).show();
        #pdb.set_trace();
        #position_prob = np.arange(0,mask.shape[0]*mask.shape[1],dtype = float);       
        #position_prob = np.reshape(position_prob/np.max(position_prob),mask.shape);
        position_prob = Gaussian(mask.shape[0]);
        #Image.fromarray((position_prob*255).astype(np.uint8)).resize(img.size).show();
        position_saliency = np.multiply(mask,position_prob)*255;
        salimage = Image.fromarray(position_saliency.astype(np.uint8))
        #salimage.resize(img.size).show();
        #pdb.set_trace();
        salimage = np.asarray(salimage.resize(img.size));
        mean_sal.append(np.mean(salimage));
        #print (np.mean(position_saliency))
        new_image = np.float32(np.asarray(img))
        final_image = np.dstack((new_image,salimage))
        final_image = Image.fromarray(final_image.astype(np.uint8));
        #pdb.set_trace();
        final_image.save(outputDir+'/'+f.replace('.jpg','.png'));
        #pickle.dump(final_image, open(outputDir+'/'+f.replace('.jpg','.p'),'wb'));
        #np.savetxt(outputDir+'/'+f.replace('.jpg','.IMG'),final_image, fmt='%0.4f')
        #pdb.set_trace();
        
print('Mean  = %f || STD = %f'%(np.mean(mean_sal),np.std(mean_sal)));
        #exit();
    
    
