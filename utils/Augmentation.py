from __future__ import print_function
#from torchvision import TCD_transforms
import numpy as np
from PIL import Image

import sys

#adds the utl folder to the path
sys.path.append('../utils');

import CropModule
import TCD_transforms
class Augmentation:
    
    def __init__(self,strategy):
        print ("Data Augmentation Initialized with strategy %s"%(strategy));
        self.strategy = strategy;
        
    #def cropAtLocation (self, x,y,z): return lambda p: p[x-z/2:x+z/2,y-z/2:y+z/2]
    
    def cropAtLocation (self, x,y,z): return lambda p : p.crop((x-z/2,y-z/2,x+z/2,y+z/2))
    
    def applyTransforms(self, crop_x = 0, crop_y = 0, crop_size = 224):
        #print (model)
        if self.strategy == "RC": # Random Crops of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.RandomCrop(crop_size),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.RandomCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
        }
        
        elif self.strategy == "RSC": #Random Sized Crops from random locations and then scaled to 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.RandomSizedCrop(crop_size),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.RandomSizedCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
        }
        elif self.strategy == "CC": # Centre crop of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.CenterCrop(crop_size),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
        }
        elif self.strategy == "SC": # Lower dimension Scaled to 224 and then centre cropped
             data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Scale(crop_size),
                TCD_transforms.CenterCrop(crop_size),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Scale(crop_size),
                TCD_transforms.CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
        }
        elif self.strategy == "S": # Dimensions are scaled to 224,224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Scale([crop_size,crop_size]),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Scale([crop_size,crop_size]),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
        }
        elif self.strategy == "UDC": #User defined
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Lambda(self.cropAtLocation(crop_x,crop_y,crop_size)),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Lambda(self.cropAtLocation(crop_x,crop_y,crop_size)),
                #TCD_transforms.RandomCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
        }      
        elif self.strategy == "MLP_SC": # Lower dimension Scaled to 224 and then centre cropped
             crop_size = 64;
             data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Scale(crop_size),
                TCD_transforms.CenterCrop(crop_size),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Scale(crop_size),
                TCD_transforms.CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]),
        }
        
        elif self.strategy == "S_4_C": # Lower dimension Scaled to 224 and then centre cropped
             data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Sal_Scale([crop_size,crop_size]),
                TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.450], [0.229, 0.224, 0.225, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Sal_Scale([crop_size,crop_size]),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.450], [0.229, 0.224, 0.225, 0.225])
            ]),
        }
        
        elif self.strategy == "SC_4_C": # Lower dimension Scaled to 224 and then centre cropped
             data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Sal_Scale(crop_size),
                TCD_transforms.CenterCrop(crop_size),
                #TCD_transforms.RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.450], [0.229, 0.224, 0.225, 0.225])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Sal_Scale(crop_size),
                TCD_transforms.CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.450], [0.229, 0.224, 0.225, 0.225])
            ]),
        }
        
        elif self.strategy == "SAL_S": # Lower dimension Scaled to 224 and then centre cropped
             data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_DT_Scale([crop_size,crop_size]),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_DT_Scale([crop_size,crop_size]),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
     }
     
        elif self.strategy == "SAL_RC": #Random Sized Crops from random locations and then scaled to 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_RandomCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_RandomCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }
        
        elif self.strategy == "SAL_SC": # Lower dimension Scaled to 224 and then centre cropped
             data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_DT_Scale(crop_size),
                TCD_transforms.Sal_CenterCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_DT_Scale(crop_size),
                TCD_transforms.Sal_CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            }

        elif self.strategy == "SAL_RSC": #Random Sized Crops from random locations and then scaled to 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_RandomSizedCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_RandomSizedCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }
        elif self.strategy == "SAL_CC": # Centre crop of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_CenterCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }
        
        elif self.strategy == "RAPID_SAL_CC": # Centre crop of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Rapid_Sal_CenterCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Rapid_Sal_CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }
        
        elif self.strategy == "RAPID_SAL_RC": # Centre crop of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Rapid_Sal_RandomCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Rapid_Sal_RandomCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }

        elif self.strategy == "RAPID_SAL_RSC": # Centre crop of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Rapid_Sal_RandomSizedCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Rapid_Sal_RandomSizedCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }
        elif self.strategy == "RAPID_SAL_SC": # Centre crop of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_DT_Scale(crop_size),
                TCD_transforms.Rapid_Sal_CenterCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_DT_Scale(crop_size),
                TCD_transforms.Rapid_Sal_CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }
       
     
        elif self.strategy == "SAL_RSC_HOUGH": # Centre crop of 224
            data_transforms = {
            'train': TCD_transforms.Compose([
                TCD_transforms.Add_Lines(),
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_CenterCrop(crop_size),
                TCD_transforms.Sal_RandomHorizontalFlip(),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
            'val': TCD_transforms.Compose([
                TCD_transforms.Add_Lines(),
                TCD_transforms.Add_Saliency(),
                TCD_transforms.Sal_CenterCrop(crop_size),
                TCD_transforms.ToTensor(),
                TCD_transforms.Normalize([0.485, 0.456, 0.406, 0.500, 0.500, 0.063], [0.229, 0.224, 0.225, 0.289, 0.289, 0.118 ])
            ]),
        }
        
        
        
        else :
            print ("Please specify correct augmentation strategy RC, RSC, CC, SC, S");
            exit();
            
        return data_transforms;