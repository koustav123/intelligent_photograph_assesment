import numpy as np

def Gaussian2D(size):
    x, y = np.meshgrid(np.linspace(-1,1,size), np.linspace(-1,1,size))
    d = np.sqrt(x*x+y*y)
    sigma, mu = .75, 0.0
    g = np.exp(-( (d-mu)**2 / ( 2.0 * sigma**2 ) ) )
    return g
    
def Gaussian(size):
    gauss = Gaussian2D(size/2);       
    full_gauss = np.vstack((
    np.hstack((gauss,gauss)),np.hstack((gauss,gauss))
    ));
    return full_gauss 
