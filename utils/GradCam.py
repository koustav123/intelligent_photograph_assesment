from __future__ import print_function
import torch
from torch.autograd import Variable
import cv2
import numpy as np
import pdb
import sys
sys.path.append('../utils');

class FeatureExtractor():
    """ Class for extracting activations and 
    registering gradients from targetted intermediate layers """
    def __init__(self, model, target_layers):
        self.model = model
        self.target_layers = target_layers
        self.gradients = []
        #pdb.set_trace();
    def save_gradient(self, grad):
    	self.gradients.append(grad)

    def __call__(self, x):
        outputs = []
        self.gradients = []
        #pdb.set_trace();
        firstBlock= self.model._modules.items()[0:4];
        featureExtractors = self.model._modules.items()[4:-2];
        classifier = self.model._modules.items()[-2:];
        #print ('Feature Extractor called')
        #pdb.set_trace();
        out = x;
        for name, module in firstBlock:
            out = module(out);
            if "fb" in self.target_layers and name == 'relu':
                out.register_hook(self.save_gradient)
                outputs += [out]
#        with open('LayerNames_ResNet.list', 'a') as f:      
        for layer, contents in featureExtractors:
            '''print (layer);
            out = contents(out);
            #outputs += [out]
            if layer in self.target_layers:
                out.register_hook(self.save_gradient)
                outputs += [out];'''
            blocks = contents._modules.items()
            for nBlock, block in blocks:
                #pdb.set_trace();
                modules = block._modules.items()
                residual = out;
                for count, (name, module) in enumerate(modules):
                    #x = module(x)
                    #if name == 'relu':
                        #print ('\''+layer, nBlock, name+'\'', sep = ':',file  = f);
                    if name == 'downsample':
                        #pdb.set_trace();
                        residual = module(residual);
                    else:
                       out = module(out);
                       if name in self.target_layers[0].split(':') and nBlock in self.target_layers[0].split(':') \
                       and layer in self.target_layers[0].split(':'):
                           out.register_hook(self.save_gradient)
                           outputs += [out];
                           #print (layer, nBlock, name);
                out +=residual;
                out = modules[2][1](out);
                if 'residue' in self.target_layers[0].split(':') and nBlock in self.target_layers[0].split(':') \
                       and layer in self.target_layers[0].split(':'):
                           out.register_hook(self.save_gradient)
                           outputs += [out];
                           #print (layer, nBlock, name);
                #pdb.set_trace();
            #pdb.set_trace();
            #for name, module in layer:
            #pdb.set_trace();
        
        return classifier, outputs, out;
        
class ModelOutputs():
	""" Class for making a forward pass, and getting:
	1. The network output.
	2. Activations from intermeddiate targetted layers.
	3. Gradients from intermeddiate targetted layers. """
	def __init__(self, model, target_layers):
		self.model = model
		self.feature_extractor = FeatureExtractor(self.model, target_layers)

	def get_gradients(self):
		return self.feature_extractor.gradients

	def __call__(self, x):
            classifier, target_activations, output  = self.feature_extractor(x)
            #print ('Feature Extractor returned')
            #pdb.set_trace();
            output = classifier[0][1](output);             
            output = output.view(output.size(0), -1)
            output = classifier[1][1](output)
            return target_activations, output
  
class GradCamResNet:
	def __init__(self, model, target_layer_names, use_cuda):
            self.model = model.eval()
            self.cuda = use_cuda
            '''self.model.eval()
            		self.cuda = use_cuda
            		if self.cuda:
            			self.model = model.cuda()'''
            
            self.extractor = ModelOutputs(self.model, target_layer_names)

	def forward(self, inputData):
		return self.model(inputData) 

	def __call__(self, inputData, index = None):
            (features, output) = self.extractor(inputData);
            #print ('Model Outputs returned')
            '''if self.cuda:
            
                (features, output) = self.extractor(inputData.cuda())
            
                #(features, output) = self.extractor(inputData)
            else:
                (features, output) = self.extractor(inputData)'''
            
            if index == None:
                index = np.argmax(output.data.numpy())
            '''classes = \
                np.loadtxt('/media/koustav/Naihati/Dataset/AVA/style_image_lists/styles.txt'
                           , dtype=str)
            classes = [c[1] for c in classes]'''
            
            # print ("Class : %s"%(classes[index]));
            # pdb.set_trace();
            #print ('Before one hot')
            one_hot = np.zeros((1, output.size()[-1]), dtype=np.float32)
            one_hot[0][index] = 1
            one_hot = Variable(torch.from_numpy(one_hot), requires_grad=True)
            #print ('After one hot')
            '''if self.cuda:
                one_hot = torch.sum(one_hot.cuda() * output)
            else:'''
            one_hot = torch.sum(one_hot * output)
            #pdb.set_trace();
            #self.model.features.zero_grad()
            #self.model.classifier.zero_grad()
            #print ('Before backward')
            #print (one_hot)
            #pdb.set_trace();
            one_hot.backward(retain_graph=True)
            #print ('After backward')
            #grads_val = self.extractor.get_gradients()[-1].cpu().data.numpy()
            grads_val = self.extractor.get_gradients()[-1].data.numpy()
            target = features[-1]
            #target = target.cpu().data.numpy()[0, :]
            target = target.data.numpy()[0, :]
            weights = np.mean(grads_val, axis=(2, 3))[0, :]
            cam = np.ones(target.shape[1:], dtype=np.float32)
            #pdb.set_trace()
            for (i, w) in enumerate(weights):
                cam += w * target[i, :, :]
            
            cam = np.maximum(cam, 0)
            cam = cv2.resize(cam, (224, 224))
            cam = cam - np.min(cam)
            cam = cam / np.max(cam)
            return cam
            
        def preprocess_image(self, img):
            means=[0.485, 0.456, 0.406]
            stds=[0.229, 0.224, 0.225]
            #pdb.set_trace();
            #print(img.shape)
            preprocessed_img = img.copy()[: , :, ::-1]
            #preprocessed_img = img.copy()
            for i in range(3):
            	preprocessed_img[:, :, i] = preprocessed_img[:, :, i] - means[i]
            	preprocessed_img[:, :, i] = preprocessed_img[:, :, i] / stds[i]
            preprocessed_img = \
            	np.ascontiguousarray(np.transpose(preprocessed_img, (2, 0, 1)))
            preprocessed_img = torch.from_numpy(preprocessed_img)
            preprocessed_img.unsqueeze_(0)
            input = Variable(preprocessed_img, requires_grad = True)
            return input
        
            
            
            
