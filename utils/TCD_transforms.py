from __future__ import division
import torch
import math
import random
from PIL import Image, ImageOps
try:
    import accimage
except ImportError:
    accimage = None
import numpy as np
import numbers
import types
import collections
import sys
import pdb
from torchvision import models
import cv2
#from skimage.transform import resize
sys.path.append('../utils');
from GradCam import GradCamResNet

class Compose(object):
    """Composes several transforms together.

    Args:
        transforms (list of ``Transform`` objects): list of transforms to compose.

    Example:
        >>> transforms.Compose([
        >>>     transforms.CenterCrop(10),
        >>>     transforms.ToTensor(),
        >>> ])
    """

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, img):
        for t in self.transforms:
            img = t(img)
        return img


class ToTensor(object):
    """Convert a ``PIL.Image`` or ``numpy.ndarray`` to tensor.

    Converts a PIL.Image or numpy.ndarray (H x W x C) in the range
    [0, 255] to a torch.FloatTensor of shape (C x H x W) in the range [0.0, 1.0].
    """

    def __call__(self, pic):
        """
        Args:
            pic (PIL.Image or numpy.ndarray): Image to be converted to tensor.

        Returns:
            Tensor: Converted image.
        """
        #pdb.set_trace();
        if isinstance(pic, np.ndarray):
            # handle numpy array
            img = torch.from_numpy(pic.transpose((2, 0, 1)))
            # backward compatibility
            return img.float().div(255)

        if accimage is not None and isinstance(pic, accimage.Image):
            nppic = np.zeros([pic.channels, pic.height, pic.width], dtype=np.float32)
            pic.copyto(nppic)
            return torch.from_numpy(nppic)

        # handle PIL Image
        if pic.mode == 'I':
            img = torch.from_numpy(np.array(pic, np.int32, copy=False))
        elif pic.mode == 'I;16':
            img = torch.from_numpy(np.array(pic, np.int16, copy=False))
        else:
            img = torch.ByteTensor(torch.ByteStorage.from_buffer(pic.tobytes()))
        # PIL image mode: 1, L, P, I, F, RGB, YCbCr, RGBA, CMYK
        if pic.mode == 'YCbCr':
            nchannel = 3
        elif pic.mode == 'I;16':
            nchannel = 1
        else:
            nchannel = len(pic.mode)
        img = img.view(pic.size[1], pic.size[0], nchannel)
        # put it from HWC to CHW format
        # yikes, this transpose takes 80% of the loading time/CPU
        img = img.transpose(0, 1).transpose(0, 2).contiguous()
        if isinstance(img, torch.ByteTensor):
            return img.float().div(255)
        else:
            return img


class ToPILImage(object):
    """Convert a tensor to PIL Image.

    Converts a torch.*Tensor of shape C x H x W or a numpy ndarray of shape
    H x W x C to a PIL.Image while preserving the value range.
    """

    def __call__(self, pic):
        """
        Args:
            pic (Tensor or numpy.ndarray): Image to be converted to PIL.Image.

        Returns:
            PIL.Image: Image converted to PIL.Image.

        """
        npimg = pic
        mode = None
        if isinstance(pic, torch.FloatTensor):
            pic = pic.mul(255).byte()
        if torch.is_tensor(pic):
            npimg = np.transpose(pic.numpy(), (1, 2, 0))
        assert isinstance(npimg, np.ndarray), 'pic should be Tensor or ndarray'
        if npimg.shape[2] == 1:
            npimg = npimg[:, :, 0]

            if npimg.dtype == np.uint8:
                mode = 'L'
            if npimg.dtype == np.int16:
                mode = 'I;16'
            if npimg.dtype == np.int32:
                mode = 'I'
            elif npimg.dtype == np.float32:
                mode = 'F'
        else:
            if npimg.dtype == np.uint8:
                mode = 'RGB'
        assert mode is not None, '{} is not supported'.format(npimg.dtype)
        return Image.fromarray(npimg, mode=mode)


class Normalize(object):
    """Normalize an tensor image with mean and standard deviation.

    Given mean: (R, G, B) and std: (R, G, B),
    will normalize each channel of the torch.*Tensor, i.e.
    channel = (channel - mean) / std

    Args:
        mean (sequence): Sequence of means for R, G, B channels respecitvely.
        std (sequence): Sequence of standard deviations for R, G, B channels
            respecitvely.
    """

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.

        Returns:
            Tensor: Normalized image.
        """
        # TODO: make efficient
        
        for t, m, s in zip(tensor, self.mean, self.std):
            t.sub_(m).div_(s)
        #pdb.set_trace()
        return tensor


class Add_Saliency(object):

     
    def create_DT(self,size):
        #pdb.set_trace();
        # Creates the two distance channels (horizontal and vertical) normalized between 0 -1.
        size = (size[1],size[0]); # Image size and array size reverse upon conversion from PIL to array 
        x_array = np.tile(np.linspace(0,1,size[1]),(size[0],1));
        y_array = np.transpose(np.tile(np.linspace(0,1,size[0]),(size[1],1)));
        dt_array = np.dstack((x_array,y_array));
        return dt_array;
    
        
    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be scaled.

        Returns:
            PIL.Image: Rescaled image.
        """

        #pdb.set_trace()
        dt_channels = self.create_DT(img.size);
        img_array = np.asarray(img);
        sal_channel = img_array[:,:,3];
        dt_image = np.dstack((sal_channel,dt_channels*255));
        #Image.fromarray(img_array[:,:,3].astype(np.uint8)).show();
        #Image.fromarray(img_array[:,:,0:3].astype(np.uint8)).show();
        #Image.fromarray(dt_image.astype(np.uint8)).show();
        new_image = np.dstack((img_array[:,:,0:3],dt_image));
        #Image.fromarray(new_image[:,:,0:3].astype(np.uint8)).show();            
        #Image.fromarray(new_image[:,:,3:6].astype(np.uint8)).show();            
        
        #Image.fromarray(new_image[:,:,0:3].astype(np.uint8)).show();            
        #Image.fromarray(new_image[:,:,3:6].astype(np.uint8)).show();            

        #pdb.set_trace();
        #return Image.fromarray(img_array.astype(np.uint8));
        return new_image;

class Add_Lines(object):

     
    
        
    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be scaled.

        Returns:
            PIL.Image: Rescaled image.
        """
        
        img_array = np.asarray(img);
        sal_channel = img_array[:,:,3].astype(np.uint8);
        rgb = img_array[:,:,0:3];
        gray = cv2.cvtColor(rgb,cv2.COLOR_BGR2GRAY);
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        lines = cv2.HoughLines(edges,1,np.pi/180,200)
        #pdb.set_trace();
        for line in lines:
            for rho,theta in line:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a*rho
                y0 = b*rho
                x1 = int(x0 + 1000*(-b))
                y1 = int(y0 + 1000*(a))
                x2 = int(x0 - 1000*(-b))
                y2 = int(y0 - 1000*(a))
                cv2.line(sal_channel,(x1,y1),(x2,y2),(255),2)
        Image.fromarray(sal_channel).show()
        Image.fromarray(rgb).show()
        Image.fromarray(edges).show()
        pdb.set_trace();
        
        return img

class Scale(object):
    """Rescale the input PIL.Image to the given size.

    Args:
        size (sequence or int): Desired output size. If size is a sequence like
            (w, h), output size will be matched to this. If size is an int,
            smaller edge of the image will be matched to this number.
            i.e, if height > width, then image will be rescaled to
            (size * height / width, size)
        interpolation (int, optional): Desired interpolation. Default is
            ``PIL.Image.BILINEAR``
    """

    def __init__(self, size, interpolation=Image.BILINEAR):
        assert isinstance(size, int) or (isinstance(size, collections.Iterable) and len(size) == 2)
        self.size = size
        self.interpolation = interpolation

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be scaled.

        Returns:
            PIL.Image: Rescaled image.
        """
        #pdb.set_trace();
        if isinstance(self.size, int):
            w, h = img.size
            if (w <= h and w == self.size) or (h <= w and h == self.size):
                return img
            if w < h:
                ow = self.size
                oh = int((self.size * h) / w)
                return img.resize((ow, oh), self.interpolation)
            else:
                oh = self.size
                ow = int((self.size * w) / h)
                return img.resize((ow, oh), self.interpolation)
        else:
            return img.resize(self.size, self.interpolation)
            

#==============================================================================
# class Sal_Scale(object):
#     """Rescale the input PIL.Image to the given size.
# 
#     Args:
#         size (sequence or int): Desired output size. If size is a sequence like
#             (w, h), output size will be matched to this. If size is an int,
#             smaller edge of the image will be matched to this number.
#             i.e, if height > width, then image will be rescaled to
#             (size * height / width, size)
#         interpolation (int, optional): Desired interpolation. Default is
#             ``PIL.Image.BILINEAR``
#     """
# 
#     def __init__(self, size, interpolation=Image.BILINEAR):
#         assert isinstance(size, int) or (isinstance(size, collections.Iterable) and len(size) == 2)
#         self.size = size
#         self.interpolation = interpolation
# 
#     def __call__(self, img):
#         """
#         Args:
#             img (PIL.Image): Image to be scaled.
# 
#         Returns:
#             PIL.Image: Rescaled image.
#         """
#         #pdb.set_trace();
#         if isinstance(self.size, int):
#             print('Fix Sal_Scale');
#             exit();
#             '''w, h = img.size
#             if (w <= h and w == self.size) or (h <= w and h == self.size):
#                 return img
#             if w < h:
#                 ow = self.size
#                 oh = int(self.size * h / w)
#                 return img.resize((ow, oh), self.interpolation)
#             else:
#                 oh = self.size
#                 ow = int(self.size * w / h)
#                 return img.resize((ow, oh), self.interpolation)'''
#         else:
#             #pdb.set_trace()
#             img_array = np.asarray(img);
#             img_array = cv2.resize(img_array,tuple(self.size));
#==============================================================================
#            return Image.fromarray(img_array.astype(np.uint8));


class Sal_DT_Scale(object):
    """Rescale the input PIL.Image to the given size.

    Args:
        size (sequence or int): Desired output size. If size is a sequence like
            (w, h), output size will be matched to this. If size is an int,
            smaller edge of the image will be matched to this number.
            i.e, if height > width, then image will be rescaled to
            (size * height / width, size)
        interpolation (int, optional): Desired interpolation. Default is
        
            ``PIL.Image.BILINEAR``
    """
    

    def __init__(self, size):
        assert isinstance(size, int) or (isinstance(size, collections.Iterable) and len(size) == 2)
        self.size = size

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be scaled.

        Returns:
            PIL.Image: Rescaled image.
        """
        #pdb.set_trace();
        if isinstance(self.size, int):
            h, w = img.shape[0:2]
            if (w <= h and w == self.size) or (h <= w and h == self.size):
                return img
            if w < h:
                ow = self.size
                oh = int(self.size * h / w)
                return cv2.resize(img,(ow, oh))
            else:
                oh = self.size
                ow = int(self.size * w / h)
                return cv2.resize(img,(ow, oh))
        else:
            new_image = cv2.resize(img, tuple(self.size));
            return new_image;


class CenterCrop(object):
    """Crops the given PIL.Image at the center.

    Args:
        size (sequence or int): Desired output size of the crop. If size is an
            int instead of sequence like (h, w), a square crop (size, size) is
            made.
    """

    def __init__(self, size):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be cropped.

        Returns:
            PIL.Image: Cropped image.
        """
        w, h = img.size
        th, tw = self.size
        if (w < tw or h < th):
            scale = Scale(self.size[0], interpolation=Image.BILINEAR)
            img = scale(img);
            w, h = img.size   
        x1 = int(round((w - tw) / 2.))
        y1 = int(round((h - th) / 2.))
        return img.crop((x1, y1, x1 + tw, y1 + th))
        
class Sal_CenterCrop(object):
    """Crops the given PIL.Image at the center.

    Args:
        size (sequence or int): Desired output size of the crop. If size is an
            int instead of sequence like (h, w), a square crop (size, size) is
            made.
    """

    def __init__(self, size):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be cropped.

        Returns:
            PIL.Image: Cropped image.
        """
        h, w = img.shape[0:2]
        tw, th = self.size
        if (w < tw or h < th):
            #pdb.set_trace();
           #img = cv2.resize(img,(256,256));           
            if w < h:
                ow = tw+(tw - w)
                oh = int((ow * h) / w)
                
                #return cv2.resize(img,(ow, oh))
            else:
                oh = th + (th - h)
                ow = int((oh * w) / h)
            img = cv2.resize(img,(ow, oh));
            (h, w) = (img.shape[0],img.shape[1])
           
        x1 = int(round((h - th) / 2.))
        y1 = int(round((w - tw) / 2.))
        #pdb.set_trace();
        return img[x1:x1 + th, y1:y1 + tw,:];

class Rapid_Sal_CenterCrop(object):    
    
    def __init__(self, size):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size
            
    def __call__(self, img):
        rgb_image = img[:,:,0:3]
        sal_image = img[:,:,3:6]
        
        center_crop = Sal_CenterCrop(self.size);
        scale = Sal_DT_Scale(self.size);
        t_rgb = center_crop(rgb_image);
        t_sal = scale(sal_image);
        final_image = np.dstack((t_rgb,t_sal));
        #pdb.set_trace();
        return final_image;
    
class Pad(object):
    """Pad the given PIL.Image on all sides with the given "pad" value.

    Args:
        padding (int or sequence): Padding on each border. If a sequence of
            length 4, it is used to pad left, top, right and bottom borders respectively.
        fill: Pixel fill value. Default is 0.
    """

    def __init__(self, padding, fill=0):
        assert isinstance(padding, numbers.Number)
        assert isinstance(fill, numbers.Number) or isinstance(fill, str) or isinstance(fill, tuple)
        self.padding = padding
        self.fill = fill

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be padded.

        Returns:
            PIL.Image: Padded image.
        """
        return ImageOps.expand(img, border=self.padding, fill=self.fill)


class Lambda(object):
    """Apply a user-defined lambda as a transform.

    Args:
        lambd (function): Lambda/function to be used for transform.
    """

    def __init__(self, lambd):
        assert isinstance(lambd, types.LambdaType)
        self.lambd = lambd

    def __call__(self, img):
        return self.lambd(img)


class RandomCrop(object):
    """Crop the given PIL.Image at a random location.

    Args:
        size (sequence or int): Desired output size of the crop. If size is an
            int instead of sequence like (h, w), a square crop (size, size) is
            made.
        padding (int or sequence, optional): Optional padding on each border
            of the image. Default is 0, i.e no padding. If a sequence of length
            4 is provided, it is used to pad left, top, right, bottom borders
            respectively.
    """

    def __init__(self, size, padding=0):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size
        self.padding = padding

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be cropped.

        Returns:
            PIL.Image: Cropped image.
        """
        
        if self.padding > 0:
            img = ImageOps.expand(img, border=self.padding, fill=0)
        #print (img.size)
        
        w, h = img.size
        th, tw = self.size


        if w == tw and h == th:
            return img

        if (w <= tw or h <= th):
            #pdb.set_trace();
            scale = Scale(self.size[0], interpolation=Image.BILINEAR)
            crop = CenterCrop(self.size[0])
            return crop(scale(img))

        x1 = random.randint(0, w - tw)
        y1 = random.randint(0, h - th)
        return img.crop((x1, y1, x1 + tw, y1 + th))

'''            if w < h:
                ow = tw+(tw - w)
                oh = int((ow * h) / w)
            else:
                oh = th + (th - h)
                ow = int((oh * w) / h)
            img = img.resize((ow,oh), Image.ANTIALIAS);    
            w, h = img.size'''


        
class Sal_RandomCrop(object):
    """Crop the given PIL.Image at a random location.

    Args:
        size (sequence or int): Desired output size of the crop. If size is an
            int instead of sequence like (h, w), a square crop (size, size) is
            made.
        padding (int or sequence, optional): Optional padding on each border
            of the image. Default is 0, i.e no padding. If a sequence of length
            4 is provided, it is used to pad left, top, right, bottom borders
            respectively.
    """

    def __init__(self, size, padding=0):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size
        self.padding = padding

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be cropped.

        Returns:
            PIL.Image: Cropped image.
        """
        #pdb.set_trace();
        if self.padding > 0:
            img = ImageOps.expand(img, border=self.padding, fill=0)

        (h, w) = (img.shape[0],img.shape[1])
        #(old_h, old_w) = (h,w);
        th, tw = self.size
        
        if (w < tw or h < th):
            #pdb.set_trace();
           #img = cv2.resize(img,(256,256));           
            if w < h:
                ow = tw+(tw - w)
                oh = int((ow * h) / w)
                
                #return cv2.resize(img,(ow, oh))
            else:
                oh = th + (th - h)
                ow = int((oh * w) / h)
                #return cv2.resize(img,(ow, oh))
            img = cv2.resize(img,(ow, oh));
            (h, w) = (img.shape[0],img.shape[1])

        if w == tw and h == th:
            return img
        y1 = random.randint(0, w - tw)
        x1 = random.randint(0, h - th)

            #pdb.set_trace();
        return img[x1:x1 + th, y1:y1 + tw,:];

class Rapid_Sal_RandomCrop(object):    
    
    def __init__(self, size):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size
            
    def __call__(self, img):
        rgb_image = img[:,:,0:3]
        sal_image = img[:,:,3:6]
        
        random_crop = Sal_RandomCrop(self.size);
        scale = Sal_DT_Scale(self.size);
        t_rgb = random_crop(rgb_image);
        t_sal = scale(sal_image);
        final_image = np.dstack((t_rgb,t_sal));
        #pdb.set_trace();
        return final_image;

class RandomHorizontalFlip(object):
    """Horizontally flip the given PIL.Image randomly with a probability of 0.5."""

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be flipped.

        Returns:
            PIL.Image: Randomly flipped image.
        """
        if random.random() < 0.5:
            return img.transpose(Image.FLIP_LEFT_RIGHT)
        return img

class Sal_RandomHorizontalFlip(object):
    """Horizontally flip the given PIL.Image randomly with a probability of 0.5."""

    def __call__(self, img):
        """
        Args:
            img (PIL.Image): Image to be flipped.

        Returns:
            PIL.Image: Randomly flipped image.
        """
        #pdb.set_trace();
        if random.random() < 0.5:
            #return img.transpose(Image.FLIP_LEFT_RIGHT)
            #pdb.set_trace();
            np.flip(img, 2);
        return img

class RandomSizedCrop(object):
    """Crop the given PIL.Image to random size and aspect ratio.

    A crop of random size of (0.08 to 1.0) of the original size and a random
    aspect ratio of 3/4 to 4/3 of the original aspect ratio is made. This crop
    is finally resized to given size.
    This is popularly used to train the Inception networks.

    Args:
        size: size of the smaller edge
        interpolation: Default: PIL.Image.BILINEAR
    """

    def __init__(self, size, interpolation=Image.BILINEAR):
        self.size = size
        self.interpolation = interpolation

    def __call__(self, img):
        for attempt in range(10):
            area = img.size[0] * img.size[1]
            target_area = random.uniform(0.08, 1.0) * area
            aspect_ratio = random.uniform(3. / 4, 4. / 3)

            w = int(round(math.sqrt(target_area * aspect_ratio)))
            h = int(round(math.sqrt(target_area / aspect_ratio)))

            if random.random() < 0.5:
                w, h = h, w

            if w <= img.size[0] and h <= img.size[1]:
                x1 = random.randint(0, img.size[0] - w)
                y1 = random.randint(0, img.size[1] - h)

                img = img.crop((x1, y1, x1 + w, y1 + h))
                assert(img.size == (w, h))

                return img.resize((self.size, self.size), self.interpolation)

        # Fallback
        scale = Scale(self.size, interpolation=self.interpolation)
        crop = CenterCrop(self.size)
        return crop(scale(img))
        
class Sal_RandomSizedCrop(object):
    """Crop the given PIL.Image to random size and aspect ratio.

    A crop of random size of (0.08 to 1.0) of the original size and a random
    aspect ratio of 3/4 to 4/3 of the original aspect ratio is made. This crop
    is finally resized to given size.
    This is popularly used to train the Inception networks.

    Args:
        size: size of the smaller edge
        interpolation: Default: PIL.Image.BILINEAR
    """

    def __init__(self, size, interpolation=Image.BILINEAR):
        self.size = size
        self.interpolation = interpolation

    def __call__(self, img):
        
        for attempt in range(10):
            area = img.shape[0] * img.shape[1]
            target_area = random.uniform(0.08, 1.0) * area
            aspect_ratio = random.uniform(3. / 4, 4. / 3)

            w = int(round(math.sqrt(target_area * aspect_ratio)))
            h = int(round(math.sqrt(target_area / aspect_ratio)))

            if random.random() < 0.5:
                w, h = h, w

            if h <= img.shape[0] and w <= img.shape[1]:
                x1 = random.randint(0, img.shape[1] - w)
                y1 = random.randint(0, img.shape[0] - h)

                #img = img.crop((x1, y1, x1 + w, y1 + h))
                img = img[y1:y1 + h,x1:x1 + w,:];
                #pdb.set_trace();
                assert(img.shape[0:2] == (h, w))
                #pdb.set_trace();

                return cv2.resize(img,(self.size, self.size))

        # Fallback
        scale = Sal_DT_Scale(self.size)
        crop = Sal_CenterCrop(self.size)
        return crop(scale(img))

class Rapid_Sal_RandomSizedCrop(object):    
    
    def __init__(self, size):
            self.size = size
            
    def __call__(self, img):
        #pdb.set_trace();        
        rgb_image = img[:,:,0:3]
        sal_image = img[:,:,3:6]
        random_sized_crop = Sal_RandomSizedCrop(self.size);
        scale = Sal_DT_Scale((self.size, self.size));
        t_rgb = random_sized_crop(rgb_image);
        t_sal = scale(sal_image);
        final_image = np.dstack((t_rgb,t_sal));
        
        return final_image;
