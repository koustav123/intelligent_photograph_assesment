from __future__ import print_function
import torch
from torchvision import models, transforms
from PIL import Image
from torch.autograd import Variable
import numpy as np
#import argparse
from sklearn.metrics import average_precision_score, accuracy_score
from sklearn.metrics import confusion_matrix
#import torchnet
#from torchvision import models

#import random
import pdb
import sys
#import piexif
import time
import os
#adds the utl folder to the path
sys.path.append('../utils');
from GradCam import GradCamResNet
import Gaussian
import CropModule

class TestModule:
    model = 0;
    data_transforms = 0;
    
    def __init__(self, model, data_transforms, dataPath , labelPath, imIdPath, fiftyPatch, multichannel = False):
        self.model = model;
        self.data_transforms = data_transforms;
        self.dataPath = dataPath;
        self.labelPath = labelPath;
        self.imIdPath = imIdPath;
        self.startTime = time.time();
        self.fiftyPatch = fiftyPatch;
        self.multichannel = multichannel;
        if self.multichannel:
            self.cam_model = models.resnet18(True);
            self.grad_cam = GradCamResNet(model = self.cam_model , target_layer_names = ['layer4:1:residue'], use_cuda=False);
            
    
    def sigmoid(self,x):
        return 1 / (1 + np.exp(-x))
        #return x/np.max(x);
        #return x;
    #def deNormalize(self, mean, std): return lambda z : np.add(np.multiply(z.numpy(),std), mean)
    
        
    def pil_loader(self,path):
        # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
        #pdb.set_trace();
        with open(path, 'rb') as f:
            with Image.open(f) as img:
                img_array = np.asarray(img);
                if len(img_array.shape) < 3 or img_array.shape[2] <= 3:
                    return img.convert('RGB')
                else:
                    return img.copy();
            #return img

    '''def PredictSortedLabelsWithWrite(self,imPath):
        scores, meanScore = self.PredictAveragewTx(imPath, write = 1<2);
        #scores = self.PredictwTx(imPath);
        sortedMeanScores = np.argsort(-meanScore);
        labels = []
        predictedLabels = [];
        with open("/home/koustav/Desktop/IPA/intelligent_photograph_assesment/Data/AVA/style_image_lists/styles.txt", 'rb') as f:
            for line in f:
                labels.append(line.strip('\n').split()[1]);
        scoreList = [];
        for score in scores:
            predLab = [];
            sortedScores = np.argsort(-score);
            for i in sortedScores:
                predLab.append(labels[i]);
            scoreList.append(zip(predLab,self.sigmoid(score[sortedScores]).tolist()));
        
        for i in sortedMeanScores:
            predictedLabels.append(labels[i]);
        
        scoreList.append(zip(predictedLabels,self.sigmoid(meanScore[sortedMeanScores]).tolist()));        
        return scoreList;
    
    def PredictSortedLabels(self,imPath):
        scores = self.PredictAveragewTx(imPath);
        #scores = self.PredictwTx(imPath);
        sortedScores = np.argsort(-scores);
        labels = []
        predictedLabels = [];
        with open("/home/koustav/Desktop/IPA/intelligent_photograph_assesment/Data/AVA/style_image_lists/styles.txt", 'rb') as f:
            for line in f:
                labels.append(line.strip('\n').split()[1]);
        
        for i in sortedScores:
            predictedLabels.append(labels[i]);
        return zip(predictedLabels,self.sigmoid(scores[sortedScores]).tolist());'''
        
       
    #predicts the argmax index
    def PredictAccuracyFromPath(self,imPath):
        #piexif.remove(imPath);
        img = Image.open(imPath);
        img_tensor = self.data_transforms(img);
        outputs = self.predict(img_tensor);
       # _, preds = torch.max(outputs.data, 1);
        preds = np.argmax(outputs);
        return preds;
        
    #Finds wTx for one random crop
    def PredictwTx(self,imPath):
        img = self.pil_loader(imPath);
        img_tensor = self.data_transforms(img);
        outputs = self.predict(img_tensor);        
        return outputs;
    

    #Predicts the transformed image tensor        
    def predict(self,img_tensor):
        img_variable = Variable(img_tensor.cuda());
        outputs = self.model(img_variable.unsqueeze(0));
        return outputs.data.cpu().numpy()[0];
    
    
    #Average of 50 random crops of the input image
    def PredictAveragewTx(self,imPath,write = None):
        img = self.pil_loader(imPath);
        scores = [];
        for i in range(50):
            img_tensor = self.data_transforms(img);
            if write:
                copy_tensor = img_tensor.clone();
                tensortoImage = transforms.Compose([
                        CropModule.DeNormalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
                        transforms.ToPILImage()
                    ]);
                #tensortoImage(copy_tensor).save('static/Cropped_Input/TestCrop_'+str(i)+'_.jpg');
            scores.append(self.predict(img_tensor));
        # Depending on whether crooped display is required return value    
        if write:
            return scores, np.mean(scores,axis = 0);
        else:
            return np.mean(scores,axis = 0);
            
            
    def Cuda_predict(self, img_tensors):
        #pdb.set_trace();
        img_variable = Variable(img_tensors.cuda());
        outputs = self.model(img_variable);
        return outputs.data.cpu().numpy();
        
        
    def Cuda_PredictAveragewTx(self,imPath,batchSize):
        img = self.pil_loader(imPath);
        imageCrops = [];
        scores = [];
        for i in range(50):
            img_tensor = self.data_transforms(img);
            imageCrops.append(img_tensor);
        #pdb.set_trace();
        batchIndex = 0;
        while batchIndex < len(imageCrops):
            #scores = np.vstack((scores,self.Cuda_predict(torch.stack(imageCrops[batchIndex:batchIndex+batchSize]))));
            scores.append(self.Cuda_predict(torch.stack(imageCrops[batchIndex:batchIndex+batchSize])));            
            batchIndex = batchIndex+batchSize;
        #pdb.set_trace();
        #scores = np.delete(scores, 0, 0);
        #pdb.set_trace();
        scores = np.vstack(scores);
        #scores = self.Cuda_predict(torch.stack(imageCrops));
        return np.mean(scores, axis = 0)
            
        
    
    #Accepts as a hit if any of the label matches
    '''def AccuracyTracker(self):
        testLabels = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab',dtype = int);
        testIds = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl',dtype = int);
        result = [];
        for c,id in enumerate(testIds):
            imPath = '/media/koustav/Naihati/Dataset/AVADataSet/'+str(id)+'.jpg';
            try:
                prediction = self.PredictAccuracyFromPath(imPath);
            except:
                continue;
            #Accept as hit if any of the label matches
            if (testLabels[c][prediction] == 1 ):
                #print ("%d %d Hit !"%(c, id))
                result.append(1);
            else:
                result.append(0);
            
        pdb.set_trace();
        return np.mean(result);'''
        
    def AccuracyTrackerForQuality(self):
        Testing_scoreData = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/AVA_Full_Testing.scores');
        totalScores = np.sum(np.multiply(np.array(range(1,Testing_scoreData.shape[1]+1)),Testing_scoreData),axis = 1);
        totalSamples = np.sum(Testing_scoreData,axis = 1);
        meanScorePerImage = np.divide(totalScores,totalSamples);
        _scores = meanScorePerImage[(meanScorePerImage <= 5) | (meanScorePerImage > 5) ];
        _labels = np.ones(_scores.shape)*(_scores > 5);
        #np.savetxt('/media/koustav/Naihati/Dataset/AVA/Full_Testing.labels',_labels.transpose(),fmt = "%d");
        _ids = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/AVA_Full_Testing.ids',dtype = int);
        result = [];cleanLabels = [];
        #pdb.set_trace();        
        for c,id in enumerate(_ids):
            imPath = '/media/koustav/Naihati/Dataset/AVA_FRESH/'+str(id)+'.jpg';
            #prediction = self.PredictAccuracyFromPath(imPath);
            
            try:
                prediction = self.PredictAccuracyFromPath(imPath);
                result.append(prediction);
                cleanLabels.append(_labels[c]);
            except IOError:
                print ("Not found : %s"%(imPath));
                continue;
            except RuntimeError:
                print ("Error Opening %s"%(imPath));
                continue;
        print("Number of Samples used for testing : %d"%(len(result)))                
        print("Number of Samples corrupted : %d"%(_labels.shape[0]-len(result)))
        return accuracy_score(cleanLabels,result);
        
    def MAPTracker(self):
        print ("\nTesting MAP... ");
        testLabels = np.loadtxt(self.labelPath, dtype = int);
        testIds = np.loadtxt(self.imIdPath,dtype = int);
        result = []; # array to store the wTx values
        corruptInt = []; # list of corrupted files
        #pdb.set_trace();
        for c,id in enumerate(testIds):
            if os.path.exists(self.dataPath + str(id)+'.png'):
                imPath = self.dataPath + str(id)+'.png';
            if os.path.exists(self.dataPath + str(id)+'.jpg'):
                imPath = self.dataPath + str(id)+'.jpg';
                
            #prediction = self.Cuda_PredictAveragewTx(imPath, 25);
            #prediction = self.PredictAveragewTx(imPath);
            try:
                if self.fiftyPatch:
                    prediction = self.Cuda_PredictAveragewTx(imPath, 10);
                else:
                    prediction = self.PredictwTx(imPath);
            except Exception as e:
                print("Could not process %s \n Error : %s"%(imPath, str(e)));
                corruptInt.append(c);
                continue;
            result.append(prediction);
            if (c%500 == 0):
                print("%d / %d images processed."%(c,len(testIds)))
        #remove corrupt labels
        testLabels = testLabels;
        testLabels = np.delete(testLabels,corruptInt,0);
        #pdb.set_trace();
        #result = result[::50];
        
        
        print ("Prediction Complete... \nNumber of samples used for testing = %d \nNumber of GT values = %d"
        %(len(result),testLabels.shape[0]));
        #pdb.set_trace();
        PerClassP = average_precision_score(testLabels,np.array(result), average=None);
        AP = average_precision_score(testLabels,np.array(result), average='samples');
        mAP_macro = average_precision_score(testLabels,np.array(result), average='macro');
        mAP_weighted = average_precision_score(testLabels,np.array(result), average='weighted');
        mAP_micro = average_precision_score(testLabels,np.array(result), average='micro');
        CM = self.CMeter(testLabels, result);
        #pdb.set_trace();
        print ("Run Time : %f seconds"%(time.time() - self.startTime));
        return CM, AP, mAP_macro, mAP_weighted, mAP_micro, PerClassP;


    def CMeter(self, testLabels, result, multilabel = False ):
        if testLabels.shape[1] == 14:
            multilabel = True;
        if not multilabel:
            #pdb.set_trace();    
            return confusion_matrix(np.argmax(testLabels, axis = 1), np.argmax(result, axis = 1));
        else :
            conf_mat = np.zeros([testLabels.shape[1],testLabels.shape[1]]);
            for gt, pl in zip(testLabels, result):
                #pdb.set_trace();
                gtLabels = np.where(gt == 1)[0];
                predLabels = np.argsort(pl)[::-1][0:gtLabels.shape[0]];
                matches = np.intersect1d(gtLabels, predLabels);
                for m in matches:
                    conf_mat[m][m]+=1;
                for g in gtLabels:
                    if g not in matches:
                        for p in predLabels:
                            if p not in matches:
                                conf_mat[g][p]+=1;
        return conf_mat;  
        
    def findMinMaxScores(self, nMin, nMax):
        testLabels = np.loadtxt(self.labelPath, dtype = int);
        testIds = np.loadtxt(self.imIdPath,dtype = int);
        result = []; # array to store the wTx values
        corruptInt = []; # list of corrupted files
        goodImages = [];
        badImages = [];
        
        for c,id in enumerate(testIds):
            imPath = self.dataPath + str(id)+'.jpg';
            try:
                #prediction = self.Cuda_PredictAveragewTx(imPath, 25);
                prediction = self.PredictwTx(imPath);
            except Exception as e:
                #print("Could not process %s \n Error : %s"%(imPath, str(e)));
                corruptInt.append(c);
                continue;
            result.append(prediction);
            if (c%500 == 0):
                print("%d / %d images processed."%(c,len(testIds)))
        #remove corrupt labels
        testLabels = testLabels;
        testLabels = np.delete(testLabels,corruptInt,0);
        testIds = np.delete(testIds,corruptInt,0);
        #pdb.set_trace();
        scores = np.array(result);
        scores = scores/np.max(scores,axis = 1)[:, np.newaxis];
        sortedScores = np.argsort(scores,axis = 0)[::-1];
        
        for i in range(sortedScores.shape[1]):
            #pdb.set_trace();
            goodImages.append([]);
            badImages.append([]);
            validIds = testLabels[:,i] == 1;
            for j in range(sortedScores.shape[0]):
                if testIds[sortedScores[j][i]] in testIds[validIds]:
                    goodImages[i].append(testIds[sortedScores[j][i]]);
                    if len(badImages[i]) == nMax:
                        break;
                        
            for k in range(sortedScores.shape[0]):
                if testIds[sortedScores[::-1][k][i]] in testIds[validIds]:
                    badImages[i].append(testIds[sortedScores[::-1][k][i]]);
                    if len(badImages[i]) == nMin:
                        break;
                   
        return(goodImages, badImages);        
'''parser = argparse.ArgumentParser(description='AVA Style Classification');

parser.add_argument('--model', type=str, default='.',
                    help='location of the model')

args = parser.parse_args();
model = torch.load(args.model);
pdb.set_trace();
t =   TestModule(model);

#accuracy = t.AccuracyTracker();
#print ("Test Accuracy for %s: %f"%(args.model,accuracy));

AP,mAP_macro,mAP_weighted, mAP_micro= t.MAPTracker();
print ("\nAP = %f \nmAP_macro = %f \nmAP_weighted = %f \nmAP_micro = %f "%(AP,mAP_macro,mAP_weighted,mAP_micro));
'''