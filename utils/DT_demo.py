import sys
import numpy as np
from PIL import Image

def create_DT(size):
    #pdb.set_trace();
    # Creates the two distance channels (horizontal and vertical) normalized between 0 -1.
    size = (size[1],size[0]); # Image size and array size reverse upon conversion from PIL to array 
    x_array = np.tile(np.linspace(0,1,size[1]),(size[0],1));
    y_array = np.transpose(np.tile(np.linspace(0,1,size[0]),(size[1],1)));
    dt_array = np.dstack((x_array,y_array));
    return dt_array;

img = Image.open(sys.argv[1]);
dt_channels = create_DT(img.size);
img_array = np.asarray(img);
sal_channel = img_array[:,:,3];
dt_image = np.dstack((sal_channel,dt_channels*255));
new_image = np.dstack((img_array[:,:,0:3],dt_image));

Image.fromarray(sal_channel.astype(np.uint8)).save(sys.argv[2]+'_Saliency.png');            
Image.fromarray(new_image[:,:,0:3].astype(np.uint8)).save(sys.argv[2]+'_Original.png');            
Image.fromarray(new_image[:,:,3:6].astype(np.uint8)).save(sys.argv[2]+'_DT.png');