# -*- coding: utf-8 -*-
"""
Created on Wed Jun 14 12:08:18 2017

@author: koustav
"""

#Useful Links 
# http://python-guide-pt-br.readthedocs.io/en/latest/scenarios/scrape/
#The above link doesn't work becuase the IP gets blocked due to too many requests

# https://www.w3schools.com/xml/xpath_syntax.asp
# script for creating AVAImageIDs  cat AVA.txt | awk '{ print $2}' > AVAImageIDs.txt
# Web Scraping Tips : https://www.scrapehero.com/how-to-prevent-getting-blacklisted-while-scraping/
####Final trick to avoid getting blocked
#https://github.com/mtobeiyf/ava_downloader/blob/master/ava_downloader.py
# urllib used instead of requests


#TOR solves the issue
#https://stackoverflow.com/questions/30286293/make-requests-using-python-over-tor
#https://www.linux.com/blog/beginners-guide-tor-ubuntu


from __future__ import print_function
import numpy as np
from lxml import html
import time
import pdb
#import urllib
#import pickle
from torrequest import TorRequest
import cStringIO
from PIL import Image
import os
import multiprocessing as mp 
from requests.exceptions import ConnectionError
import sys
import piexif
from functools import partial
import itertools
from multiprocessing import Pool, freeze_support


imageIdList = np.loadtxt('../Data/AVA/AVAImageIDs.txt',dtype = str);
tr = TorRequest(proxy_port=9050, ctrl_port=9051, password='Chmod777!')
tr.reset_identity();
nDownloaded = mp.Value('d', 0.0,lock = False)

#@rate_limited(1)
def PlaceRequest(url, tr):
    #page = requests.get(url);
    #page = urllib.urlopen(url);
	#print ("Request Called for %s"%url)
	try:
	    page = tr.get(url);
	except ConnectionError as e:
		print ("Error 1 ");
		sys.stdout.flush()
		#time.sleep(np.random.randint(1,5));
		#tr.reset_identity();
		#requestIPReset();
		#time.sleep(np.random.randint(2,3));
		page = tr.get(url);
	except ConnectionError as e:
		print ("Error 2 ");
		sys.stdout.flush()
		return None;
    #html = page.read();

    #if page.status_code != 200:
     #   raise ApiError('Cannot call API: {}'.format(page.status_code));
    
	return page;



#corpus = []




def DoDownloadMultiThreading(imId):
	global tr;
	#print(imId)
	if (os.path.exists('/media/koustav/Naihati/Dataset/AVADataSet/'+imId+'.jpg')):
		try:		
			piexif.remove('/media/koustav/Naihati/Dataset/AVADataSet/'+imId+'.jpg');
			return None;
		except piexif._exeptions.InvalidImageDataError as e:
			os.system("echo "+imId+" >> Output/CorruptedData.data" );
		#print("%s exists"%imId,end = '\n');
		
   	 	#if (count+1 <10126):
		#	continue;
	page = PlaceRequest('http://www.dpchallenge.com/image.php?IMAGE_ID='+imId, tr);
	if not page:
		print ("Could Not Process : %s"%(imId))
		return None;
		
        #pdb.set_trace()
        tree = html.fromstring(page.text);
        imagePathLists = tree.xpath('//@src');
	#print (imId)
	for path in imagePathLists:
		#print (path)
		if imId in path:
			#pdb.set_trace();
			img = PlaceRequest(path, tr)
			if not img:
				print ("Could not Download : %s"%(path))
				return None;
			imFile = cStringIO.StringIO(img.content)
			#print("Saving %s ..."%(imId));
			finalImage = Image.open(imFile).convert('RGB');
			finalImage.save('/media/koustav/Naihati/Dataset/AVADataSet/'+imId+'.jpg');	
			try:		
				piexif.remove('/media/koustav/Naihati/Dataset/AVADataSet/'+imId+'.jpg');
			except piexif._exeptions.InvalidImageDataError as e:
				os.system("echo "+imId+" >> Output/CorruptedData.data" );
			#nDownloaded.value = nDownloaded.value + 1;
			break;

        '''#tree = html.fromstring(page.content);
    		tree = html.fromstring(page);
    		posts  = tree.xpath('//table[@class="forum-post"]/tr/td/text()');
		commentString = "";
		for post in posts:
			post = post.encode('utf8');
			post = post.strip().strip('\n');
			commentString = commentString + "#" + post;
   		print(count+1, "#",id, commentString ,end='\n');'''


#if __name__=="__main__":
	

def unzip_arguments(a_b):
    """Convert `f([1,2])` to `f(1,2)` call."""
    return DoDownloadMultiThreading(*a_b)

def main():

    nImages = imageIdList.shape[0];
    batchSize = 256;
    batches = range(nImages)[::batchSize];
    for batch in batches:
    	
    		DIR = '/media/koustav/Naihati/Dataset/AVADataSet/'
    		#nDataset = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])
        	#print("Downloaded after last batch : %d "%(nDownloaded.value));
        	print("Current Dataset size : ", end = " ");
		sys.stdout.flush()
        #	print("Processing Images %d to %d\n"%(batch,batch+batchSize));
		os.system("ls "+DIR+ "| wc -l");        	
		tr.reset_identity();
        	imageList = imageIdList[batch : batch+batchSize]
        	processList = [];
        	p=mp.Pool(48)
        	#partial_DoDownloadMultiThreading = partial(DoDownloadMultiThreading, case=imageList)
        	print("Processing Images %d to %d..."%(batch, batch+batchSize));
		sys.stdout.flush()
        	p.map(DoDownloadMultiThreading, imageList); # range(0,1000) if you want to replicate your example
        	#val = p.map(unzip_arguments, itertools.izip(imageList, itertools.repeat(nDownloaded)))
		p.daemon = True;
        	p.close();
        	p.join();
        	#processAlive  = False;
        	'''for image in imageList:		
        		p = mp.Process(target = DoDownloadMultiThreading, args=(nDownloaded, image))
        		#p.daemon = True;
            		p.start()
        		processList.append(p);
        	for p in processList:
            		p.join();'''
        	#print("%d images processed..." %(batchSize))
        	'''processAlive = True;
        	while processAlive:
        		#time.sleep(5);
        		processAlive  = False;
        		for process in processList:
        			if process.is_alive():
        				print("Child Processes still alive : %s..."%(process.is_alive()))
        				processAlive = True;	
        		print("Child Processes still alive : %s..."%(processAlive))'''
          
        
if __name__=="__main__":
    freeze_support()
    main();
