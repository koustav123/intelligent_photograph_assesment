from __future__ import print_function
import numpy as np
from lxml import html
import time
import pdb
from torrequest import TorRequest
import cStringIO
from PIL import Image
import os
from requests.exceptions import ConnectionError, RequestException
import sys
import piexif
import multiprocessing
import datetime
import argparse

tr = TorRequest(proxy_port=9050, ctrl_port=9051, password='Chmod777!')

parser = argparse.ArgumentParser(description='AVA DATA Downloader');
parser.add_argument('--id_file', type=str, default='../Data/AVA/AVA_Full_Testing.ids',
                    help='location of the ids')
parser.add_argument('--op', type=str, default='/media/koustav/Naihati/Dataset/AVA_FRESH/',
                    help='folder to save images')
args = parser.parse_args();


def PlaceRequest(url):
     global tr;
     try:
            page = tr.get(url);
            while( 'Page cannot be served due to suspicious activity detected from your address' in page.text):
                tr.reset_identity();
                time.sleep(np.random.randint( 3,10));
                page = tr.get(url);
                
     except ConnectionError as e:
            print ("Error 1 for %s"%(url));
            os.system("echo "+url+" >> Output/Connection.log" );
            sys.stdout.flush()
            tr.reset_identity();
            page = tr.get(url);
  
     except RequestException as e:
            print ("Error 2 for %s"%(url));
            os.system("echo "+url+" >> Output/Connection.log" );
            sys.stdout.flush()
            return None;
    #html = page.read();

    #if page.status_code != 200:
     #   raise ApiError('Cannot call API: {}'.format(page.status_code));
    
     return page;
			


def SaveImage((imPath,image)):
    image.save(imPath);	
    '''try:		
		piexif.remove(imPath);
    except piexif._exeptions.InvalidImageDataError as e:
		os.system("echo "+imPath+" >> Output/CorruptedData.data" );'''
    

def main():
    global DIR, nDownloaded,tr;
    startTime = datetime.datetime.now().replace(microsecond=0);
    imageIdList = np.loadtxt(args.id_file,dtype = str);
    print("Loading ids from %s"%args.id_file);
    print("Saving images to  %s"%args.op);
    tr.reset_identity();
    DIR = args.op;
    #CPU_THREAD_LIMIT = 24;
    nDownloaded = 0;
    #processList = []
    imageDataList = []
    #pdb.set_trace();
    for count, imId in enumerate(imageIdList):
        #print (count);
        #pdb.set_trace()
        if (os.path.exists(DIR+imId+'.jpg')):
            #continue;
            try:		
                piexif.remove(DIR+imId+'.jpg');
                continue;
            except piexif._exeptions.InvalidImageDataError as e:
        			os.system("echo "+imId+" >> Output/CorruptedData_Full.data" );
        else:
            #pdb.set_trace();
            page = PlaceRequest('http://www.dpchallenge.com/image.php?IMAGE_ID='+imId);
            if not page:
                print ("Could Not Process : %s"%(imId))
                continue;
            else :
                tree = html.fromstring(page.text);
                imagePathLists = tree.xpath('//@src');
                for path in imagePathLists:
                    if imId in path:
                        img = PlaceRequest(path)
                        if not img:
                            print ("Could not Download : %s"%(path))
                            break;
                        else:
                            nDownloaded+=1;
                            imFile = cStringIO.StringIO(img.content)
                            try :
                                finalImage = Image.open(imFile).convert('RGB');
                                imageDataList.append((DIR + imId+'.jpg',finalImage));
                            except :
                                os.system("echo "+imId+" >> Output/CorruptedData_Full.data" );
                            #p = multiprocessing.Process(target = SaveImage, args=(DIR + imId+'.jpg', finalImage));
                            #p.daemon = True;
                            #processList.append(p);
                            #p.start();
                            
                            #p.join();
        if len(imageDataList) >= 128:
        #Print current directory count
        #for p in processList:                 
        #p.join();
            #print ("Entering multiprocessing...")
            p=multiprocessing.Pool(32)
            p.map(SaveImage, imageDataList); # range(0,1000) if you want to replicate your example
            #val = p.map(unzip_arguments, itertools.izip(imageList, itertools.repeat(nDownloaded)))
            p.daemon = True;
            p.close();
            p.join();
            currentTime = datetime.datetime.now().replace(microsecond=0);
            #print("Number of Images Downloaded : %d"%(nDownloaded));
            print("Current Dataset size : ", end = " ");
            sys.stdout.flush()
            os.system("ls "+DIR+ "| wc -l");        	
            print("Time elapsed = %s \n"%(currentTime - startTime));
            sys.stdout.flush()
            imageDataList = [];

'''if (multiprocessing.cpu_count() >= CPU_THREAD_LIMIT):
print ("Number of Processes : %d, download halted...."%(multiprocessing.cpu_count()))
#time.sleep(5);'''
		
if __name__=="__main__":
    main();
			