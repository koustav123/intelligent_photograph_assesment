from __future__ import print_function
import numpy as np
from lxml import html
import time
import pdb
from torrequest import TorRequest
import cStringIO
from PIL import Image
import os
from requests.exceptions import ConnectionError, RequestException
import sys
import piexif
import multiprocessing
import datetime
import argparse
from skimage.measure import compare_ssim

tr = TorRequest(proxy_port=9050, ctrl_port=9051, password='Chmod777!')

parser = argparse.ArgumentParser(description='Flickr-Style DATA Downloader');
parser.add_argument('--id_file', type=str, default='DataSource/flickr_df_mar2014.csv',
                    help='location of the ids')
parser.add_argument('--op', type=str, default='/media/koustav/Naihati/Dataset/FlickrStyle/',
                    help='folder to save images')
parser.add_argument('--start', type=int, default=0,
                    help='starting index')
parser.add_argument('--end', type=int, default=0,
                    help='end index')

args = parser.parse_args();
blankImage = Image.open('DataSource/4033.jpg').convert('LA');

def PlaceRequest(url):
     global tr;
     try:
            page = tr.get(url);
            '''while( 'Page cannot be served due to suspicious activity detected from your address' in page.text):
                tr.reset_identity();
                time.sleep(np.random.randint( 3,10));
                page = tr.get(url);'''
                
     except ConnectionError as e:
            print ("Error 1 for %s"%(url));
            os.system("echo "+url+" >> Output/Connection.log" );
            sys.stdout.flush()
            tr.reset_identity();
            page = tr.get(url);
  
     except RequestException as e:
            print ("Error 2 for %s"%(url));
            os.system("echo "+url+" >> Output/Connection.log" );
            sys.stdout.flush()
            return None;
    #html = page.read();

    #if page.status_code != 200:
     #   raise ApiError('Cannot call API: {}'.format(page.status_code));
    
     return page;
			


def SaveImage((imPath,image)):
    image.save(imPath);	
    '''try:		
		piexif.remove(imPath);
    except piexif._exeptions.InvalidImageDataError as e:
		os.system("echo "+imPath+" >> Output/CorruptedData.data" );'''
  
def LoadImageList(source):
    imageUrlList = [];
    with open(source, 'rb') as f:
        for line in f:
            line = line.strip('\n');
            imageUrlList.append(line.split(',')[1]);
            
    return imageUrlList;

def main():
    global DIR, nDownloaded,tr;
    startTime = datetime.datetime.now().replace(microsecond=0);
    imageUrlList = LoadImageList(args.id_file);
    imageIdList = range(len(imageUrlList));
    print("Loading ids from %s"%args.id_file);
    print("Saving images to  %s"%args.op);
    print ("Processing images %d - %d"%(args.start,args.end));
    tr.reset_identity();
    DIR = args.op;
    #CPU_THREAD_LIMIT = 24;
    nDownloaded = 0;
    #processList = []
    imageDataList = []
    #pdb.set_trace();
    for count, imId in enumerate(imageIdList):
        if count == 0:
            continue;
        if count < args.start:
            continue;
        if count > args.end:
            break;
        #print (imageUrlList[count]);
        #pdb.set_trace()
        imId = str(imId);
        if (os.path.exists(DIR+imId+'.jpg')):
            #continue;
            try:		
                piexif.remove(DIR+imId+'.jpg');
                continue;
            except piexif._exeptions.InvalidImageDataError as e:
        			os.system("echo "+imId+" >> Output/CorruptedData_Full.data" );
        else:
            #pdb.set_trace();
            page = PlaceRequest(imageUrlList[count]);
            if not page:
                print ("Could Not Process : %s"%(imageUrlList[count]))
                #os.system("echo " + imId +' '+ imageUrlList[count]+" >> Output/LostUrls.data" );
                continue;
            else :
               
                nDownloaded+=1;
                imFile = cStringIO.StringIO(page.content)
                try :
                    finalImage = Image.open(imFile).convert('RGB');
                    imageDataList.append((DIR + imId+'.jpg',finalImage));
                except :
                    os.system("echo "+imId+" >> Output/CorruptedData_Full.data" );
                            
        if len(imageDataList) >= 1:
        
            p=multiprocessing.Pool(32)
            p.map(SaveImage, imageDataList); # range(0,1000) if you want to replicate your example
            #p.daemon = True;
            p.close();
            p.join();
            currentTime = datetime.datetime.now().replace(microsecond=0);
            print("Current Dataset size : ", end = " ");
            sys.stdout.flush()
            os.system("ls "+DIR+ "| wc -l");        	
            print("Time elapsed = %s \n"%(currentTime - startTime));
            sys.stdout.flush()
            imageDataList = [];
		
if __name__=="__main__":
    main();
			