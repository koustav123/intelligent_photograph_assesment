#!/bin/bash

datapath=../Data/AVA/;

echo -e "\nDatapath is $datapath\n\n"

N_training_images=`cat $datapath"style_image_lists/styles.txt" | wc -l`

echo -e "Total number of style attributes = $N_training_images\n\n"

echo -e "The Styles used are : "

cat $datapath"style_image_lists/styles.txt";

echo -e "\n\nNumber of images used for training : "`cat $datapath"style_image_lists/train.jpgl" | wc -l`

echo -e "Number of images used for testing: "`cat $datapath"style_image_lists/test.jpgl" | wc -l`

echo -e "\n\nNumber of samples per category : \n"

sort -n $datapath"/style_image_lists/train.lab" | uniq -c



#For selecting only those images from big AVA which have style annotations
#https://unix.stackexchange.com/questions/134829/compare-two-columns-of-different-files-and-print-if-it-matches

#awk 'NR==FNR{c[$1]++;next};c[$2] > 0' ../Data/AVA/style_image_lists/train.jpgl ../Data/AVA/AVA.txt > ../Data/AVA/AVA_STYLE_ONLY.txt

#To select the comments from full AVA comments
#awk 'NR==FNR{c[$1]++;next};c[$1] > 0' ../Data/AVA/AVA_STYLE_ONLY.txt /home/koustav/Desktop/AVAScripts/aesthetics/data/ava/AVAComments/AVA_Comments_Full.txt > ../Data/AVA/AVA_COMMENTS_STYLE_DATA.txt

python ProcessData.py

#for creating style named directories from styles.txt 
#awk '{print "../AVAStyleImages/train/"$2}' styles.txt | xargs mkdir

#for cleaning test.multilab for all zero entries
#cat test.multilab | grep -n "0 0 0 0 0 0 0 0 0 0 0 0 0 0" | grep -Eo '^[^:]+'> xyz


