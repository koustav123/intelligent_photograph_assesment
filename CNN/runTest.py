from __future__ import print_function
import argparse
import torch
import numpy as np
import sys
import datetime
import os
import csv
#adds the utl folder to the path
sys.path.append('../utils');
sys.path.append('../TCD-Net');

import Augmentation
import TestModule
import Models
parser = argparse.ArgumentParser(description='AVA Style Classification');


parser.add_argument('--model', type=str, default='DenseNet161',
                    help='Name of Convolutional Net (ResNet152, DenseNet161, VGG19, VGG16)')



parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')

parser.add_argument('--aug', type=str, default = 'RSC',
                    help='data augmentation strategy')
parser.add_argument('--classNames', type=str, default='',help='Path to classNAmes')
parser.add_argument('--dataset', type=str, default = '',
                    help='dataset')                    
parser.add_argument('--testDataPath', type=str, default='',help='Path to testdata')
parser.add_argument('--testLabels', type=str, default='',help='Path to labels')
parser.add_argument('--testIds', type=str, default='',help='Path to ids')
parser.add_argument('--csv', type=str, default='',help='csv file')
parser.add_argument('--fiftyPatch', action='store_true',
                    help='use 50 patch testing')
args = parser.parse_args();

model_ft = torch.load(args.model);

#print(model_ft);
#Testing

aug = Augmentation.Augmentation(args.aug);
data_transforms = aug.applyTransforms(crop_size = 224);
t =   TestModule.TestModule(model_ft,data_transforms['val'], args.testDataPath, args.testLabels, args.testIds, args.fiftyPatch);

#accuracy = t.AccuracyTrackerForQuality();

#accuracy = t.AccuracyTracker();
#print ("Test Accuracy for %s: %f"%(args.model,accuracy));
labels = [];
with open(args.classNames, 'rb') as f:
    for line in f:
        labels.append(line.strip('\n').split()[1]);
labels.sort();
CM, AP,mAP_macro,mAP_weighted, mAP_micro, PerClassP = t.MAPTracker();
print ("\nAP = %f \nmAP_macro = %f \nmAP_weighted = %f \nmAP_micro = %f \n"%(AP,mAP_macro,mAP_weighted,mAP_micro));
print ("Per Class : ",PerClassP);
print ("Confusion :", CM);
results = [datetime.datetime.now(), args.dataset, args.model, args.aug, AP, mAP_macro,mAP_weighted, mAP_micro ]+ PerClassP.tolist()
results = [str(i) for i in results];
parameters = ['Time','Dataset','Model','Aug-Test', 'AP', 'MAP_MACRO', 'MAP_WEIGHTED', 'MAP_MICRO']+labels
#pdb.set_trace();

if not os.path.exists(args.csv):
    with open(args.csv, 'a', os.O_NONBLOCK) as f:
        writer = csv.writer(f)
        writer.writerow(parameters);
    
with open(args.csv, 'a', os.O_NONBLOCK) as f:
    writer = csv.writer(f)
    writer.writerow(results);


    