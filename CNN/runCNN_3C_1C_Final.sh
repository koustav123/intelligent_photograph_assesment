#!/bin/bash

function echo_and_run {
  echo "$" "$@"
  eval $(printf '%q ' "$@") < /dev/tty
}

echo -e '\n\nExperiment started at : ' `date` '\n'

epoch120=30

for lr in 0.0001 0.001
do
	for model in DenseNet161 ResNet152 VGG19
	do

		for iter in {1..3}
		do
			echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle_Iteration_$iter --model $model --save /media/koustav/Naihati/CNN_Models/3_C_1_C_1/ --batch_size 12 --epochs $epoch120 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train CC --aug_test CC --dr 0.0 --pretrained --csv Output/Final_Results_1.csv --withTesting
		done

		for iter in {1..3}
		do
			echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle_Iteration_$iter --model $model --save /media/koustav/Naihati/CNN_Models/3_C_1_C_1/ --batch_size 12 --epochs $epoch120 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train S --aug_test S --dr 0.0 --pretrained --csv Output/Final_Results_1.csv --withTesting
		done

		for iter in {1..3}
		do
		echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle_Iteration_$iter --model $model --save /media/koustav/Naihati/CNN_Models/3_C_1_C_1/ --batch_size 12 --epochs $epoch120 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train RC --aug_test RC --dr 0.0 --pretrained --csv Output/Final_Results_1.csv --withTesting --fiftyPatch
		done

		for iter in {1..3}
		do
		echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle_Iteration_$iter --model $model --save /media/koustav/Naihati/CNN_Models/3_C_1_C_1/ --batch_size 12 --epochs $epoch120 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train SC --aug_test SC --dr 0.0 --pretrained --csv Output/Final_Results_1.csv --withTesting
		done

		for iter in {1..3}
		do
		echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle_Iteration_$iter --model $model --save /media/koustav/Naihati/CNN_Models/3_C_1_C_1/ --batch_size 12 --epochs $epoch120 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train RSC --aug_test RSC --dr 0.0 --pretrained --csv Output/Final_Results_1.csv --withTesting --fiftyPatch
		done

	done
done

echo -e '\nExperiment ended at : ' `date` '\n\n'
