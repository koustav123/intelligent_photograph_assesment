#!/bin/bash

function echo_and_run {
  echo "$" "$@"
  eval $(printf '%q ' "$@") < /dev/tty
}

echo -e '\n\nExperiment started at : ' `date` '\n'

for aug_train in CC S RC SC RSC
		do
		for aug_test in CC S RC SC RSC
			do
			echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle_after_learning_rate_changed --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 128 --epochs 200 --lr 0.01 --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/Augmentation.csv --aug_train $aug_train --aug_test $aug_test --withTesting
			done
		done

echo -e '\nExperiment ended at : ' `date` '\n\n'
