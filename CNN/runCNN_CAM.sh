#!/bin/bash

function echo_and_run {
  echo "$" "$@"
  eval $(printf '%q ' "$@") < /dev/tty
}

echo -e '\n\nExperiment started at : ' `date` '\n'

for dr in 0.0 0.5
do
#echo $dr
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVA_STYLE_FOUR_CHANNEL/ --dataset AVAStyle_With_Saliency_DT_Channel --model TCD_ResNet18_SAL --aug_train SAL_S --aug_test SAL_S --save /media/koustav/Naihati/CNN_Models/CAM_STYLE/ --batch_size 128 --lr .001 --epochs 25 --pretrained --dr $dr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData_4_Channels/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/CAM_DROPOUT.csv --withTesting
done

echo -e '\nExperiment ended at : ' `date` '\n\n'
