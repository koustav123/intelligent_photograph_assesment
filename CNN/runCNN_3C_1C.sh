#!/bin/bash

function echo_and_run {
  echo "$" "$@"
  eval $(printf '%q ' "$@") < /dev/tty
}

echo -e '\n\nExperiment started at : ' `date` '\n'

for freeze in '' '--freeze'
do
	for aug in RSC SC RC CC S
	do
		for lr in 0.0001 0.001
		do
			for dr in 0 0.2
			do
				#echo $freeze $aug $lr $dr
				echo_and_run CUDA_VISIBLE_DEVICES=0 python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model TCD_ResNet18_WD --save /media/koustav/Naihati/CNN_Models/3_C_1_C/ --batch_size 128 --epochs 120 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train $aug --aug_test $aug --withTesting --dr $dr --pretrained $freeze --csv Output/3C_1C_DR_AUG_FREEZE_LR.csv
			done
		done
	done
done


echo -e '\nExperiment ended at : ' `date` '\n\n'
