#!/bin/bash

function echo_and_run {
  echo "$" "$@"
  eval $(printf '%q ' "$@") < /dev/tty
}

echo -e '\n\nExperiment started at : ' `date` '\n'

#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model DenseNet161 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 8 --epochs 35 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model DenseNet121 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 8 --epochs 35 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --model /media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_DenseNet121_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 48 --epochs 50 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl
#done

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet34 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 32 --epochs 50 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl
#done

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet50 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 16 --epochs 50 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl
#done

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet101 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 8 --epochs 50 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl
#done

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet152 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 4 --epochs 50 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl
#done
#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net_DenseNet161_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_DenseNet121_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_ResNet18_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_ResNet34_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_ResNet50_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net__dataset_AVAStyle_model_ResNet101_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net_ResNet152_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore runTest.py  --cuda --dataset AVA --model /media/koustav/Naihati/CNN_Models/Style/net_VGG19_aug_RSC_lr_0.001.model --aug RSC --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model VGG16 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 8 --epochs 35 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl

for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 2 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done

for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 4 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done

for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 8 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done

for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 16 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done

for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 32 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done

for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 64 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done


for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 128 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done

for i in {1..5}
do
echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 256 --epochs 30 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/BatchSize.csv
done


#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 128 --epochs 35 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/Augmentation.csv
#done

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 128 --epochs 35 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/Augmentation.csv
#done

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 128 --epochs 35 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/Augmentation.csv
#done

#for i in {1..5}
#do
#echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model ResNet18 --save /media/koustav/Naihati/CNN_Models/Style/ --batch_size 128 --epochs 35 --pretrained --testDataPath /media/koustav/Naihati/Dataset/AVADataSet/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --csv Output/Augmentation.csv
#done
echo -e '\nExperiment ended at : ' `date` '\n\n'
