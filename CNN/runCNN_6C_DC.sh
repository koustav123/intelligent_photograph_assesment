#!/bin/bash

function echo_and_run {
  echo "$" "$@"
  eval $(printf '%q ' "$@") < /dev/tty
}

echo -e '\n\nExperiment started at : ' `date` '\n'


for aug in RC RSC CC SC S 
	do
 		for lr in 0.0001 0.001
			do
				for dr in 0.0 0.2
					do
#						echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVA_STYLE_FOUR_CHANNEL/ --dataset AVAStyle_Double_Column_Test --model TCD_ResNet18_SAL_2_Col --save /media/koustav/Naihati/CNN_Models/CAM_STYLE_3/ --batch_size 32 --epochs 60 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData_4_Channels/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train $aug --aug_test $aug --withTesting --dr $dr --pretrained --csv 6C_2C_25_Aug.csv

						echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVA_STYLE_WITH_SALIENCY/ --dataset AVAStyle_Double_Column_Test --model TCD_DenseNet161_SAL_2_Col --save /media/koustav/Naihati/CNN_Models/Junk/ --batch_size 2 --epochs 1 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData_With_Saliency/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train $aug --aug_test $aug --withTesting --dr $dr --pretrained


#						echo_and_run python -W ignore CNN_Master.py --cuda --datapath /media/koustav/Naihati/Dataset/AVA/AVAStyleImages/ --dataset AVAStyle --model TCD_ResNet18_WD --save /media/koustav/Naihati/CNN_Models/Junk/ --batch_size 128 --epochs 1 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train $aug --aug_test $aug --dr $dr --pretrained

					done
					
			done
	done

echo -e '\nExperiment ended at : ' `date` '\n\n'
