#!/bin/bash

function echo_and_run {
  echo "$" "$@"
  eval $(printf '%q ' "$@") < /dev/tty
}

echo -e '\n\nExperiment started at : ' `date` '\n'

for model in TCD_ResNet18_SAL_2_Col
do
	for lr in 0.0001 0.001 0.01
	do
		for dr in 0.2 0.5 0.7
		do
		
			echo_and_run python -W ignore CNN_Master.py --datapath /media/koustav/Naihati/Dataset/AVA/AVA_STYLE_WITH_SALIENCY/ --dataset AVAStyle_after_saliency_6_C --model $model --save /media/koustav/Naihati/CNN_Models/CAM_STYLE_3/ --batch_size 32 --epochs 60 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData_With_Saliency/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train SAL_DT_SCALE --aug_test SAL_DT_SCALE --withTesting --dr $dr --cuda --csv Output/6C_DC.csv --pretrained --freeze

echo_and_run python -W ignore CNN_Master.py --datapath /media/koustav/Naihati/Dataset/AVA/AVA_STYLE_WITH_SALIENCY/ --dataset AVAStyle_after_saliency_6_C --model $model --save /media/koustav/Naihati/CNN_Models/CAM_STYLE_3/ --batch_size 32 --epochs 60 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData_With_Saliency/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train SAL_DT_SCALE --aug_test SAL_DT_SCALE --withTesting --dr $dr --cuda --csv Output/6C_DC.csv --pretrained

echo_and_run python -W ignore CNN_Master.py --datapath /media/koustav/Naihati/Dataset/AVA/AVA_STYLE_WITH_SALIENCY/ --dataset AVAStyle_after_saliency_6_C --model $model --save /media/koustav/Naihati/CNN_Models/CAM_STYLE_3/ --batch_size 32 --epochs 60 --lr $lr --testDataPath /media/koustav/Naihati/Dataset/AVA/AVAStyleTestData_With_Saliency/ --testLabels /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.multilab --testIds /media/koustav/Naihati/Dataset/AVA/style_image_lists/test.jpgl --aug_train SAL_DT_SCALE --aug_test SAL_DT_SCALE --withTesting --dr $dr --cuda --csv Output/6C_DC.csv

		done
	done
done

echo -e '\nExperiment ended at : ' `date` '\n\n'
