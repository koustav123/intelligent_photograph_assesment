# License: BSD
# Author: Sasank Chilamkurthy

from __future__ import print_function, division
import pdb

import argparse
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
#import numpy as np
#import torchvision
#from torchvision import datasets, models, transforms
from torchvision import datasets, models
from torch.optim.lr_scheduler import ReduceLROnPlateau
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time
import copy
import os

import sys
import csv
import datetime
#adds the utl folder to the path
sys.path.append('../utils');
sys.path.append('../TCD-Net');

import Augmentation as ag
import GhosalkImageFolder as gk
#pdb.set_trace();
import TestModule
import Models
from torch.utils.data import dataloader
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

#Command Line Parsers
def my_collate(batch):
    batch = filter (lambda x:x is not None, batch)
    return dataloader.default_collate(batch)
    
class ErrorHandlingImageFolder(datasets.ImageFolder):
    __init__ = datasets.ImageFolder.__init__
    def __getitem__(self, index):
        try: 
            return super(ErrorHandlingImageFolder, self).__getitem__(index)
        except Exception as e:
            pass
plt.figure();            
    
parser = argparse.ArgumentParser(description='AVA Quality Classification');

parser.add_argument('--dataset', type=str, default='',
                    help='dataset to use')
parser.add_argument('--datapath', type=str, default='',
                    help='location of the data corpus')
parser.add_argument('--model', type=str, default='',
                    help='Name of Convolutional Net (ResNet152, DenseNet161, VGG19, VGG16)')
parser.add_argument('--lr', type=float, default=0.001,
                    help='initial learning rate')
parser.add_argument('--epochs', type=int, default=25,
                    help='upper epoch limit')
parser.add_argument('--batch_size', type=int, default=5, metavar='N',
                    help='batch size')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--save', type=str,  default='',
                    help='path to save the final model')
parser.add_argument('--aug_train', type=str, default = 'SC',
                    help='training data augmentation strategy')
parser.add_argument('--aug_test', type=str, default = 'SC',
                    help='testing data augmentation strategy')                    
parser.add_argument('--tiny', action='store_true',
                    help='use tiny dataset')
parser.add_argument('--pretrained', action='store_true',
                    help='use pretrained model')
parser.add_argument('--fiftyPatch', action='store_true',
                    help='use 50 patch testing')
parser.add_argument('--csv', type=str, default='Output/All_Results.csv',
                    help='csv to save data')
                    
'''parser.add_argument('--testDataPath', type=str, default='',help='Path to testdata')
parser.add_argument('--testLabels', type=str, default='',help='Path to testLabels')
parser.add_argument('--testIds', type=str, default='',help='Path to testIds')'''
args = parser.parse_args();

model_path = args.save+'net_'+'_dataset_' + args.dataset + '_model_'+ args.model +'_pretrained_'+str(args.pretrained)+ '_aug_'+str(args.aug_train)+'_lr_'+str(args.lr)+'.model';
# Data augmentation and normalization for training
# Just normalization for validation

#Call the data Augmentation module with parameters
DAug = ag.Augmentation(args.aug_train);
data_transforms = DAug.applyTransforms();


data_dir = args.datapath;
if args.tiny:
    dsets = {x: gk.MyImageFolder(os.path.join(data_dir, x), data_transforms[x])
             for x in ['train', 'val']}
else:
    dsets = {x: MyImageFolder(os.path.join(data_dir, x), data_transforms[x])
             for x in ['train', 'val']}
dset_loaders = {x: torch.utils.data.DataLoader(dsets[x], batch_size=args.batch_size,
                                               shuffle=True, num_workers=12, collate_fn=my_collate)
                for x in ['train', 'val']}
dset_sizes = {x: len(dsets[x]) for x in ['train', 'val']}
dset_classes = dsets['train'].classes


#use_gpu = torch.cuda.is_available()
use_gpu = args.cuda;




#plt.figure();
'''def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    plt.imshow(inp)
    if title is not None:
        plt.title(title)

    plt.pause(.0001)  # pause a bit so that plots are updated'''

# Get a batch of training data
#inputs, classes = next(iter(dset_loaders['train']))

# Make a grid from batch
#out = torchvision.utils.make_grid(inputs)

#imshow(out, title=[dset_classes[x] for x in classes])


#Training
def train_model(model, criterion, optimizer, lr_scheduler, num_epochs=25):
    since = time.time()

    best_model = model
    best_acc = 0.0
    global model_path;
    #model_path = model_path+'.model';
    for epoch in range(num_epochs):
        #print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        #print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                optimizer = lr_scheduler(optimizer, epoch)
                model.train(True)  # Set model to training mode
                print ('\n', end='');
            else:
                model.train(False)  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0
            #pdb.set_trace();
            # Iterate over data.
            for count, data in enumerate(dset_loaders[phase]):
                # get the inputs
                inputs, labels = data
                #print ("Count : ", count)
                #pdb.set_trace();
                # wrap them in Variable
                if use_gpu:
                    inputs, labels = Variable(inputs.cuda()), \
                        Variable(labels.cuda())
                else:
                    inputs, labels = Variable(inputs), Variable(labels)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                outputs = model(inputs)
                _, preds = torch.max(outputs.data, 1)
                #pdb.set_trace();
                loss = criterion(outputs, labels)

                # backward + optimize only if in training phase
                if phase == 'train':
                    loss.backward()
                    optimizer.step()

                # statistics
                running_loss += loss.data[0]
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dset_sizes[phase]
            epoch_acc = running_corrects / dset_sizes[phase]

            print('Epoch %d || %s Loss: %.4f || Acc: %.4f'%(epoch,
                phase, epoch_loss, epoch_acc),end = ' || ')

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model = copy.deepcopy(model)
                
                with open(model_path, 'wb') as f:
                    torch.save(model, f);

    time_elapsed = time.time() - since
    print('\nTraining complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))
    return best_model


#LR scheduler
def exp_lr_scheduler(optimizer, epoch, init_lr=args.lr, lr_decay_epoch=5):
    """Decay learning rate by a factor of 0.1 every lr_decay_epoch epochs."""
    lr = init_lr * (0.5**(epoch // lr_decay_epoch))

    if epoch % lr_decay_epoch == 0:
        print('\n\nLR is set to {}'.format(lr))

    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

    return optimizer
    return optimizer

#visualizing
'''def visualize_model(model, num_images=6):
    images_so_far = 0
    #fig = plt.figure()

    for i, data in enumerate(dset_loaders['val']):
        inputs, labels = data
        if use_gpu:
            inputs, labels = Variable(inputs.cuda()), Variable(labels.cuda())
        else:
            inputs, labels = Variable(inputs), Variable(labels)

        outputs = model(inputs)
        _, preds = torch.max(outputs.data, 1)

        for j in range(inputs.size()[0]):
            images_so_far += 1
            ax = plt.subplot(num_images//2, 2, images_so_far)
            ax.axis('off')
            ax.set_title('predicted: {}'.format(dset_classes[labels.data[j]]))
            imshow(inputs.cpu().data[j])

            if images_so_far == num_images:
                return'''

if args.model == "DenseNet161":
    model_ft = models.densenet161(args.pretrained);
    model_ft.classifier = nn.Linear(model_ft.classifier.in_features,len(dset_classes));

elif args.model == "DenseNet121":
    model_ft = models.densenet121(args.pretrained);
    model_ft.classifier = nn.Linear(model_ft.classifier.in_features,len(dset_classes));


elif args.model == "ResNet152" :
    model_ft = models.resnet152(args.pretrained)
    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, len(dset_classes));


elif args.model == "ResNet18" :
    model_ft = models.resnet18(args.pretrained)
    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, len(dset_classes));

elif args.model == "ResNet34" :
    model_ft = models.resnet34(args.pretrained)
    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, len(dset_classes));

elif args.model == "ResNet50" :
    model_ft = models.resnet50(args.pretrained)
    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, len(dset_classes));

elif args.model == "ResNet101" :
    model_ft = models.resnet101(args.pretrained)
    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, len(dset_classes));


elif args.model == "VGG19":
    model_ft = models.vgg19(args.pretrained);
    model_ft.classifier._modules['6'] = nn.Linear(4096, len(dset_classes));
    
elif args.model == "VGG16":
    model_ft = models.vgg16(args.pretrained);
    model_ft.classifier._modules['6'] = nn.Linear(4096, len(dset_classes));
    
elif args.model == "MLP":
    model_ft = Models.Net_MLP();
    #model_ft.classifier._modules['6'] = nn.Linear(4096, len(dset_classes));
    
else :
    print ("Model %s not found"%(args.model))
    exit();    
    
if use_gpu:
    model_ft = model_ft.cuda()

criterion = nn.CrossEntropyLoss()
#pdb.set_trace()
# Observe that all parameters are being optimized
optimizer_ft = optim.SGD(model_ft.parameters(), lr=args.lr, momentum=0.9)


#model_ft = torch.load('/media/koustav/Naihati/CNN_Models/net_DenseNet161_aug_RSC_lr_0.001.model');
#Training
model_ft = train_model(model_ft, criterion, optimizer_ft, exp_lr_scheduler,
                      num_epochs=args.epochs);

#Testing
#t =   TestModule.TestModule(model_ft,data_transforms['val']);
#accuracy = t.AccuracyTracker();
#print ("Test Accuracy for %s: %f"%(args.model,accuracy));

#AP,mAP_macro,mAP_weighted, mAP_micro, PerClassPre = t.MAPTracker();
#print ("\nAP = %f \nmAP_macro = %f \nmAP_weighted = %f \nmAP_micro = %f \n\n"%(AP,mAP_macro,mAP_weighted,mAP_micro));
#print (PerClassPre);
#Visualize
#visualize_model(model_ft)
