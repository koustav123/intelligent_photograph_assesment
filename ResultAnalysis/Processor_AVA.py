import numpy as np
import pdb
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 12})

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2., 1.025*height,
                '%0.2f' % (height),
                ha='center', va='bottom')
                
                


models = ['DenseNet161', 'DenseNet121','ResNet18', 'ResNet34', 'ResNet50', 'ResNet101', 'ResNet152', 'VGG19', 'VGG16']
classes = np.loadtxt('/media/koustav/Naihati/Dataset/AVA/style_image_lists/styles.txt', dtype = str);
classes = [c[1].replace('_','\n') for c in classes]
AP = [0.754178, 0.753297, 0.754159, 0.701820, 0.751965, 0.751940, 0.741874, 0.751993, 0.761157 ];

mAP_micro = [0.671407, 0.669931, 0.653037,  0.600149, 0.672688, 0.668458, 0.643077, 0.664666, 0.664978]; 

PCP = np.transpose(np.loadtxt('/home/koustav/Desktop/IPA/intelligent_photograph_assesment/CNN/Output/PCP_reshaped',delimiter = ',', dtype = float));
  
n_groups = 14
#fig, ax = plt.subplots()
#pdb.set_trace();
index = np.arange(n_groups)
bar_width = .5
opacity = 0.8
classIndices = range(1,len(classes)+1)
plt.figure();

for c,(m,pcp) in enumerate(zip(models, PCP)):
    plt.subplot(3,3,c+1);
    #pdb.set_trace();
    rects = plt.bar(index, pcp, bar_width,
                 alpha=opacity,
                 color='g');
    autolabel(rects);
    plt.xticks(index, classIndices)
    plt.ylim([0,1])
    plt.title(m);
    
plt.figure();
index = np.arange(len(models[2:7]));
rects = plt.bar(index, mAP_micro[2:7], bar_width,
                 alpha=opacity,
                 color='g');
plt.ylim([0,1])
plt.xticks(index, models[2:7]);
autolabel(rects);
plt.tight_layout();
plt.show();
