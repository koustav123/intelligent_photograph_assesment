import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 30})
import pdb
plt.figure()
precision_rot = [35.20, 34.27, 33.16, 39.39 ];
Map = [58.10, 70.57, 71.68, 71.99]
networks = ['Fusion','ResNet\n152','DenseNet\n161','Ours\n(2 Columns\n+ GSSM)'];

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                '%0.2f' % (height),
                ha='center', va='bottom')

n_groups = len(precision_rot)
#fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = .4
opacity = 1


rect_1 = plt.bar(index + .2, precision_rot, bar_width,
                 alpha=opacity,
                 color='#1677C8', label = "RoT")
autolabel(rect_1);

rect_2 = plt.bar(index-.2, Map, bar_width,
                 alpha=opacity,
                 color='#890D29', label = "MAP")
autolabel(rect_2);
plt.legend(loc='upper left')
plt.xticks(index, networks);
plt.ylim([0,100])    
plt.ylabel('Precision')    
plt.tight_layout()    
plt.show()