import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 10})
matplotlib.rcParams.update({'savefig.directory':'./Results/'})
matplotlib.rcParams.update({'savefig.format':'pdf'})
import pdb

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2., 1.01*height,
                '%0.2f' % (height),
                ha='center', va='bottom')

data = np.loadtxt('/home/koustav/Desktop/IPA/intelligent_photograph_assesment/ResultAnalysis/Data/Data-Aug-25-ResNet_without_50.csv', dtype = str, delimiter  = ',');
MAP =  np.array(data [:,0], dtype = float);
Train_Aug =  np.array(data [:,1])
Test_Aug =  np.array(data [:,2])

n_groups = 5
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = .15
opacity = 0.8

#pdb.set_trace();

rects1 = plt.bar(index-2*bar_width,MAP[::5], bar_width,
                 alpha=opacity,
                 color='b',
                 label=Test_Aug[0])
autolabel(rects1);
rects2 = plt.bar(index-bar_width, MAP[1:][::5], bar_width,
                 alpha=opacity,
                 color='g',
                 label=Test_Aug[1])
autolabel(rects2);
rects3 = plt.bar(index, MAP[2:][::5],bar_width,
                 alpha=opacity,
                 color='r',
                 label=Test_Aug[2]) 
autolabel(rects3);
rects4 = plt.bar(index + bar_width, MAP[3:][::5],bar_width,
                 alpha=opacity,
                 color='c',
                 label=Test_Aug[3]) 
                 
autolabel(rects4); 
rects5 = plt.bar(index + 2*bar_width, MAP[4:][::5],bar_width,
                 alpha=opacity,
                 color='k',
                 label=Test_Aug[4])
autolabel(rects5); 
#plt.title(data[0,j]);
plt.xticks(index,Test_Aug[0:5])
plt.legend()
plt.ylim([0,1]);
 
plt.tight_layout()

plt.show();
'''
#For PCP
data = np.loadtxt('/home/koustav/Desktop/IPA/intelligent_photograph_assesment/ResultAnalysis/Data/Data-PCP-Aug-25-ResNet_with_50.csv', dtype = str, delimiter  = ',');
for j in range(2,16):
    MAP =  np.array(data [1:,j], dtype = float);
    Train_Aug =  np.array(data [1:,0])
    Test_Aug =  np.array(data [1:,1])
    
    n_groups = 5
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = .15
    opacity = 0.8
    
    #pdb.set_trace();
    
    rects1 = plt.bar(index-2*bar_width,MAP[::5], bar_width,
                     alpha=opacity,
                     color='b',
                     label=Test_Aug[0])
    autolabel(rects1);
    rects2 = plt.bar(index-bar_width, MAP[1:][::5], bar_width,
                     alpha=opacity,
                     color='g',
                     label=Test_Aug[1])
    autolabel(rects2);
    rects3 = plt.bar(index, MAP[2:][::5],bar_width,
                     alpha=opacity,
                     color='r',
                     label=Test_Aug[2]) 
    autolabel(rects3);
    rects4 = plt.bar(index + bar_width, MAP[3:][::5],bar_width,
                     alpha=opacity,
                     color='c',
                     label=Test_Aug[3]) 
                     
    autolabel(rects4); 
    rects5 = plt.bar(index + 2*bar_width, MAP[4:][::5],bar_width,
                     alpha=opacity,
                     color='k',
                     label=Test_Aug[4])
    autolabel(rects5); 
    plt.title(data[0,j]);
    plt.xticks(index,Test_Aug[0:5])
    plt.legend()
    plt.ylim([0,1]);
     
    plt.tight_layout()
plt.show();'''