import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 10})
import pdb
# data to plot

CC_CC = np.array([ 0.55576952,  0.65664953,  0.51865047,  0.75247123,  0.71341467,
        0.55339541,  0.56451697,  0.46006401,  0.72850937,  0.29849118,
        0.66950804,  0.8573789 ,  0.39912415,  0.56033656])
        
CC_W = np.array([ 0.55460866,  0.6967276 ,  0.56652187,  0.37104634,  0.73370468,
        0.51542413,  0.51053458,  0.44964603,  0.74723259,  0.24736477,
        0.67233859,  0.87538742,  0.31047621,  0.664958  ])
        
CC_RC = np.array([ 0.59121827,  0.75323566,  0.62409981,  0.82164846,  0.78717166,
        0.58283076,  0.61659188,  0.58324027,  0.81241569,  0.23322076,
        0.70292851,  0.91682121,  0.45566281,  0.63959607]);
        
CC_ICC = np.array([ 0.55336574,  0.69574448,  0.56614367,  0.44952024,  0.73285034,
        0.51687847,  0.52362543,  0.4801517 ,  0.75712541,  0.26240353,
        0.68822636,  0.89170807,  0.35758734,  0.65698614]);
        
CC_RNC = np.array([ 0.5945036 ,  0.74669731,  0.67827178,  0.67097659,  0.76426928,
        0.59459524,  0.59226174,  0.54293439,  0.81533729,  0.26638238,
        0.71437079,  0.91650594,  0.43764339,  0.67783853])
        
RC_CC = np.array([ 0.55219348,  0.74942969,  0.57433193,  0.80993018,  0.72451126,
        0.55165063,  0.58805698,  0.52936573,  0.76660795,  0.25151471,
        0.75033252,  0.87505009,  0.41503282,  0.57561384])
        
RC_W = np.array([ 0.54728079,  0.8046074 ,  0.69213078,  0.47317859,  0.79228861,
        0.52030242,  0.5581134 ,  0.4848233 ,  0.83796072,  0.27279615,
        0.77478896,  0.89468861,  0.34960394,  0.65929863])
        
RC_RC = np.array([ 0.5971197 ,  0.81117926,  0.69810891,  0.84913539,  0.80398926,
        0.58009922,  0.6374722 ,  0.61239056,  0.83803194,  0.27917321,
        0.7887148 ,  0.91312729,  0.46418955,  0.63385731])
        
RC_ICC = np.array([ 0.56132511,  0.79333477,  0.72325549,  0.53964179,  0.78687367,
        0.51493021,  0.57410652,  0.51083022,  0.82464477,  0.26380011,
        0.78137623,  0.90232526,  0.39415267,  0.67229551])
        
RC_RNC = np.array([ 0.59344493,  0.81780901,  0.74160738,  0.715696  ,  0.80551313,
        0.56943081,  0.61640499,  0.57043105,  0.83821466,  0.27459351,
        0.79715348,  0.9204939 ,  0.45003573,  0.69769853])

W_CC = np.array([ 0.46901317,  0.65189638,  0.54932149,  0.44361682,  0.68642679,
        0.46902522,  0.46649109,  0.44250721,  0.7376799 ,  0.24748064,
        0.63088465,  0.8012953 ,  0.26676644,  0.53197768])
        
W_W = np.array([ 0.5610318 ,  0.72864786,  0.699321  ,  0.65879397,  0.75700314,
        0.57843528,  0.56475697,  0.55561792,  0.83178711,  0.3259764 ,
        0.69434662,  0.8861252 ,  0.36272433,  0.72137284])
        
W_RC = np.array([ 0.53908542,  0.74821554,  0.60374769,  0.50969601,  0.8065166 ,
        0.54305572,  0.53074967,  0.52525677,  0.81943621,  0.23700971,
        0.69716609,  0.88988605,  0.29997778,  0.59829895])
        
W_ICC = np.array([ 0.57179932,  0.73518889,  0.6853811 ,  0.6699494 ,  0.74643499,
        0.56429223,  0.54490381,  0.5380592 ,  0.82435065,  0.3021151 ,
        0.68511975,  0.89181294,  0.40017454,  0.70963447])
        
W_RNC = np.array([ 0.5894855 ,  0.77637394,  0.7250665 ,  0.7620254 ,  0.79866078,
        0.58323611,  0.58148014,  0.56715797,  0.85619928,  0.27761819,
        0.71600923,  0.90830825,  0.39175795,  0.70189198])
        
ICC_CC = np.array([ 0.53147658,  0.6769501 ,  0.46456935,  0.4682418 ,  0.69585224,
        0.51439441,  0.52663515,  0.45255384,  0.67790561,  0.25230864,
        0.63209467,  0.80988807,  0.32036847,  0.50337603])
        
ICC_W = np.array([ 0.59261504,  0.7653141 ,  0.64057102,  0.68222211,  0.80947115,
        0.56354291,  0.62035267,  0.54121477,  0.77210509,  0.30513889,
        0.75803947,  0.89479092,  0.39975449,  0.68892225])
        
ICC_RC = np.array([ 0.60061455,  0.77393646,  0.5397013 ,  0.50830258,  0.81319014,
        0.55775225,  0.5873115 ,  0.56056044,  0.79522023,  0.23300804,
        0.68607982,  0.89090986,  0.37894728,  0.6194073 ])
        
ICC_ICC = np.array([ 0.60049928,  0.7483438 ,  0.63549542,  0.70816751,  0.78602395,
        0.57164317,  0.62156487,  0.52924771,  0.7669904 ,  0.31218564,
        0.75601424,  0.89923594,  0.40288989,  0.66629216])
        
ICC_RNC = np.array([ 0.61927371,  0.79748672,  0.62914517,  0.79601796,  0.80519393,
        0.60059539,  0.63495329,  0.56926963,  0.79963957,  0.27781138,
        0.75323845,  0.91382611,  0.42023088,  0.67275944]);

RNC_CC = np.array([ 0.55719146,  0.69791005,  0.53061053,  0.70743288,  0.73221363,
        0.54819214,  0.59976323,  0.50521376,  0.75879301,  0.270627  ,
        0.7379492 ,  0.86871743,  0.41670386,  0.56387928])
        
RNC_W = np.array([ 0.59958851,  0.75703153,  0.66986742,  0.67008091,  0.82548445,
        0.57828192,  0.60882247,  0.49950246,  0.83502341,  0.30742532,
        0.79795875,  0.91162283,  0.41891003,  0.68043698])

RNC_RC = np.array([ 0.6088515 ,  0.76694737,  0.61835953,  0.77133676,  0.82030438,
        0.58243192,  0.64139954,  0.56907121,  0.83320532,  0.28003876,
        0.79082673,  0.91779314,  0.44833745,  0.6475546 ])
        
RNC_ICC = np.array([ 0.61292427,  0.7512526 ,  0.68793852,  0.72540601,  0.81169386,
        0.56418122,  0.61815964,  0.52090146,  0.82588989,  0.29669334,
        0.80361559,  0.92165376,  0.42295664,  0.6960639 ])
        
RNC_RNC = np.array([ 0.62359259,  0.79622459,  0.6949797 ,  0.81958032,  0.81738271,
        0.59321792,  0.65156677,  0.55707146,  0.84366989,  0.30503934,
        0.81196209,  0.93423939,  0.46355093,  0.69294309])
        
Precision_Array_Large = np.vstack((CC_CC,CC_W,CC_RC,CC_ICC,CC_RNC,
                                       W_CC,W_W,W_RC,W_ICC,W_RNC,
                                       RC_CC,RC_W,RC_RC,RC_ICC,RC_RNC,
                                       ICC_CC,ICC_W,ICC_RC,ICC_ICC,ICC_RNC,
                                       RNC_CC,RNC_W,RNC_RC,RNC_ICC,RNC_RNC));
                                       
# create plot
n_groups = 14
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = .25
opacity = 0.8

 
rects1 = plt.bar(index-bar_width, W_W, bar_width,
                 alpha=opacity,
                 color='b',
                 label='W_W')
 
rects2 = plt.bar(index, RNC_RNC, bar_width,
                 alpha=opacity,
                 color='g',
                 label='RNC_RNC')

rects3 = plt.bar(index + bar_width, RC_RC,bar_width,
                 alpha=opacity,
                 color='r',
                 label='RC_RC') 
#plt.xlabel('Classes')
#plt.ylabel('Scores')
plt.title('Scores per class')
plt.xticks(index, ('Complementary \nColors', 'Duotones', 'HDR',
                               'Image \nGrain', 'Light \nOn \nWhite', 'Long \nExposure', 'Macro',
                               'Motion \nBlur', 'Negative \nImage', 'Rule \nof \nThirds', 'Shallow \nDOF', 'Silhouettes', 'Soft \nFocus', 
                               'Vanishing \nPoint'), rotation = 90)
plt.legend()
 
plt.tight_layout()
#plt.show()
#plt.savefig('../Paper/OVERLEAF/figure/pdf/OverfittingProblem.pdf',dpi = 300);

# For plots of different augmentation for a single attribute
'''nStrategies = 25;
nAttributes = 14
fig, ax = plt.subplots()
index = np.arange(nStrategies)
bar_width = 0.75
opacity = 0.8
#pdb.set_trace();
strategies = ['CC_CC','CC_W','CC_RC','CC_ICC','CC_RNC',
                                       'W_CC','W_W','W_RC','W_ICC','W_RNC',
                                       'RC_CC','RC_W','RC_RC','RC_ICC','RC_RNC',
                                       'ICC_CC','ICC_W','ICC_RC','ICC_ICC','ICC_RNC',
                                       'RNC_CC','RNC_W','RNC_RC','RNC_ICC','RNC_RNC'];
Styles = ['Complementary_Colors', 'Duotones', 'HDR',
                               'Image_Grain', 'Light_On_White', 'Long_Exposure', 'Macro',
                               'Motion_Blur', 'Negative_Image', 'Rule_of_Thirds', 'Shallow_DOF', 'Silhouettes', 'Soft_Focus', 
                               'Vanishing_Point']
#for i in range(nStrategies):
#pdb.set_trace();
                               

for i in range(nAttributes):
#for i in range(2):
    plt.figure();
    #Colors = np.chararray(list('g'*25),itemsize = 7)
    #Colors[np.argmax(Precision_Array_Large[:,i])] = '#D66A00';
    #pdb.set_trace();
    redColorIndex = Precision_Array_Large[:,i]>1.07*np.mean(Precision_Array_Large[:,i]);
    Colors = np.chararray(25, itemsize=10);
    Colors[:] = 'g'
    maxColorIndex = np.argmax(Precision_Array_Large[:,i]);
    #Colors[redColorIndex] = '#D66A00';
    Colors[maxColorIndex] = 'b';
    plt.bar(index, Precision_Array_Large[:,i],bar_width,alpha=opacity,color = Colors); 
    plt.title(Styles[i])
    plt.xticks(index,strategies,rotation = 90);
    plt.ylim([0,1]);
    plt.tight_layout()
    plt.savefig('../Paper/OVERLEAF/figure/pdf/'+Styles[i]+'.pdf',dpi = 300);
    
#plt.legend()



#plt.show()
np.savetxt('Output/PerClassPrecision.txt',100*Precision_Array_Large,fmt='%1.2f',delimiter='&',newline='\\\\\n');
plt.close()'''

pdb.set_trace();